package repository

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/net/html"
	"gorm.io/gorm"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"sea/crawl/app/entities"
	"sea/crawl/app/services/database"
	"sea/crawl/app/services/redis"
	"sea/crawl/app/services/req"
	"strconv"
	"strings"
)

type PostRepository struct {
	db      *gorm.DB
	redis   *redis.RedisClient
	request *req.Req
}

func LoadInstance() *PostRepository {
	return &PostRepository{
		db:      database.DB,
		redis:   redis.Redis,
		request: req.LoadInstance(),
	}
}
func (repo *PostRepository) PublishAllPost(domain string) {
	var posts entities.Posts
	repo.db.Limit(5).Find(&posts, "process_status = ?", 1)
	for _, v := range posts {
		repo.PublishSinglePost(domain, int(v.ID))
		fmt.Println(v.ID)

	}
	fmt.Println("process done all")
}

func (uc *PostRepository) HandlePost(domain string, postId int) (*entities.Post, error) {
	var p entities.Post
	uc.db.First(&p, "id=?", postId)
	if p.ProcessStatus > 1 {
		return &p, errors.New("status in valid")
	}
	postData, err := uc.redis.GetAllItemFromHash("translation_post_" + strconv.Itoa(postId))
	if err != nil || len(postData) == 0 {
		return &p, errors.New("redis data in valid")
	}
	if postData["content"] != "" {
		p.WpContent = postData["content"]
	}
	if postData["title"] != "" {
		p.WpTitle = postData["title"]
	}
	if postData["seo_title"] != "" {
		p.WpSeoTitle = postData["seo_title"]
	}

	if postData["seo_description"] != "" {
		p.WpSeoDescription = postData["seo_description"]
	}

	if p.WpTitle == "" && p.WpSeoTitle != "" {
		p.WpTitle = p.WpSeoTitle
	}

	if p.WpSeoTitle == "" && p.WpTitle != "" {
		p.WpSeoTitle = p.WpTitle
	}

	imageUrl := p.FeatureImage

	if imageUrl != "" {
		id, imageUploadUrl, err := uc.HandleImage(domain, imageUrl)

		if err == nil {
			p.FeatureId = id
			p.UpdateFeatureUrl = imageUploadUrl
		}
	} else {
		return &p, errors.New("image in empty")
	}

	re := regexp.MustCompile("<img (.+)>")
	matches := re.FindAllString(p.WpContent, -1)
	content := p.WpContent
	for _, val := range matches {
		doc, err := html.Parse(strings.NewReader(val))
		if err != nil {
			return &p, err
		}
		var src, alt string = "", ""
		var f func(*html.Node)
		f = func(n *html.Node) {
			if n.Type == html.ElementNode && n.Data == "img" {
				dataSrc := ""
				for _, img := range n.Attr {
					if img.Key == "src" {
						src = img.Val

					}
					if img.Key == "alt" {
						alt = img.Val
					}
					if img.Key == "data-src" && img.Val != "" {
						dataSrc = img.Val
					}

					if src == "" && dataSrc != "" {
						src = dataSrc
					}

				}
			}
			for c := n.FirstChild; c != nil; c = c.NextSibling {
				f(c)
			}
		}
		f(doc)
		_, url, err := uc.HandleImage(domain, src)

		newImage := "<img src='" + url + "' alt='" + alt + "' />"
		content = strings.Replace(content, val, newImage, 1)
	}
	p.WpContent = content

	return &p, nil
}

func (uc *PostRepository) PublishSinglePost(domain string, postId int) {
	p, err := uc.HandlePost(domain, postId)
	if err != nil {
		p.ProcessStatus = 5
		uc.db.Save(&p)
		fmt.Println(err)
		return
	}
	urlPath := domain + "/wp-json/sea/publish-post"
	data := make(map[string]string)
	if p.WpContent == "" || p.WpTitle == "" {
		p.ProcessStatus = 5
		uc.db.Save(&p)
		fmt.Println(err)
		return
	}
	data["title"] = p.WpTitle
	data["content"] = p.WpContent
	data["seo_title"] = p.WpSeoTitle
	data["seo_description"] = p.WpSeoDescription
	data["featured_media"] = strconv.Itoa(p.FeatureId)
	_, code := uc.request.PostJson(urlPath, data)
	if code == 200 {
		p.ProcessStatus = 2
	} else {
		p.ProcessStatus = 4
	}

	uc.db.Save(&p)
	fmt.Println("publish successfully", p.ID)
}

func (repo *PostRepository) HandleImage(domain, imageUrl string) (int, string, error) {
	u, _ := url.Parse(imageUrl)
	path := u.Path
	imagePath, err := DownloadFile(path, imageUrl)
	if err != nil {
		fmt.Println(err)
		return 0, "", err
	}

	id, url := repo.UploadImage(domain, imagePath)

	return id, url, nil
}

func (uc *PostRepository) PublishPost(p *entities.Post) {
	var data = make(map[string]any)
	data["title"] = p.Title

	fmt.Println(p)
}

func DownloadFile(imagePath string, url string) (string, error) {

	path := filepath.Dir(imagePath)

	pathDir := "./storage" + path
	if _, err := os.Stat(pathDir); errors.Is(err, os.ErrNotExist) {
		err := os.MkdirAll(pathDir, os.ModePerm)
		if err != nil {
			return "", err
		}
	}

	// Get the data
	fmt.Println(url)
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if err != nil {
		return "", err
	}
	// Create the file
	fmt.Println("line : " + "./storage" + imagePath)
	storagePath := "./storage/" + imagePath
	out, err := os.Create(storagePath)
	if err != nil {
		return "", err
	}
	// fmt.Println(out)
	defer out.Close()
	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return storagePath, err
}

func (uc *PostRepository) UploadImage(domain string, imagePath string) (int, string) {

	var sec map[string]string
	file, _ := os.Open(imagePath)
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, _ := writer.CreateFormFile("file", filepath.Base(file.Name()))
	io.Copy(part, file)
	writer.Close()

	r, _ := http.NewRequest("POST", domain+"/wp-json/sea/upload", body)
	r.Header.Add("Content-Type", writer.FormDataContentType())
	client := &http.Client{}
	resp, err := client.Do(r)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		//bodyString := string(bodyBytes)
		json.Unmarshal(bodyBytes, &sec)
	}
	id, _ := strconv.Atoi(sec["id"])
	return id, sec["url"]
}
