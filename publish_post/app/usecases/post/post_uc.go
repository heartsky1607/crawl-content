package post

import (
	"sea/crawl/app/entities"
)

type PostInterface interface {
	HandlePost(string, int) (*entities.Post, error)
	PublishPost(post *entities.Post)
	PublishSinglePost(string, int)
	PublishAllPost(string)
}
type Uc struct {
	repo PostInterface
}

func LoadInstance(repo PostInterface) *Uc {
	return &Uc{
		repo: repo,
	}
}

func (uc *Uc) HandlePost(domain string, postId int) (*entities.Post, error) {
	return uc.repo.HandlePost(domain, postId)
}
func (uc *Uc) PublishPost(p *entities.Post) {
	uc.repo.PublishPost(p)
}

func (uc *Uc) PublishSinglePost(domain string, postId int) {
	uc.repo.PublishSinglePost(domain, postId)
}

func (uc *Uc) PublishAllPost(domain string) {
	uc.repo.PublishAllPost(domain)
}
