package controllers

import (
	"github.com/gofiber/fiber/v2"
	"sea/publish_post/app/repository"
	"sea/publish_post/app/usecases/post"
)

type PostController struct {
	repo *post.Uc
}

func LoadInstance() *PostController {
	repo := repository.LoadInstance()
	uc := post.LoadInstance(repo)
	return &PostController{
		repo: uc,
	}
}

func (controller *PostController) Index(ctx *fiber.Ctx) error {
	domains := []string{
		"https://afcchampionsleague2022.com",
		"https://arsenal2022.com",
		"https://acmilan.cc",
		"https://asroma.live",
		"https://atalanta.cc",
		"https://mourinho.cc",
		"https://athleticbilbao.football",
		"https://atlticomadrid.com",
		"https://barca2022.football",
	}

	for _, domain := range domains {
		controller.repo.PublishAllPost(domain)
	}

	//controller.repo.PublishSinglePost(2441)
	ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"status": 200,
		"data":   "Okay",
	})

	return nil
}
