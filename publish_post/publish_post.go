package main

import (
	"github.com/gofiber/fiber/v2"
	"log"
	"sea/crawl/app/services/database"
	"sea/publish_post/app/controllers"
)

func main() {
	Load()
	app := fiber.New()
	SetupRoute(app)
	log.Fatal(app.Listen(":8000"))
}
func Load() {
	err := database.LoadInstance()
	if err != nil {
		panic(err)
	}
}

func SetupRoute(app *fiber.App) {
	controller := controllers.LoadInstance()
	app.Get("/", controller.Index)
}
