package commons

import "golang.org/x/exp/slices"

func CheckItemInSlice(data []string, item string) int {
	return slices.IndexFunc(data, func(c string) bool { return c == item })
}
