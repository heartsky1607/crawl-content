package commons

type successRes struct {
	Data   interface{} `json:"data"`
	Paging interface{} `json:"paging"`
	Filter interface{} `json:"filter"`
}

func NewSuccessResponse(data, paging, filter interface{}) *successRes {
	return &successRes{data, paging, filter}

}

func SimpleSuccessResponse(data interface{}) *successRes {
	return &successRes{data, nil, nil}
}
