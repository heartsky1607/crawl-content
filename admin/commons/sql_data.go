package commons

import "time"

type SqlModel struct {
	ID        uint       `gorm:"primary_key, AUTO_INCREMENT" json:"id"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at;"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"column:created_at;"`
}
