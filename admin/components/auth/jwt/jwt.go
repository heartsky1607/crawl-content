package authjwt

import (
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"time"
)

type Provider interface {
	Generate(data JwtPayload, expiry int) (*jwtToken, error)
	Validate(token string) (JwtPayload, error)
}

type jwtProvider struct {
	prefix string
	secret string
}

func NewJwtProvider(secret string) *jwtProvider {
	return &jwtProvider{
		prefix: "jwt",
		secret: secret,
	}
}

type customClaim struct {
	Payload JwtPayload `json:"payload"`
	jwt.RegisteredClaims
}

func (j *jwtProvider) Generate(data JwtPayload, expire int) (*jwtToken, error) {
	now := time.Now()
	expirationTime := time.Now().Add(time.Duration(expire) * time.Minute)
	claims := &customClaim{
		data,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
			ID:        fmt.Sprintf("%d", now.UnixNano()),
			Issuer:    "",
		},
	}

	var t = jwt.NewWithClaims(
		jwt.SigningMethodHS256,
		claims,
	)
	secret := []byte(j.secret)
	myToken, err := t.SignedString(secret)
	if err != nil {
		return nil, err
	}
	return &jwtToken{
		Token: myToken,
	}, nil
}

func (j *jwtProvider) Validate(myToken string) (JwtPayload, error) {
	res, err := jwt.ParseWithClaims(myToken, &customClaim{}, func(t *jwt.Token) (interface{}, error) {
		return []byte(j.secret), nil
	})
	if err != nil {
		return JwtPayload{}, err
	}

	if !res.Valid {
		return JwtPayload{}, errors.New("invalid token")
	}

	claims, ok := res.Claims.(*customClaim)
	if !ok {
		return JwtPayload{}, errors.New("invalid token")
	}
	return claims.Payload, nil
}
