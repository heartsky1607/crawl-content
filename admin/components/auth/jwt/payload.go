package authjwt

type TokenPayload interface {
	UserId() int
	Role() string
}

type JwtPayload struct {
	Uid   int    `json:"user_id"`
	URole string `json:"role"`
}

func (j JwtPayload) UserId() int {
	return j.Uid
}
func (j JwtPayload) Role() string {
	return j.URole
}
