package authjwt

type Token interface {
	GetToken() string
}

type jwtToken struct {
	Token string
}

func (j *jwtToken) GetToken() string {
	return j.Token
}
