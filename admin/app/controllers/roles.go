package controllers

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"sea/admin/app/repositories"
	"sea/admin/app/usecases"
	"sea/core/commons"
	"sea/core/entities"
	"strconv"
)

type RoleController struct {
	repo *usecases.RoleUc
}

func NewRoleController() *RoleController {
	repo := repositories.NewRoleRepository()
	uc := usecases.NewRoleUc(repo)
	return &RoleController{
		repo: uc,
	}
}

func (role *RoleController) Create(ctx *fiber.Ctx) error {
	r := entities.RoleCreate{}
	if err := ctx.BodyParser(&r); err != nil {
		ctx.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}

	if err := r.Validate(); err != nil {
		ctx.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	id, err := role.repo.Insert(&r)
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}

	ctx.Status(fiber.StatusOK).JSON(&fiber.Map{
		"status":  true,
		"role_id": id,
	})
	return nil
}

func (role *RoleController) ListRole(ctx *fiber.Ctx) error {
	limit := ctx.Query("limit", "5")
	l, err := strconv.Atoi(limit)
	if err != nil {
		ctx.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	page := ctx.Query("page", "1")
	p, err := strconv.Atoi(page)
	if err != nil {
		ctx.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	var pagination = commons.Pagination{
		Limit: l,
		Page:  p,
	}
	data, err := role.repo.ListRole(&pagination)
	if err != nil {
		ctx.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	ctx.Status(fiber.StatusOK).JSON(&fiber.Map{
		"data": data,
	})
	return nil
}

func (role *RoleController) Detail(ctx *fiber.Ctx) error {
	id, err := ctx.ParamsInt("id")

	if err != nil {
		ctx.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	result, err := role.repo.Detail(id)
	if err != nil {
		ctx.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}

	ctx.Status(fiber.StatusOK).JSON(&fiber.Map{
		"data": result,
	})
	return nil
}

func (role *RoleController) Update(ctx *fiber.Ctx) error {
	id, err := ctx.ParamsInt("id")
	fmt.Println("id", id)
	r := entities.RoleUpdate{}
	if err = ctx.BodyParser(&r); err != nil {
		ctx.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}

	if err = r.Validate(); err != nil {
		ctx.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	if err = role.repo.Update(id, &r); err != nil {
		ctx.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}

	ctx.Status(fiber.StatusOK).JSON(&fiber.Map{
		"success": true,
		"role_id": id,
	})
	return nil
}
