package repositories

import (
	"gorm.io/gorm"
	"sea/core/commons"
	"sea/core/entities"
	"sea/core/services/database"
)

type RoleRepository struct {
	db *gorm.DB
}

func (repo *RoleRepository) Detail(id int) (*entities.Role, error) {
	var role entities.Role
	err := repo.db.Find(&role, "id=?", id)

	return &role, err.Error
}

func (repo *RoleRepository) Insert(data *entities.RoleCreate) (int, error) {
	err := repo.db.Create(data)
	return int(data.ID), err.Error
}

func (repo *RoleRepository) Update(id int, data *entities.RoleUpdate) error {
	var role entities.Role
	if err := repo.db.First(&role, "id=?", id); err.Error != nil {
		return err.Error
	}
	role.Name = data.Name
	if err := repo.db.Save(&role); err.Error != nil {
		return err.Error
	}
	return nil
}

func (repo *RoleRepository) Delete(i int) error {
	//TODO implement me
	panic("implement me")
}

func (repo *RoleRepository) ListRole(data *commons.Pagination) (*entities.Roles, error) {
	var roles entities.Roles
	index := (data.Page - 1) * data.Limit
	repo.db.Limit(data.Limit).Offset(index).Find(&roles)
	return &roles, nil
}

func NewRoleRepository() *RoleRepository {
	db, err := database.LoadDbInstance()
	if err != nil {
		panic(err)
	}
	return &RoleRepository{
		db: db,
	}
}
