package usecases

import (
	"sea/core/commons"
	"sea/core/entities"
)

type RoleInterface interface {
	Insert(*entities.RoleCreate) (int, error)
	Update(int, *entities.RoleUpdate) error
	Delete(int) error
	ListRole(*commons.Pagination) (*entities.Roles, error)
	Detail(int) (*entities.Role, error)
}

type RoleUc struct {
	repo RoleInterface
}

func NewRoleUc(repo RoleInterface) *RoleUc {
	return &RoleUc{
		repo: repo,
	}
}

func (uc *RoleUc) Insert(data *entities.RoleCreate) (int, error) {
	return uc.repo.Insert(data)
}

func (uc *RoleUc) Update(id int, data *entities.RoleUpdate) error {
	return uc.repo.Update(id, data)
}

func (uc *RoleUc) Delete(i int) error {
	//TODO implement me
	panic("implement me")
}

func (uc *RoleUc) ListRole(data *commons.Pagination) (*entities.Roles, error) {
	return uc.repo.ListRole(data)
}
func (uc *RoleUc) Detail(i int) (*entities.Role, error) {
	return uc.repo.Detail(i)
}
