module sea/admin

go 1.19

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	go.mongodb.org/mongo-driver v1.11.2 // indirect
	golang.org/x/exp v0.0.0-20230212135524-a684f29349b6 // indirect
)
