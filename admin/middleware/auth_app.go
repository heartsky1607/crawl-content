package middleware

import (
	"errors"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
	"os"
	authjwt "sea/admin/components/auth/jwt"
	usersrepository "sea/admin/modules/users/repository"
	"strings"
)

func extractTokenFromHeaderString(s string) (string, error) {

	parse := strings.Split(s, " ")
	if parse[0] != "Bearer" || len(parse) < 2 || strings.TrimSpace(parse[1]) == "" {
		return "", errors.New("token error")
	}
	return parse[1], nil
}
func AuthApp(db *gorm.DB) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		repo := usersrepository.NewUserRepository(db)
		token, err := extractTokenFromHeaderString(ctx.GetReqHeaders()["Authorization"])

		if err != nil {
			ctx.Status(403).JSON(&fiber.Map{
				"status": false,
				"error":  "request header is invalid ",
			})
			return nil
		}
		provider := authjwt.NewJwtProvider(os.Getenv("JWT_SECRET_KEY"))
		err = repo.VerifyToken(ctx, token, provider)
		if err != nil {
			ctx.Status(403).JSON(&fiber.Map{
				"status": false,
				"error":  "token is invalid ",
			})
			return nil

		}

		//user, err := authStore.FindUser(ctx.UserContext(), map[string]interface{}{"id": payload.UserId()})
		//if err != nil {
		//	fmt.Println(err)
		//	ctx.Next()
		//}

		//ctx.Set("current_user", string(user.ID))
		ctx.Next()
		return nil
	}
}
