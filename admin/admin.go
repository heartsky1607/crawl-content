package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"log"
	"regexp"
	"sea/admin/app/controllers"
	"sea/admin/middleware"
	"sea/admin/modules/crawl/jobs/collect_url"
	"sea/admin/modules/crawl/jobs/collect_url/job_handle"
	crawlUrl "sea/admin/modules/crawl/jobs/crawl_url"
	crawl_url_handle "sea/admin/modules/crawl/jobs/crawl_url/job_handle"
	userstransport "sea/admin/modules/users/transport"
	"sea/core/module/sky/queue"
	"sea/core/services/database"
	"sea/core/services/redis"
	"strings"
	"sync"
)

func main() {
	app := fiber.New()
	Load()
	SetUpRouteAdmin(app)
	pool := make(chan chan collect_url_handle.PayloadCrawlUrl)
	// collect url
	dispacher, err := collect_url.NewCollectDispatcher(pool, "crawl_url")
	if err != nil {

		fmt.Println("dispacher", err)
	}
	dispacher.Initialize(3)
	defer dispacher.Queue.CloseConnection()

	// crawl content
	poolCrawl := make(chan chan crawl_url_handle.PayloadCrawlContent)
	dispatcherCrawl, err := crawlUrl.NewCrawlDispatcher(poolCrawl, "crawl_post")
	if err != nil {
		fmt.Println("dispacher", err)
	}
	dispatcherCrawl.Initialize(8)
	defer dispatcherCrawl.Queue.CloseConnection()

	app.Get("/send", func(ctx *fiber.Ctx) error {

		_, dataList := collect_url_handle.LoadConfiguration("")
		q, err := queue.NewQueue("crawl_url")
		if err != nil {
			fmt.Println("queue init error", err)
			return nil
		}
		var wg sync.WaitGroup
		for _, v := range dataList {
			fmt.Println(v)
			wg.Add(1)
			go func(domain string, q queue.Queue, wg *sync.WaitGroup) {
				fmt.Println("send domain to queue", domain)
				defer func() {
					wg.Done()
					if err := recover(); err != nil {
						log.Println("panic occurred: send domain to job ", err)
					}
				}()

				payload := collect_url_handle.PayloadCrawlUrl{Domain: domain}
				data, err := json.Marshal(payload)
				if err != nil {
					fmt.Println("update json ", err)
					return
				}
				ct := context.Background()
				err = q.SendMessage(ct, string(data))

			}(v.Site, q, &wg)

		}
		wg.Wait()
		defer q.CloseConnection()
		//fmt.Println(err, "send success")
		return nil
	})

	app.Get("/send-debug", func(ctx *fiber.Ctx) error {
		q, err := queue.NewQueue("crawl_url")
		if err != nil {
			fmt.Println(err)
			return nil
		}
		go func() {

			defer func() {
				if err := recover(); err != nil {
					log.Println("panic occurred:", err)
				}
			}()
			payload := collect_url_handle.PayloadCrawlUrl{Domain: "https://www.football.london"}
			data, err := json.Marshal(payload)
			if err != nil {
				fmt.Println(err)
				return
			}
			ct := context.Background()
			err = q.SendMessage(ct, string(data))
			defer q.CloseConnection()
		}()

		//fmt.Println(err, "send success")
		return nil
	})

	app.Get("/crawl", func(ctx *fiber.Ctx) error {
		_, dataList := collect_url_handle.LoadConfiguration("")
		q, err := queue.NewQueue("crawl_post")
		if err != nil {
			fmt.Println(err)
			return nil
		}
		var wg sync.WaitGroup
		for _, v := range dataList {

			wg.Add(1)
			go func(siteUrl string, q queue.Queue, wg *sync.WaitGroup) {

				defer func() {
					wg.Done()
					if err := recover(); err != nil {
						log.Println("panic occurred:", err)
					}
				}()

				domain := strings.Replace(siteUrl, ".", "", -1)
				re := regexp.MustCompile(`//|:|https|www`)
				domain = re.ReplaceAllString(domain, "")

				redisClient := redis.Redis
				for {
					val, _ := redisClient.PopItemFromSet("new_list" + domain)
					fmt.Println(val)
					if val == "" {
						break
					}
					payload := crawl_url_handle.PayloadCrawlContent{Domain: siteUrl, PostUrl: val}
					data, err := json.Marshal(payload)
					if err != nil {
						fmt.Println(err)
						return
					}
					ct := context.Background()
					err = q.SendMessage(ct, string(data))

				}
			}(v.Site, q, &wg)

		}
		wg.Wait()
		defer q.CloseConnection()
		//fmt.Println(err, "send success")
		return nil
	})
	log.Fatal(app.Listen(":8000"))

}

func Load() {
	database.LoadDbInstance()

}

func SetUpRouteAdmin(app *fiber.App) {
	rolesCtr := controllers.NewRoleController()
	UserHandler := userstransport.NewUserHandle(database.DB)

	v1 := app.Group("/v1")
	{
		v1.Post("/login", UserHandler.Authentication)
		//v1.Post("/login", userapi.LoginHandler(database.DB))

		adminRoute := v1.Group("/administration")
		{

			roleGroup := adminRoute.Group("roles")
			{
				roleGroup.Post("/", rolesCtr.Create)
				roleGroup.Get("/", rolesCtr.ListRole)
				roleGroup.Get("/:id", rolesCtr.Detail)
				roleGroup.Put("/:id", rolesCtr.Update)

			}

			userGroup := adminRoute.Group("users")
			{

				userGroup.Post("/", UserHandler.RegisterHandler)
				userGroup.Get("/:id", middleware.AuthApp(database.DB), UserHandler.GetDetailUser)
				userGroup.Put("/:id", UserHandler.UpdateUserDetail)

				//userGroup.Get("/", middleware.RequireAuth(authenStore, provider), userapi.GetListUserHandler(database.DB))
				userGroup.Get("/", UserHandler.PaginationUserHandler)
			}

		}
	}
}
