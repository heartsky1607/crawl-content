package crawlmodel

import "strings"

type CrawlPostCreate struct {
	Title          string `json:"title" gorm:"title"`
	Content        string `json:"content" gorm:"content"`
	SeoTitle       string `json:"seoTitle" gorm:"seo_title"`
	SeoDescription string `json:"seoDescription" gorm:"seo_description"`
	FeatureImage   string `json:"featureImage" gorm:"feature_image"`
	SiteUrl        string `json:"siteUrl" gorm:"site_url"`
	OriginUrl      string `json:"originUrl" gorm:"origin_url"`
	ProcessStatus  int    `json:"processStatus" gorm:"process_status"`
	UniqueID       string `json:"unique_id" gorm:"unique_id"`
}

func (p CrawlPostCreate) Validate() bool {
	if strings.TrimSpace(p.Title) == "" || strings.TrimSpace(p.Content) == "" {
		return false
	}

	return true
}
func (p CrawlPostCreate) TableName() string {
	return "posts"
}
