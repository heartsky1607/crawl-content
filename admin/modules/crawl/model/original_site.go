package crawlmodel

import "sea/admin/commons"

type OriginSiteModel struct {
	commons.SqlModel
	Domain               string `json:"domain" gorm:"column=domain;"`
	PostMinLength        string `json:"post_min_length" gorm:"column=post_min_length"`
	PostMaxLength        string `json:"post_max_length" gorm:"column=post_max_length"`
	OriginLanguage       string `json:"origin_language" gorm:"column=origin_language"`
	TranslateToEnglish   bool   `json:"translate_to_english" gorm:"translate_to_english"`
	TranslationRequired  bool   `json:"translation_required" gorm:"translation_required"`
	ScheduleCrawlSetting string `json:"schedule_crawl_setting" gorm:"schedule_crawl_setting"`
	TranslationService   string `json:"translation_service" gorm:"translation_service"`
	SpinService          string `json:"spin_service" gorm:"spin_service"`
	ImageRequired        string `json:"image_required" gorm:"image_required"`
}
