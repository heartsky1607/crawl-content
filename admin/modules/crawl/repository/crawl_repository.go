package repository

import (
	"gorm.io/gorm"
	crawlmodel "sea/admin/modules/crawl/model"
	"sea/core/services/database"
	"sea/core/services/redis"
)

type crawlRepository struct {
	redis *redis.RedisClient
	db    *gorm.DB
}

func NewCrawlRepository() *crawlRepository {
	return &crawlRepository{
		redis: redis.Redis,
		db:    database.DB,
	}
}

func (repo *crawlRepository) SavePost(post *crawlmodel.CrawlPostCreate) error {
	err := repo.db.Table(crawlmodel.CrawlPostCreate{}.TableName()).Create(post)
	if err.Error != nil {
		return err.Error
	}
	return nil
}
