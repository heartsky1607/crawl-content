package jobs

import (
	"context"
)

type Job interface {
	GetName() string
	Dispatch(context.Context, ...interface{}) error
	GetQueueName() string
	Execute(...interface{}) error
	FallBack(...interface{}) error
}
