package jobs

type CrawlConfiguration struct {
	Site         string `json:"site"`
	CrawlContent string `json:"crawl_content"`
	CrawlUrl     string `json:"crawl_url"`
}
