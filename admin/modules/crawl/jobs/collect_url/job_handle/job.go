package collect_url_handle

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"regexp"
	crawl_url_handle "sea/admin/modules/crawl/jobs/crawl_url/job_handle"
	"sea/core/module/sky/queue"
	"sea/core/services/redis"
	"strings"
)

type collectUrlJob struct {
	name      string
	queueName string
	queue     queue.Queue
	redis     *redis.RedisClient
}

func NewCrawJob(qn string) (*collectUrlJob, error) {
	q, err := queue.NewQueue(qn)
	if err != nil {
		return nil, err
	}
	return &collectUrlJob{
		queueName: qn,
		redis:     redis.Redis,
		queue:     q,
	}, nil
}

func (job *collectUrlJob) GetName() string {
	return job.name
}

func (job *collectUrlJob) Execute(domain string) error {
	fmt.Println("job domain", domain)
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred :", err)
		}
	}()
	result, _ := LoadConfiguration(domain)
	f := result.CrawlUrl
	fmt.Println("function", f)
	reflect.ValueOf(job).MethodByName(f).Call([]reflect.Value{reflect.ValueOf(domain)})
	return nil
}
func (job *collectUrlJob) Dispatch(ctx context.Context, domain string) error {
	payload := PayloadCrawlUrl{Domain: domain}
	data, err := json.Marshal(payload)
	if err != nil {
		return err
	}
	err = job.queue.SendMessage(ctx, string(data))
	if err != nil {
		return err
	}
	return nil
}

func (job *collectUrlJob) GetQueueName() string {
	return job.queueName
}

func (job *collectUrlJob) FallBack(...interface{}) error {
	return nil
}

func (job *collectUrlJob) saveDomainPostUrl(site string, postUrl string) {
	fmt.Println("post url", postUrl)
	domain := strings.Replace(site, ".", "", -1)
	re := regexp.MustCompile(`//|:|https|www`)
	domain = re.ReplaceAllString(domain, "")
	check := job.redis.CheckItemInSet("crawl_list"+domain, postUrl)
	q, err := queue.NewQueue("crawl_post")
	if err != nil {
		fmt.Println(err)
		return
	}
	payload := crawl_url_handle.PayloadCrawlContent{Domain: site, PostUrl: postUrl}
	data, err := json.Marshal(payload)
	if err != nil {
		fmt.Println(err)
		return
	}
	ct := context.Background()
	err = q.SendMessage(ct, string(data))
	if check == false {
		job.redis.AddItemToSet("crawl_list"+domain, postUrl)
		job.redis.AddItemToSet("new_list"+domain, postUrl)
	}
}
