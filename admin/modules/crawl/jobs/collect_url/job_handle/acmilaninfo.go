package collect_url_handle

import (
	"github.com/gocolly/colly"
	"strings"
)

func (job *collectUrlJob) CollectUrlAcMilanInfo(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)

	c.OnHTML("div.besides-block > div.beside-post", func(e *colly.HTMLElement) {
		profileUrl := e.ChildAttr("a", "href")
		if strings.HasPrefix(profileUrl, domain) {
			job.saveDomainPostUrl(domain, profileUrl)
		}

	})
	c.OnHTML("div#home-content > div#primary > main#main > div.home-featured-block ", func(e *colly.HTMLElement) {
		e.ForEach("div.featured-post > figure.post-thumb", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if strings.HasPrefix(profileUrl, domain) {
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})

	})

	c.OnHTML("div#home-content > div#primary > main#main > div.home-featured-block ", func(e *colly.HTMLElement) {
		e.ForEach("div.featured-post-three > figure.post-thumb-mini", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if strings.HasPrefix(profileUrl, domain) {
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})

	})

	c.OnHTML("div#home-content > div#primary > main#main > div.single-col ", func(e *colly.HTMLElement) {
		e.ForEach("div.featured-post-block-coltype > figure.post-thumb-mini", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if strings.HasPrefix(profileUrl, domain) {
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})
	})
	c.Visit(domain)
	return
}
