package collect_url_handle

import (
	"github.com/gocolly/colly"
	"strings"
)

func (job *collectUrlJob) CollectUrlRealMadrid(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)

	c.OnHTML("div.grid_not_cabecera > div.master-layout > div.new-noticias > div.new-noticias_container > div.new-noticias_row", func(e *colly.HTMLElement) {
		e.ForEach("div.new-noticias_item", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if !strings.HasPrefix(profileUrl, "#") {
				profileUrl = domain + profileUrl
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})
	})
	c.OnHTML("div.grid_not_cabecera > div.master-layout > div.new-noticias-mini > div.new-noticias_container > div.new-noticias_grid", func(e *colly.HTMLElement) {
		e.ForEach("div.new-noticias_item", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if !strings.HasPrefix(profileUrl, "#") {
				profileUrl = domain + profileUrl
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})
	})

	c.OnHTML("div.grid_not_cabecera > div.master-layout > div.nh_contenido > div.nh_contenido_container > div.nh_contenido_content > div.nh_contenido_info > div.nh_contenido_swiper_container> ul", func(e *colly.HTMLElement) {
		e.ForEach("li", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if !strings.HasPrefix(profileUrl, "#") {
				profileUrl = domain + profileUrl
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})
	})
	c.Visit(domain)
	return
}
