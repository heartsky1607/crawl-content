package collect_url_handle

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"sea/admin/modules/crawl/jobs"
)

func LoadConfiguration(domain string) (jobs.CrawlConfiguration, []jobs.CrawlConfiguration) {
	jsonFile, err := os.Open("config.json")

	if err != nil {
		fmt.Println(err)
	}
	var result jobs.CrawlConfiguration
	var data []jobs.CrawlConfiguration
	byteValue, _ := io.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &data)
	if domain != "" {
		for _, v := range data {
			if v.Site == domain {
				return v, data
			}
		}
	}

	defer jsonFile.Close()

	return result, data
}
