package collect_url_handle

import (
	"github.com/gocolly/colly"
	"strings"
)

func (job *collectUrlJob) CollectUrlMarca(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)

	c.OnHTML("article > div > div.ue-c-cover-content__main > header", func(e *colly.HTMLElement) {
		profileUrl := e.ChildAttr("a", "href")
		if strings.HasPrefix(profileUrl, domain) {
			job.saveDomainPostUrl(domain, profileUrl)
		}

	})
	c.Visit(domain)
	return
}
