package collect_url_handle

import (
	"github.com/gocolly/colly"
	"strings"
)

func (job *collectUrlJob) CollectUrlFootballLondon(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)
	c.OnHTML("main.mod-pancakes > div.pancake", func(e *colly.HTMLElement) {
		e.ForEach("div.teaser > figure", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if strings.HasPrefix(profileUrl, domain) {
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})

	})
	c.Visit(domain)
	return
}
