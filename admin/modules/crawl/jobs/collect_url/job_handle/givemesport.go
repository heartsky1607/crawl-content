package collect_url_handle

import (
	"fmt"
	"github.com/gocolly/colly"
	"strings"
)

func (job *collectUrlJob) CollectUrlGiveMeSport(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)
	c.OnHTML("div.gms-body > main.gms-main > div.gms-hero", func(e *colly.HTMLElement) {
		profileUrl := e.ChildAttr("a", "href")
		fmt.Println(profileUrl)
		if strings.HasPrefix(profileUrl, domain) {
			job.saveDomainPostUrl(domain, profileUrl)
		}

	})
	c.OnHTML("div.gms-body > main.gms-main > div.gms-cols > div.gms-group", func(e *colly.HTMLElement) {
		e.ForEach("div.gms-teaser", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if strings.HasPrefix(profileUrl, domain) {
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})
	})
	c.OnHTML("div.gms-body > main.gms-main > section.gms-cols", func(e *colly.HTMLElement) {
		e.ForEach("div.gms-teaser", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if strings.HasPrefix(profileUrl, domain) {
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})
	})
	c.Visit(domain)
	return
}
