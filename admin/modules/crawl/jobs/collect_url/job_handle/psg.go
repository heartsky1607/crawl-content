package collect_url_handle

import (
	"github.com/gocolly/colly"
	"strings"
)

func (job *collectUrlJob) CollectUrlPsg(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)

	c.OnHTML("div#main > div > div.home-page-feature > section > div.container > div.content-grid ", func(e *colly.HTMLElement) {
		e.ForEach("div.card > div.card__content", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if strings.HasPrefix(profileUrl, domain) {
				job.saveDomainPostUrl(domain, profileUrl)
			}
		})

	})
	c.Visit(domain)
	return
}
