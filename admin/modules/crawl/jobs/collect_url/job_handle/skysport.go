package collect_url_handle

import (
	"github.com/gocolly/colly"
	"strings"
)

func (job *collectUrlJob) CollectUrlSkySports(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)

	c.OnHTML("div.news-list", func(e *colly.HTMLElement) {
		e.ForEach("div.news-list__item", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			if strings.HasPrefix(profileUrl, domain) && strings.Contains(profileUrl, "/football/news/") {
				job.saveDomainPostUrl(domain, profileUrl)
			}

		})
	})

	c.Visit(domain)
	return
}
