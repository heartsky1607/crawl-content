package collect_url

import (
	"encoding/json"
	"fmt"
	"log"
	"sea/admin/modules/crawl/jobs/collect_url/job_handle"
	"sea/core/module/sky/queue"
)

type collectDispatcher struct {
	Workers    []*collectUrlWorker
	WorkerPool chan chan collect_url_handle.PayloadCrawlUrl
	Queue      queue.Queue
	QueueName  string
}

func NewCollectDispatcher(pool chan chan collect_url_handle.PayloadCrawlUrl, qn string) (*collectDispatcher, error) {

	workQueue, err := queue.NewQueue(qn)
	if err != nil {
		return nil, err
	}
	return &collectDispatcher{
		WorkerPool: pool,
		Queue:      workQueue,
		QueueName:  qn,
	}, nil
}

func (disp *collectDispatcher) Initialize(maxWorker int) {
	// Now, create all of our workers.
	for i := 0; i < maxWorker; i++ {
		fmt.Println("Starting worker", i+1)
		worker := NewCollectUrlWorker(i+1, disp.WorkerPool, disp.QueueName)
		disp.Workers = append(disp.Workers, worker)
		worker.Initialize()
	}
	go func() {
		messages, err := disp.Queue.GetChannel().Consume(
			disp.Queue.GetName(), // queue name
			"",                   // consumer
			true,                 // auto-ack
			false,                // exclusive
			false,                // no local
			false,                // no wait
			nil,                  // arguments
		)
		if err != nil {
			log.Println(err)
		}
		for message := range messages {
			var data collect_url_handle.PayloadCrawlUrl
			err = json.Unmarshal([]byte(message.Body), &data)
			if err != nil {
				fmt.Println(err, data)
				continue
			}

			go func() {
				worker := <-disp.WorkerPool
				worker <- data
			}()

		}

	}()
}
