package collect_url

import (
	"fmt"
	collectUrlHandle "sea/admin/modules/crawl/jobs/collect_url/job_handle"
)

type collectUrlWorker struct {
	Id            int
	WorkerPool    chan chan collectUrlHandle.PayloadCrawlUrl
	WorkerChannel chan collectUrlHandle.PayloadCrawlUrl
	QuitChannel   chan bool
	queueName     string
}

func NewCollectUrlWorker(id int, pool chan chan collectUrlHandle.PayloadCrawlUrl, queueName string) *collectUrlWorker {
	return &collectUrlWorker{
		Id:            id,
		WorkerPool:    pool,
		WorkerChannel: make(chan collectUrlHandle.PayloadCrawlUrl, 5),
		QuitChannel:   make(chan bool),
		queueName:     queueName,
	}
}

func (worker *collectUrlWorker) Initialize() {
	go func() {
		for {
			worker.WorkerPool <- worker.WorkerChannel
			select {
			case data := <-worker.WorkerChannel:
				job, err := collectUrlHandle.NewCrawJob(worker.queueName)
				if err != nil {
					fmt.Println(err)
					worker.Stop()
				}
				job.Execute(data.Domain)
			case <-worker.QuitChannel:
				return
			}
		}
	}()
}

func (worker *collectUrlWorker) Stop() {
	go func() {
		worker.QuitChannel <- true
	}()
}
