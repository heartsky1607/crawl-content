package crawl_url_handle

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/net/html"
	"io"
	"net/url"
	"os"
	"regexp"
	"sea/admin/modules/crawl/jobs"
	"unicode"
)

func LoadConfiguration(domain string) jobs.CrawlConfiguration {
	jsonFile, err := os.Open("config.json")

	if err != nil {
		fmt.Println(err)
	}
	var result jobs.CrawlConfiguration
	var data []jobs.CrawlConfiguration
	byteValue, _ := io.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &data)
	for _, v := range data {
		if v.Site == domain {
			return v
		}
	}

	defer jsonFile.Close()

	return result
}

func RemoveEmptyTags(html string) string {
	content := TrimContent(html)
	re, _ := regexp.Compile("<[^>/]+></[^>]+>")
	rep := re.ReplaceAllString(content, "")
	if rep != content {
		return RemoveEmptyTags(rep)
	}
	return rep
}

func TrimContent(s string) string {
	isSpace := true
	rr := make([]rune, 0, len(s))
	for _, r := range s {
		if !unicode.IsSpace(r) {
			rr = append(rr, r)
			isSpace = false
		} else {
			if isSpace == false {
				rr = append(rr, r)
				isSpace = true
			}
		}
	}
	result := string(rr)
	re, _ := regexp.Compile("\n")
	result = re.ReplaceAllString(result, "")
	re, _ = regexp.Compile("<!--.*?-->")
	result = re.ReplaceAllString(result, "")
	re, _ = regexp.Compile("<div (.*?)>")
	result = re.ReplaceAllString(result, "<div>")
	re, _ = regexp.Compile("<p (.*?)>")
	result = re.ReplaceAllString(result, "<p>")
	re, _ = regexp.Compile("<span (.*?)>")
	result = re.ReplaceAllString(result, "<span>")
	re, _ = regexp.Compile("\"\"")
	result = re.ReplaceAllString(result, "\"")
	return result
}

func CollectText(n *html.Node, buf *bytes.Buffer) {
	if n.Type == html.TextNode {
		buf.WriteString(n.Data)
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		CollectText(c, buf)
	}
}
func ClearImageUrl(imageUrl string) string {
	u, _ := url.Parse(imageUrl)
	u.RawQuery = ""
	u.Fragment = ""
	return u.String()
}

func GetBody(n *html.Node) (string, error) {
	var body *html.Node
	var crawler func(*html.Node)
	crawler = func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == "body" {
			body = node
			return
		}
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			crawler(child)
		}
	}
	crawler(n)
	if body != nil {
		return RenderNode(body), nil
	}
	return "", errors.New("Missing <body> in the node tree")
}
func RenderNode(n *html.Node) string {
	var buf bytes.Buffer
	w := io.Writer(&buf)
	html.Render(w, n)
	return buf.String()
}

func RemoveTag(n *html.Node, tag string) bool {
	if n.Type == html.ElementNode && n.Data == tag {
		n.Parent.RemoveChild(n)
		return true
	}
	return false
}
