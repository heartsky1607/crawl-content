package crawl_url_handle

import (
	"bytes"
	"fmt"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"log"
	crawlmodel "sea/admin/modules/crawl/model"
	"strings"
)

func (*crawlJob) CrawlContentRealMadrid(urlPath string) crawlmodel.CrawlPostCreate {
	//fmt.Println(urlPath)
	var post crawlmodel.CrawlPostCreate
	c := colly.NewCollector(
		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)

	//infoCollector := c.Clone()
	fmt.Println("start collect")
	c.OnHTML("div.grid_not_cabecera > div.master-layout >div.full_section > div.nnt-cabecera_container > h1", func(e *colly.HTMLElement) {
		fmt.Println(e.Text)
		post.Title = e.Text
	})
	c.OnHTML("div.grid_not_cabecera > div.master-layout >div.full_section > div.nnt-cabecera_container > div.nnt-cabecera_media_container", func(e *colly.HTMLElement) {
		//fmt.Println(e.DOM.Html())
		imageResource := e.ChildAttr("img", "data-srcset")
		imgs := strings.Split(imageResource, ",")
		imageUrl := ""
		for _, v := range imgs {
			if strings.Contains(v, "960w") {
				imageUrl = strings.Replace(v, "960w", "", 1)
			}
		}
		if imageUrl != "" {
			//imageUrl = strings.Replace(imageUrl, "cc_160px", "cc_960px", 1)
			post.FeatureImage = "https://www.realmadrid.com" + imageUrl
			fmt.Println(post.FeatureImage)
		}

	})

	c.OnHTML("div.grid_not_cabecera > div.master-layout >div.full_section > div.nnt-noticia_container  > div.nnt-noticia_container_text", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContentRealMadrid(post.Content)
		content = RemoveEmptyTags(content)
		post.Content = content
		fmt.Println(content)
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})
	c.Visit(urlPath)
	post.OriginUrl = urlPath

	//cr.db.Create(&post)
	return post
}

func filterContentRealMadrid(s string) string {

	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScriptRealMadrid(doc)
	removeDivRealMadrid(doc)
	html.Render(&content, doc)
	body, err := GetBody(doc)
	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)
	return body
}

func removeScriptRealMadrid(n *html.Node) {

	// if note is script tag
	if n.Type == html.ElementNode && n.Data == "script" {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		CollectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}

	}

	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScriptSkySports(c)
	}
}

func removeDivRealMadrid(n *html.Node) {
	if n.Type == html.ElementNode && n.Data == "p" {
		for _, attr := range n.Attr {
			if attr.Key == "class" && (attr.Val == "nnt-noticia_title" || attr.Val == "nnt-noticia_video_subtitle") {
				n.Parent.RemoveChild(n)
			}
		}

	}
	if n.Type == html.ElementNode && n.Data == "div" {
		imageUrl := ""
		alt := ""
		checkVideo := false
		for _, attr := range n.Attr {
			if attr.Key == "class" && attr.Val == "m_videojs_hd" {
				checkVideo = true
			}
		}
		if checkVideo {
			for _, attr := range n.Attr {
				if attr.Key == "title" {
					alt = attr.Val
				}
				if attr.Key == "data-video-poster" {
					imageUrl = attr.Val
				}
			}
			imageUrl = ClearImageUrl(imageUrl)

			attr := []html.Attribute{}
			attr = append(attr, html.Attribute{"", "src", imageUrl})
			attr = append(attr, html.Attribute{"", "alt", alt})
			nodeLink := html.Node{
				Type: html.ElementNode,
				Data: "img",
				Attr: attr,
			}
			n.Parent.InsertBefore(&nodeLink, n.NextSibling)
			n.Parent.RemoveChild(n)
			return
		}

	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeDivRealMadrid(c)
	}
}
