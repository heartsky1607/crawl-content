package crawl_url_handle

import (
	"bytes"
	"fmt"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"log"
	"net/url"
	crawlmodel "sea/admin/modules/crawl/model"
	"strings"
)

func (*crawlJob) CrawlContentPsg(urlPath string) crawlmodel.CrawlPostCreate {
	//fmt.Println(urlPath)
	var post crawlmodel.CrawlPostCreate
	c := colly.NewCollector(
		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)

	//infoCollector := c.Clone()
	fmt.Println("start collect")
	c.OnHTML("article > div.article__header > h1", func(e *colly.HTMLElement) {
		text := RemoveEmptyTags(e.Text)
		post.Title = text
		fmt.Println(text)
	})
	c.OnHTML("article > div.article__body > div.article__body__inner > p ", func(e *colly.HTMLElement) {
		imageUrl := e.ChildAttr("img", "src")
		if imageUrl != "" {
			u, _ := url.Parse(imageUrl)
			u.RawQuery = ""
			u.Fragment = ""
			post.FeatureImage = "https://en.psg.fr" + u.String()
			fmt.Println(post.FeatureImage)
		}
	})
	c.OnHTML("article > div.article__body", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContentPsg(post.Content)
		content = RemoveEmptyTags(content)
		post.Content = content
		fmt.Println(content)
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})
	c.Visit(urlPath)
	post.OriginUrl = urlPath
	//cr.db.Create(&post)
	return post
}

func filterContentPsg(s string) string {

	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScriptPsg(doc)
	removeDivPsg(doc)
	html.Render(&content, doc)
	body, err := GetBody(doc)
	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)
	return body
}

func removeScriptPsg(n *html.Node) {

	// if note is script tag
	if n.Type == html.ElementNode && (n.Data == "script" || n.Data == "iframe" || n.Data == "blockquote") {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		CollectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}
	}

	if n.Type == html.ElementNode && n.Data == "img" {
		imageUrl := ""
		alt := ""
		for _, attr := range n.Attr {

			if attr.Key == "src" {
				imageUrl = attr.Val

			}
			if attr.Key == "alt" {
				alt = attr.Val

			}

		}
		if !strings.Contains(imageUrl, "https://en.psg.fr") {
			imageUrl = "https://en.psg.fr" + imageUrl
			imageUrl = ClearImageUrl(imageUrl)
		}

		attr := []html.Attribute{}
		attr = append(attr, html.Attribute{"", "src", imageUrl})
		attr = append(attr, html.Attribute{"", "alt", alt})
		nodeLink := html.Node{
			Type: html.ElementNode,
			Data: "img",
			Attr: attr,
		}

		n.Parent.InsertBefore(&nodeLink, n.NextSibling)

		n.Parent.RemoveChild(n)
	}

	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScriptPsg(c)
	}
}

func removeDivPsg(n *html.Node) {

	if n.Type == html.ElementNode && n.Data == "ul" {
		n.Parent.RemoveChild(n)
		return
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeDivPsg(c)
	}
}
