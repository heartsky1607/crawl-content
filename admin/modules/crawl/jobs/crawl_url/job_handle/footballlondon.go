package crawl_url_handle

import (
	"bytes"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"log"
	crawlmodel "sea/admin/modules/crawl/model"
	"strings"
)

func (*crawlJob) CrawlContentFootballLondon(urlPath string) crawlmodel.CrawlPostCreate {
	var post crawlmodel.CrawlPostCreate
	c := colly.NewCollector(
		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)

	//infoCollector := c.Clone()
	//fmt.Println("start collect")
	c.OnHTML("article > div.headline-with-subtype > h1", func(e *colly.HTMLElement) {
		//fmt.Println(e.Text)
		post.Title = e.Text
	})
	c.OnHTML("article > div.article-wrapper > div.content-column > figure > div.img-container", func(e *colly.HTMLElement) {
		post.FeatureImage = e.ChildAttr("img", "src")
		//fmt.Println(post.FeatureImage)
	})

	c.OnHTML("article > div.article-wrapper > div.content-column > div.article-body", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContentFootballLondon(post.Content)
		content = RemoveEmptyTags(content)
		post.Content = content
		//fmt.Println(content)
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})

	c.Visit(urlPath)
	post.OriginUrl = urlPath
	//fmt.Println(post)
	//cr.db.Create(&post)
	return post
}

func filterContentFootballLondon(s string) string {
	index := strings.Index(s, "READ NEXT")
	if index > 0 {
		s = s[0:index]
	}

	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScriptFootballLondon(doc)
	removeDivFootballLondon(doc)
	html.Render(&content, doc)
	body, err := GetBody(doc)
	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)

	return body
}

func removeScriptFootballLondon(n *html.Node) {

	// if note is script tag
	if n.Type == html.ElementNode && n.Data == "script" {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		CollectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}

	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScriptFootballLondon(c)
	}
}
func removeDivFootballLondon(n *html.Node) {

	if n.Type == html.ElementNode && n.Data == "b" {
		text := &bytes.Buffer{}

		CollectText(n, text)
		if strings.Contains(strings.ToUpper(text.String()), "READ MORE") {
			n.Parent.Parent.RemoveChild(n.Parent)
		}
		return
	}

	if n.Type == html.ElementNode && n.Data == "div" {
		text := &bytes.Buffer{}

		CollectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		}

	}
	if n.Type == html.ElementNode && n.Data == "span" {
		for _, span := range n.Attr {
			if span.Key == "itemprop" && span.Val == "author" {
				n.Parent.RemoveChild(n)
				return
			}
		}
	}

	if n.Type == html.ElementNode && n.Data == "meta" {
		n.Parent.RemoveChild(n)
		return
	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeDivFootballLondon(c)
	}
}
