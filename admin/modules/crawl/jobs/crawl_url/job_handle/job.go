package crawl_url_handle

import (
	"context"
	"log"
	"reflect"
	crawlmodel "sea/admin/modules/crawl/model"
	"sea/admin/modules/crawl/repository"
	crawlUsecase "sea/admin/modules/crawl/usecase"

	"sea/core/module/sky/queue"
	"sea/core/services/redis"
)

type crawlJob struct {
	name      string
	queueName string
	queue     queue.Queue
	redis     *redis.RedisClient
	repo      crawlUsecase.SavePostCrawlStorage
}

func NewCrawJob(qn string) (*crawlJob, error) {
	q, err := queue.NewQueue(qn)
	if err != nil {
		return nil, err
	}
	r := repository.NewCrawlRepository()

	return &crawlJob{
		queueName: qn,
		redis:     redis.Redis,
		queue:     q,
		repo:      crawlUsecase.NewSavePostCrawlUseCase(r),
	}, nil

}

func (job *crawlJob) GetName() string {
	return job.name
}

func (job *crawlJob) Execute(domain string, postUrl string) error {
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred: execute job crawl  ", err)
		}
	}()
	result := LoadConfiguration(domain)
	f := result.CrawlContent
	var post crawlmodel.CrawlPostCreate
	p := reflect.ValueOf(job).MethodByName(f).Call([]reflect.Value{reflect.ValueOf(postUrl)})
	for _, v := range p {
		i := v.Interface()
		post = i.(crawlmodel.CrawlPostCreate)
	}
	post.SiteUrl = domain
	err := job.repo.SavePost(&post)
	return err
}
func (job *crawlJob) Dispatch(ctx context.Context, args ...interface{}) error {
	return nil
}

func (job *crawlJob) GetQueueName() string {
	return job.queueName
}

func (job *crawlJob) FallBack(...interface{}) error {
	return nil
}
