package crawl_url_handle

import (
	"bytes"
	"fmt"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"log"
	crawlmodel "sea/admin/modules/crawl/model"
	"strings"
)

func (*crawlJob) CrawlContentSkySports(urlPath string) crawlmodel.CrawlPostCreate {
	//fmt.Println(urlPath)
	var post crawlmodel.CrawlPostCreate
	c := colly.NewCollector(
		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)

	//infoCollector := c.Clone()
	//fmt.Println("start collect")
	c.OnHTML("div.section-wrap > div.sdc-article-header > div.sdc-article-header__wrap > div.sdc-article-header__main > div.sdc-article-header__titles > h1 >span", func(e *colly.HTMLElement) {
		fmt.Println(e.Text)
		post.Title = e.Text
	})
	c.OnHTML("div.section-wrap > div.sdc-article-image > figure > div.sdc-article-image__wrapper", func(e *colly.HTMLElement) {
		post.FeatureImage = e.ChildAttr("img", "src")
		fmt.Println(post.FeatureImage)
	})

	c.OnHTML("div.section-wrap > div.sdc-site-layout-wrap  > div.sdc-site-layout > div.sdc-site-layout__col > div.sdc-article-body", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContentSkySports(post.Content)
		content = RemoveEmptyTags(content)
		post.Content = content
		fmt.Println(content)
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})
	c.Visit(urlPath)
	post.OriginUrl = urlPath
	//cr.db.Create(&post)
	return post
}

func filterContentSkySports(s string) string {

	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScriptSkySports(doc)
	removeDivSkySports(doc)
	html.Render(&content, doc)
	body, err := GetBody(doc)
	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)

	return body
}

func removeScriptSkySports(n *html.Node) {

	// if note is script tag
	if n.Type == html.ElementNode && n.Data == "script" {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		CollectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}

	}

	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScriptSkySports(c)
	}
}
func removeDivSkySports(n *html.Node) {

	if n.Type == html.ElementNode && n.Data == "div" {
		for _, attr := range n.Attr {
			if attr.Key == "class" && strings.Contains(attr.Val, "sdc-article-tweet") {
				n.Parent.RemoveChild(n)
				return
			}
		}
	}
	if n.Type == html.ElementNode && n.Data == "img" {
		imageUrl := ""
		alt := ""
		for _, attr := range n.Attr {

			if attr.Key == "src" {
				imageUrl = attr.Val

			}
			if attr.Key == "alt" {
				alt = attr.Val
			}
		}
		imageUrl = ClearImageUrl(imageUrl)
		attr := []html.Attribute{}
		attr = append(attr, html.Attribute{"", "src", imageUrl})
		attr = append(attr, html.Attribute{"", "alt", alt})
		nodeLink := html.Node{
			Type: html.ElementNode,
			Data: "img",
			Attr: attr,
		}
		n.Parent.InsertBefore(&nodeLink, n.NextSibling)
		n.Parent.RemoveChild(n)
	}
	if n.Type == html.ElementNode && n.Data == "ul" {
		n.Parent.RemoveChild(n)
		return
	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeDivSkySports(c)
	}
}
