package crawl_url_handle

import (
	"bytes"
	"fmt"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"log"
	crawlmodel "sea/admin/modules/crawl/model"

	"strings"
)

func (*crawlJob) CrawlContentAcMilanInfo(urlPath string) crawlmodel.CrawlPostCreate {
	fmt.Println(urlPath)
	var post crawlmodel.CrawlPostCreate
	c := colly.NewCollector(
		colly.MaxDepth(3),
	)
	//infoCollector := c.Clone()
	//fmt.Println("start collect")
	c.OnHTML("article > header > h1", func(e *colly.HTMLElement) {
		fmt.Println(e.Text)
		post.Title = e.Text
	})
	c.OnHTML("article > div > figure.single-thumb", func(e *colly.HTMLElement) {
		post.FeatureImage = e.ChildAttr("img", "src")
	})

	c.OnHTML("article > div.entry-content", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContentAcmilanInfo(post.Content)
		content = RemoveEmptyTags(content)
		fmt.Println(content)
		post.Content = content
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})

	c.Visit(urlPath)
	post.OriginUrl = urlPath
	return post
}

func filterContentAcmilanInfo(s string) string {
	index := strings.Index(s, "READ MORE")
	if index > 0 {
		s = s[0:index]
	}
	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScriptAcMilanInfo(doc)
	html.Render(&content, doc)
	body, err := GetBody(doc)
	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)

	return body
}

func removeScriptAcMilanInfo(n *html.Node) {
	// if note is script tag
	if n.Type == html.ElementNode && n.Data == "script" {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		CollectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}
	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScriptAcMilanInfo(c)
	}
}
