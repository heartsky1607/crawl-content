package crawl_url_handle

type PayloadCrawlContent struct {
	Domain  string `json:"domain"`
	PostUrl string `json:"postUrl"`
}
