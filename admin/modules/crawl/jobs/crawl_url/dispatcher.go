package crawl_url

import (
	"encoding/json"
	"fmt"
	"log"
	crawlUrlHandle "sea/admin/modules/crawl/jobs/crawl_url/job_handle"
	"sea/core/module/sky/queue"
)

type crawlDispatcher struct {
	Workers    []*crawlContentWorker
	WorkerPool chan chan crawlUrlHandle.PayloadCrawlContent
	Queue      queue.Queue
	QueueName  string
}

func NewCrawlDispatcher(pool chan chan crawlUrlHandle.PayloadCrawlContent, qn string) (*crawlDispatcher, error) {

	workQueue, err := queue.NewQueue(qn)
	if err != nil {
		return nil, err
	}
	return &crawlDispatcher{
		WorkerPool: pool,
		Queue:      workQueue,
		QueueName:  qn,
	}, nil
}

func (disp *crawlDispatcher) Initialize(maxWorker int) {
	// Now, create all of our workers.
	for i := 0; i < maxWorker; i++ {
		fmt.Println("Starting worker crawl", i+1)
		worker := NewCrawlContentWorker(i+1, disp.WorkerPool, disp.QueueName)
		disp.Workers = append(disp.Workers, worker)
		worker.Initialize()
	}
	go func() {
		messages, err := disp.Queue.GetChannel().Consume(
			disp.Queue.GetName(), // queue name
			"",                   // consumer
			true,                 // auto-ack
			false,                // exclusive
			false,                // no local
			false,                // no wait
			nil,                  // arguments
		)
		if err != nil {
			log.Println(err)
		}
		for message := range messages {
			var data crawlUrlHandle.PayloadCrawlContent
			err = json.Unmarshal([]byte(message.Body), &data)
			if err != nil {
				fmt.Println(err, data)
				continue
			}

			go func() {
				worker := <-disp.WorkerPool
				worker <- data
			}()

		}

	}()
}
