package crawl_url

import (
	crawlUrlHandle "sea/admin/modules/crawl/jobs/crawl_url/job_handle"
)

type crawlContentWorker struct {
	Id            int
	WorkerPool    chan chan crawlUrlHandle.PayloadCrawlContent
	WorkerChannel chan crawlUrlHandle.PayloadCrawlContent
	QuitChannel   chan bool
	queueName     string
}

func NewCrawlContentWorker(id int, pool chan chan crawlUrlHandle.PayloadCrawlContent, queueName string) *crawlContentWorker {
	return &crawlContentWorker{
		Id:            id,
		WorkerPool:    pool,
		WorkerChannel: make(chan crawlUrlHandle.PayloadCrawlContent, 5),
		QuitChannel:   make(chan bool),
		queueName:     queueName,
	}
}

func (worker *crawlContentWorker) Initialize() {
	go func() {
		for {
			worker.WorkerPool <- worker.WorkerChannel
			select {
			case data := <-worker.WorkerChannel:
				job, err := crawlUrlHandle.NewCrawJob(worker.queueName)
				if err != nil {
					worker.Stop()
				}
				job.Execute(data.Domain, data.PostUrl)
			case <-worker.QuitChannel:
				return
			}
		}
	}()
}

func (worker *crawlContentWorker) Stop() {
	go func() {
		worker.QuitChannel <- true
	}()
}
