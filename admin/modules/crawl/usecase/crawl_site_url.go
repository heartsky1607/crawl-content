package crawlusecase

type CrawlSiteUrlStorage interface {
	CrawlPostLink(string) error
}

type crawlSiteUrlCase struct {
	repo CrawlSiteUrlStorage
}

func NewCrawlSiteUrlCase(repo CrawlSiteUrlStorage) *crawlSiteUrlCase {
	return &crawlSiteUrlCase{
		repo: repo,
	}
}

func (uc *crawlSiteUrlCase) CrawlPostLinkFromOrigin(siteUrl string) error {
	return uc.repo.CrawlPostLink(siteUrl)
}
