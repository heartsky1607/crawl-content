package crawlusecase

import (
	"asyncjob/component/hasher"
	"errors"
	"fmt"
	crawlmodel "sea/admin/modules/crawl/model"
)

type SavePostCrawlStorage interface {
	SavePost(create *crawlmodel.CrawlPostCreate) error
}

type savePostCrawlUseCase struct {
	repo SavePostCrawlStorage
	site crawlmodel.OriginSiteModel
}

func NewSavePostCrawlUseCase(repo SavePostCrawlStorage) *savePostCrawlUseCase {
	return &savePostCrawlUseCase{
		repo: repo,
	}
}

func (biz *savePostCrawlUseCase) SavePost(post *crawlmodel.CrawlPostCreate) error {
	if err := post.Validate(); err != true {
		fmt.Println("data invalid")
		return errors.New("data invalid")
	}
	post.UniqueID = hasher.NewMd5Hash().Hash(post.OriginUrl)
	if err := biz.repo.SavePost(post); err != nil {
		return err
	}
	return nil
}
