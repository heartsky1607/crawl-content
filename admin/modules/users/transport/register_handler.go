package userstransport

import (
	"github.com/gofiber/fiber/v2"
	"net/http"
	UsersModel "sea/admin/modules/users/model"
	UsersUseCase "sea/admin/modules/users/usecase"
)

func (handler *userHandler) RegisterHandler(ctx *fiber.Ctx) error {

	biz := UsersUseCase.NewRegisterUc(handler.userRepo)
	var data UsersModel.UserModelCreate
	err := ctx.BodyParser(&data)
	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}

	err = biz.RegisterAccount(ctx, &data)
	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"success": true,
	})
	return nil

}
