package userstransport

import (
	"github.com/gofiber/fiber/v2"
	"net/http"
	UsersModel "sea/admin/modules/users/model"
	UsersUseCase "sea/admin/modules/users/usecase"
)

func (handler *userHandler) Authentication(ctx *fiber.Ctx) error {

	var data UsersModel.UserModelLogin
	if err := ctx.BodyParser(&data); err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	biz := UsersUseCase.NewAuthenticateUserCase(handler.userRepo)
	token, err := biz.Login(ctx, &data)
	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"success": token,
	})
	return nil
}
