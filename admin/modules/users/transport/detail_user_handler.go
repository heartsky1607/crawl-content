package userstransport

import (
	"github.com/gofiber/fiber/v2"
	"net/http"
	UsersUseCase "sea/admin/modules/users/usecase"
)

func (handler *userHandler) GetDetailUser(ctx *fiber.Ctx) error {
	biz := UsersUseCase.NewDetailUserCase(handler.userRepo)
	id, err := ctx.ParamsInt("id", 1)
	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}

	result, err := biz.GetDetailUser(ctx, id)
	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"data": result,
	})
	return nil
}
