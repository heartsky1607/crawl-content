package userstransport

import (
	"github.com/gofiber/fiber/v2"
	"net/http"
	UsersModel "sea/admin/modules/users/model"
	usersusecase "sea/admin/modules/users/usecase"
)

func (handler *userHandler) UpdateUserDetail(ctx *fiber.Ctx) error {
	id, err := ctx.ParamsInt("id")
	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	var data UsersModel.UserModelUpdate
	if err := ctx.BodyParser(&data); err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	biz := usersusecase.NewUpdateUserCase(handler.userRepo)
	err = biz.UpdateUser(ctx, id, &data)
	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"success": true,
	})
	return nil
}
