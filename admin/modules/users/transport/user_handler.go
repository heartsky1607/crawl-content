package userstransport

import (
	"gorm.io/gorm"
	usersrepository "sea/admin/modules/users/repository"
)

type userHandler struct {
	db       *gorm.DB
	userRepo *usersrepository.UserRepository
}

func NewUserHandle(db *gorm.DB) *userHandler {
	return &userHandler{
		db:       db,
		userRepo: usersrepository.NewUserRepository(db),
	}
}
