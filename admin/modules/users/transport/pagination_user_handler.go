package userstransport

import "C"
import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"net/http"
	"sea/admin/commons"
	usersmodel "sea/admin/modules/users/model"
	UsersUseCase "sea/admin/modules/users/usecase"
)

func (handler *userHandler) PaginationUserHandler(ctx *fiber.Ctx) error {

	biz := UsersUseCase.NewListUserCase(handler.userRepo)
	page := ctx.QueryInt("page", 1)
	limit := ctx.QueryInt("limit", 10)
	var paging = commons.Pagination{Limit: limit, Page: page}

	var roles []string
	err := json.Unmarshal([]byte(ctx.Query("roles", "[]")), &roles)

	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}

	filter := usersmodel.NewUserFilter()
	filter.SetRole(roles)
	result, err := biz.GetUserPagination(ctx, filter, &paging)
	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{
			"error": err.Error(),
		})
		return nil
	}
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"data":       result,
		"filter":     filter,
		"pagination": paging,
	})
	return nil

}
