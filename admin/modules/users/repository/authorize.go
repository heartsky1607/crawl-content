package usersrepository

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	authjwt "sea/admin/components/auth/jwt"
	UsersModel "sea/admin/modules/users/model"
)

func (repo *UserRepository) VerifyToken(ctx *fiber.Ctx, token string, provider authjwt.Provider) error {
	data, err := provider.Validate(token)
	if err != nil {
		fmt.Println(err)
		return err
	}
	var user UsersModel.UserModelDetail
	userId := data.UserId()
	role := data.Role()
	err = repo.db.Table(UsersModel.UserModelDetail{}.TableName()).First(&user, "id=? and role=?", userId, role).Error
	if err != nil {
		fmt.Println(err)
		return err
	}
	ctx.Locals("user", &user)

	return nil
}
