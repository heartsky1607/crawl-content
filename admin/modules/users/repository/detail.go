package usersrepository

import (
	"errors"
	"github.com/gofiber/fiber/v2"
	UsersModel "sea/admin/modules/users/model"
)

func (repo *UserRepository) GetDetailUser(ctx *fiber.Ctx, id int) (*UsersModel.UserModelDetail, error) {

	data := ctx.Locals("user")
	if data != nil {
		return data.(*UsersModel.UserModelDetail), nil
	}

	return nil, errors.New("request is incorrect")

}
