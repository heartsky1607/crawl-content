package usersrepository

import (
	"github.com/gofiber/fiber/v2"
	"sea/admin/commons"
	usersmodel "sea/admin/modules/users/model"
)

func (repo *UserRepository) GetListUser(ctx *fiber.Ctx, filter usersmodel.UserFilter, paging *commons.Pagination, moreKeys ...string) (*[]usersmodel.UserModelDetail, error) {
	var data []usersmodel.UserModelDetail
	db := repo.db.Table(usersmodel.UserModelDetail{}.TableName()).Count(&paging.Total)
	if paging.Page >= 0 && paging.Limit >= 0 {
		limit := paging.Limit
		offset := (paging.Page - 1) * limit
		db.Offset(offset).Limit(limit)
	}
	if len(filter.GetRole()) >= 0 {
		db.Where("role in ?", filter.GetRole())
	}
	err := db.Find(&data)
	return &data, err.Error
}
