package usersrepository

import (
	"errors"
	"github.com/gofiber/fiber/v2"
	"os"
	authjwt "sea/admin/components/auth/jwt"
	comphash "sea/admin/components/hash"
	usersmodel "sea/admin/modules/users/model"
)

func (repo *UserRepository) GetUserToken(ctx *fiber.Ctx, user *usersmodel.UserModel) (string, error) {
	payload := authjwt.JwtPayload{
		Uid:   int(user.ID),
		URole: user.Role,
	}
	provider := authjwt.NewJwtProvider(os.Getenv("JWT_SECRET_KEY"))
	token, err := provider.Generate(payload, 60*24*365)
	if err != nil {
		return "", err
	}
	return token.GetToken(), nil
}
func (repo *UserRepository) AuthenticatedUser(ctx *fiber.Ctx, data *usersmodel.UserModelLogin) (*usersmodel.UserModel, error) {
	var user usersmodel.UserModel
	err := repo.db.Table(usersmodel.UserModel{}.TableName()).Where("username=?", data.Username).First(&user)
	if err.Error != nil {
		return nil, err.Error
	}
	hashedPass := comphash.NewMd5Hash().Hash(data.Password + user.Salt)
	if hashedPass != user.Password {
		return nil, errors.New("info is incorrect")
	}

	return &user, nil
}
