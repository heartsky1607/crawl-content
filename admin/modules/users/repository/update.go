package usersrepository

import (
	"github.com/gofiber/fiber/v2"
	UsersModel "sea/admin/modules/users/model"
)

func (repo *UserRepository) UpdateUserDetail(ctx *fiber.Ctx, id int, data *UsersModel.UserModelUpdate) error {
	err := repo.db.Table(UsersModel.UserModelUpdate{}.TableName()).Where("id=?", id).Updates(data).Error
	if err != nil {
		return err
	}
	return nil
}
