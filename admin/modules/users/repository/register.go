package usersrepository

import (
	"github.com/gofiber/fiber/v2"
	usersmodel "sea/admin/modules/users/model"
)

func (repo *UserRepository) Register(ctx *fiber.Ctx, data *usersmodel.UserModelCreate) error {
	if err := repo.db.Create(data).Error; err != nil {
		return err
	}
	return nil
}
