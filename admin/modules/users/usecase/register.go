package usersusecase

import (
	"github.com/gofiber/fiber/v2"
	"sea/admin/commons"
	comphash "sea/admin/components/hash"
	usersmodel "sea/admin/modules/users/model"
)

type RegisterStorage interface {
	Register(ctx *fiber.Ctx, data *usersmodel.UserModelCreate) error
}

type registerUc struct {
	repo RegisterStorage
}

func NewRegisterUc(repo RegisterStorage) *registerUc {
	return &registerUc{repo: repo}
}

func (uc *registerUc) RegisterAccount(ctx *fiber.Ctx, data *usersmodel.UserModelCreate) error {
	if err := data.Validate(); err != nil {
		return err
	}
	salt := commons.GenSalt(50)
	data.Password = comphash.NewMd5Hash().Hash(data.Password + salt)
	data.Salt = salt
	err := uc.repo.Register(ctx, data)
	if err != nil {
		return err
	}
	return nil
}
