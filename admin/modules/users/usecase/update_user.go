package usersusecase

import (
	"github.com/gofiber/fiber/v2"
	UsersModel "sea/admin/modules/users/model"
)

type UpdateUserStorage interface {
	UpdateUserDetail(*fiber.Ctx, int, *UsersModel.UserModelUpdate) error
}

type updateUserCase struct {
	repo UpdateUserStorage
}

func NewUpdateUserCase(repo UpdateUserStorage) *updateUserCase {
	return &updateUserCase{repo: repo}
}

func (uc *updateUserCase) UpdateUser(ctx *fiber.Ctx, id int, data *UsersModel.UserModelUpdate) error {
	return uc.repo.UpdateUserDetail(ctx, id, data)
}
