package usersusecase

import (
	"github.com/gofiber/fiber/v2"
	"sea/admin/commons"
	usersmodel "sea/admin/modules/users/model"
)

type ListUserStorage interface {
	GetListUser(ctx *fiber.Ctx, filter usersmodel.UserFilter, paging *commons.Pagination, moreKeys ...string) (*[]usersmodel.UserModelDetail, error)
}

type listUserCase struct {
	repo ListUserStorage
}

func NewListUserCase(repo ListUserStorage) *listUserCase {
	return &listUserCase{repo: repo}
}

func (biz *listUserCase) GetAllUser(ctx *fiber.Ctx, filter usersmodel.UserFilter, paging *commons.Pagination) (*[]usersmodel.UserModelDetail, error) {
	paging.Limit = -1
	paging.Page = -1
	data, err := biz.repo.GetListUser(ctx, filter, paging)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (biz *listUserCase) GetUserPagination(ctx *fiber.Ctx, filter usersmodel.UserFilter, paging *commons.Pagination) (*[]usersmodel.UserModelDetail, error) {
	filter.Verify()
	paging.Verify()
	data, err := biz.repo.GetListUser(ctx, filter, paging)
	if err != nil {
		return nil, err
	}
	return data, nil
}
