package usersusecase

import (
	"github.com/gofiber/fiber/v2"
	usersmodel "sea/admin/modules/users/model"
)

type LoginStorage interface {
	GetUserToken(ctx *fiber.Ctx, model *usersmodel.UserModel) (string, error)
	AuthenticatedUser(ctx *fiber.Ctx, data *usersmodel.UserModelLogin) (*usersmodel.UserModel, error)
}

type authenticateUserCase struct {
	repo LoginStorage
}

func NewAuthenticateUserCase(repo LoginStorage) *authenticateUserCase {
	return &authenticateUserCase{
		repo: repo,
	}
}

func (uc *authenticateUserCase) Login(ctx *fiber.Ctx, data *usersmodel.UserModelLogin) (string, error) {
	err := data.Validate()
	if err != nil {
		return "", err
	}
	result, err := uc.repo.AuthenticatedUser(ctx, data)
	if err != nil {
		return "", err
	}

	token, err := uc.repo.GetUserToken(ctx, result)
	if err != nil {
		return "", err
	}

	return token, nil
}
