package usersusecase

import (
	"github.com/gofiber/fiber/v2"
	authjwt "sea/admin/components/auth/jwt"
	UsersModel "sea/admin/modules/users/model"
)

type AuthorizeStorage interface {
	GetDetailUser(*fiber.Ctx, int) (*UsersModel.UserModelDetail, error)
	VerifyToken(*fiber.Ctx, string, authjwt.Provider) error
}
