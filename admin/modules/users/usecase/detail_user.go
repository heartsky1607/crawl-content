package usersusecase

import (
	"github.com/gofiber/fiber/v2"
	UsersModel "sea/admin/modules/users/model"
)

type DetailUserStorage interface {
	GetDetailUser(*fiber.Ctx, int) (*UsersModel.UserModelDetail, error)
}

type detailUserCase struct {
	repo DetailUserStorage
}

func NewDetailUserCase(repo DetailUserStorage) *detailUserCase {
	return &detailUserCase{
		repo: repo,
	}
}

func (repo *detailUserCase) GetDetailUser(ctx *fiber.Ctx, id int) (*UsersModel.UserModelDetail, error) {
	result, err := repo.repo.GetDetailUser(ctx, id)
	if err != nil {
		return nil, err
	}
	return result, nil
}
