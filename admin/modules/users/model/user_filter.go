package usersmodel

import (
	"sea/admin/commons"
)

type UserFilter interface {
	GetRole() []string
	SetRole([]string) error
	SetRoleDefault() error
	Verify()
}

type userFilter struct {
	Role []string `json:"role" form:"role"`
}

const (
	ROLE_ADMIN = "admin"
	ROLE_USER  = "user"
)

var VALID_ROLES = []string{ROLE_ADMIN, ROLE_USER}

func NewUserFilter() *userFilter {
	return &userFilter{}
}

func (f *userFilter) Verify() {
	roles := f.GetRole()

	if len(roles) > 0 {
		var newRole []string
		for _, v := range roles {
			role := v
			if commons.CheckItemInSlice(VALID_ROLES, role) >= 0 {
				newRole = append(newRole, role)
			}
		}
		if len(newRole) > 0 {
			f.SetRole(newRole)

		} else {
			f.SetRoleDefault()
		}
	} else {
		f.SetRoleDefault()
	}

}

func (f *userFilter) GetRole() []string {
	return f.Role
}

func (f *userFilter) SetRoleDefault() error {
	f.Role = []string{"user"}
	return nil
}

func (f *userFilter) SetRole(roles []string) error {
	f.Role = roles
	return nil
}
