package usersmodel

import (
	"errors"
	"sea/admin/commons"
	"strings"
)

type UserModel struct {
	commons.SqlModel
	Username string `json:"username" gorm:"column:username;"`
	Password string `json:"password" gorm:"column:password;"`
	Salt     string `json:"salt" gorm:"column:salt;"`
	Role     string `json:"role" gorm:"column:role;"`
}

func (UserModel) TableName() string {
	return "users"
}

type UserModelCreate struct {
	Username string `json:"username" gorm:"column:username;"`
	Password string `json:"password" gorm:"column:password;"`
	Salt     string `json:"salt" gorm:"column:salt;"`
	Role     string `json:"role" gorm:"column:role;"`
}

func (UserModelCreate) TableName() string {
	return UserModel{}.TableName()
}

func (user UserModelCreate) Validate() error {
	user.Username = strings.TrimSpace(user.Username)
	user.Password = strings.TrimSpace(user.Password)
	user.Role = strings.TrimSpace(user.Role)
	if len(user.Username) == 0 || len(user.Password) == 0 || len(user.Role) == 0 {
		return errors.New("user data invalid")
	}
	return nil
}

type UserModelUpdate struct {
	Username *string `json:"username" gorm:"column:username;"`
	Password *string `json:"password" gorm:"column:password;"`
	Role     *string `json:"role" gorm:"column:role;"`
}

func (UserModelUpdate) TableName() string {
	return UserModel{}.TableName()
}

type UserModelLogin struct {
	Username string `json:"username" gorm:"column:username;"`
	Password string `json:"password" gorm:"column:password;"`
}

func (user UserModelLogin) Validate() error {

	user.Username = strings.TrimSpace(user.Username)
	user.Password = strings.TrimSpace(user.Password)
	if len(user.Username) == 0 || len(user.Password) == 0 {
		return errors.New("request invalid")
	}
	return nil
}

type UserModelDetail struct {
	commons.SqlModel
	Username string `json:"username" gorm:"column:username;"`
	Role     string `json:"role" gorm:"column:role;"`
}

func (UserModelDetail) TableName() string {
	return UserModel{}.TableName()
}
