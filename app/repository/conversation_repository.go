package repository

import (
	"fmt"
	"socket_app/sevice/mongodb"
	"socket_app/sevice/redis"

	"go.mongodb.org/mongo-driver/mongo"
)

type conversationRepository struct {
	redis  *redis.RedisClient
	client *mongo.Client
}

func NewConversationRepo(client *mongo.Client) *conversationRepository {
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		mongodb.ConnectDb()

	}
	return &conversationRepository{
		redis:  redis.Redis,
		client: client,
	}
}
