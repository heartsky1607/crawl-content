package repository

import (
	"context"
	"fmt"
	"os"
	"socket_app/model"
	"socket_app/sevice/mongodb"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (repo *conversationRepository) StoreConversation(conversation *model.ConversationCreate) error {
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		mongodb.ConnectDb()

	}

	db := repo.client.Database(os.Getenv("MONGO_DATABASE"))
	collection := db.Collection(conversation.Id)

	filter := bson.M{"id": conversation.Id}
	update := bson.M{
		"$set": bson.M{"id": conversation.Id, "user_id": conversation.UserId, "merchant_id": conversation.MerchantId, "last_active": conversation.LastActive, "active_status": conversation.ActiveStatus},
	}

	upsert := true
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
		Upsert:         &upsert,
	}

	result := collection.FindOneAndUpdate(context.TODO(), filter, update, &opt)
	if result.Err() != nil {
		return result.Err()
	}

	collectionList := db.Collection("conversations")
	resultList := collectionList.FindOneAndUpdate(context.TODO(), filter, update, &opt)
	if resultList.Err() != nil {
		return result.Err()
	}

	doc := bson.M{}
	decodeErr := result.Decode(&doc)
	fmt.Println("data conversation", doc)

	return decodeErr

	// // create a bson.D object

}
