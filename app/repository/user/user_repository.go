package userRepo

import (
	"fmt"
	"socket_app/sevice/mongodb"
	"socket_app/sevice/redis"

	"go.mongodb.org/mongo-driver/mongo"
)

type userRepository struct {
	redis  *redis.RedisClient
	client *mongo.Client
}

func NewUserRepo(client *mongo.Client) *userRepository {
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		mongodb.ConnectDb()

	}
	return &userRepository{
		redis:  redis.Redis,
		client: client,
	}
}
