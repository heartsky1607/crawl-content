package messageRepo

import (
	"context"
	"fmt"
	"os"
	"socket_app/model"
	"socket_app/sevice/mongodb"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (repo *messageRepository) AddMessage(roomId string, message *model.MessageCreate) error {
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		mongodb.ConnectDb()

	}
	collection := repo.client.Database(os.Getenv("MONGO_DATABASE")).Collection(roomId + "_messages")
	// user, err := bson.Marshal(message)
	// if err != nil {
	// 	panic(err)
	// }
	// // // insert the bson object using InsertOne()
	// result, err := collection.InsertOne(context.TODO(), user)
	// // // check for errors in the insertion
	// if err != nil {
	// 	panic(err)
	// }
	// // display the id of the newly inserted object
	filter := bson.M{"id": message.Id}

	update := bson.M{
		"$set": bson.M{"id": message.Id, "content": message.Content, "created_at": message.CreatedAt, "userinfo": message.Userinfo, "type": message.Type, "success": true},
	}

	upsert := true
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
		Upsert:         &upsert,
	}

	result := collection.FindOneAndUpdate(context.TODO(), filter, update, &opt)
	if result.Err() != nil {
		panic(result.Err())
	}

	doc := bson.M{}
	decodeErr := result.Decode(&doc)
	fmt.Println(os.Getenv("MONGO_DATABASE"), roomId+"_messages")
	return decodeErr
}
