package messageRepo

import (
	"fmt"
	"socket_app/sevice/mongodb"
	"socket_app/sevice/redis"

	"go.mongodb.org/mongo-driver/mongo"
)

type messageRepository struct {
	redis  *redis.RedisClient
	client *mongo.Client
}

func NewMessageRepo(client *mongo.Client) *messageRepository {
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		mongodb.ConnectDb()

	}
	return &messageRepository{
		redis:  redis.Redis,
		client: client,
	}
}
