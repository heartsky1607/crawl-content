package repository

import (
	"context"
	"fmt"
	"os"
	"socket_app/sevice/mongodb"

	"go.mongodb.org/mongo-driver/bson"
)

func (repo *conversationRepository) DeleteConversation(name string) error {
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		mongodb.ConnectDb()

	}

	collection := repo.client.Database(os.Getenv("MONGO_DATABASE")).Collection("conversations")

	filter := bson.M{"id": name}

	result, err := collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		return err
	}

	repo.client.Database(os.Getenv("MONGO_DATABASE")).Collection(name).Drop(context.TODO())
	repo.client.Database(os.Getenv("MONGO_DATABASE")).Collection(name + "_messages").Drop(context.TODO())

	fmt.Println("data delete", result.DeletedCount)
	return nil

	// // create a bson.D object
	// user, err := bson.Marshal(data)
	// if err != nil {
	// 	panic(err)
	// }
	// // insert the bson object using InsertOne()
	// result, err := collection.InsertOne(context.TODO(), user)
	// // check for errors in the insertion
	// if err != nil {
	// 	panic(err)
	// }
	// // display the id of the newly inserted object
	// fmt.Println(result.InsertedID)

}
