package roomRepo

import (
	"fmt"
	"socket_app/sevice/mongodb"
	"socket_app/sevice/redis"

	"go.mongodb.org/mongo-driver/mongo"
)

type roomRepository struct {
	redis  *redis.RedisClient
	client *mongo.Client
}

func NewRoomRepo(client *mongo.Client) *roomRepository {
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		mongodb.ConnectDb()

	}
	return &roomRepository{
		redis:  redis.Redis,
		client: client,
	}
}
