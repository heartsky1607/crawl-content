package repository

import (
	"context"
	"fmt"
	"os"
	"socket_app/model"
	"socket_app/sevice/mongodb"

	"go.mongodb.org/mongo-driver/bson"
)

func (repo *conversationRepository) SaveConversationMessages(name string, data []model.Message) error {
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		mongodb.ConnectDb()

	}
	collection := repo.client.Database(os.Getenv("MONGO_DATABASE")).Collection(name + "_messages")
	message := data[0]
	message.Success = true
	user, err := bson.Marshal(message)
	if err != nil {
		panic(err)
	}
	// // insert the bson object using InsertOne()
	result, err := collection.InsertOne(context.TODO(), user)
	// // check for errors in the insertion
	if err != nil {
		panic(err)
	}
	// // display the id of the newly inserted object
	fmt.Println(os.Getenv("MONGO_DATABASE"), name+"_messages", result.InsertedID)
	return err
}
