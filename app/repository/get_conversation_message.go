package repository

import (
	"context"
	"fmt"
	"log"
	"os"
	"socket_app/model"
	"socket_app/sevice/mongodb"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (repo *conversationRepository) GetMessagePaginator(roomId string, page int, limit int) ([]model.Message, error) {
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		mongodb.ConnectDb()

	}
	var result []model.Message

	collection := repo.client.Database(os.Getenv("MONGO_DATABASE")).Collection(roomId + "_messages")
	l := int64(limit)
	skip := int64(page*limit - limit)
	fOpt := options.FindOptions{Limit: &l, Skip: &skip}
	fOpt.SetSort(bson.D{{Key: "created_at", Value: -1}})
	// options := options.Find()
	// options.SetSort(bson.D{{Key: "content", Value: 1}})
	// options.SetSkip(l)
	// options.SetLimit(skip)

	curr, err := collection.Find(context.TODO(), bson.D{{}}, &fOpt)
	if err != nil {
		return result, err
	}

	for curr.Next(context.TODO()) {
		var el model.Message
		if err := curr.Decode(&el); err != nil {
			log.Println(err)
		}
		result = append(result, el)
	}

	return result, nil

	// // create a bson.D object
	// user, err := bson.Marshal(data)
	// if err != nil {
	// 	panic(err)
	// }
	// // insert the bson object using InsertOne()
	// result, err := collection.InsertOne(context.TODO(), user)
	// // check for errors in the insertion
	// if err != nil {
	// 	panic(err)
	// }
	// // display the id of the newly inserted object
	// fmt.Println(result.InsertedID)

}
