package usercase

import (
	"socket_app/model"
)

type SaveMessageStorage interface {
	SaveConversationMessages(name string, data []model.Message) error
}

type saveMessageUc struct {
	repo SaveMessageStorage
}

func NewSaveMessageUc(repo SaveMessageStorage) *saveMessageUc {
	return &saveMessageUc{
		repo: repo,
	}
}

func (biz *saveMessageUc) SaveMessage(conversationId string, messages []model.Message) error {

	for _, message := range messages {
		if err := message.Validate(); err != nil {
			return err
		}
	}
	return biz.repo.SaveConversationMessages(conversationId, messages)
}
