package userusecase

type DetailUserStorage interface {
	DetailUser(string) error
}

type detailUserUc struct {
	repo DetailUserStorage
}

func NewDetailUserUc(repo DetailUserStorage) *detailUserUc {
	return &detailUserUc{
		repo: repo,
	}
}

func (biz *detailUserUc) DetailUser(id string) error {
	return biz.repo.DetailUser(id)
}
