package userusecase

import "socket_app/model"

type EditUserStorage interface {
	EditUser(*model.UserUpdate) error
}

type editUserUc struct {
	repo EditUserStorage
}

func NewEditUserUc(repo EditUserStorage) *editUserUc {
	return &editUserUc{
		repo: repo,
	}
}

func (biz *editUserUc) Store(user *model.UserUpdate) error {
	return biz.repo.EditUser(user)
}
