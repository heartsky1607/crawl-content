package userusecase

import "socket_app/model"

type ListUserStorage interface {
	ListUser(page int, limit int) ([]model.User, error)
}

type listUserUc struct {
	repo ListUserStorage
}

func NewListUserUc(repo ListUserStorage) *listUserUc {
	return &listUserUc{
		repo: repo,
	}
}

func (biz *listUserUc) ListUser(page int, limit int) ([]model.User, error) {
	return biz.repo.ListUser(page, limit)
}
