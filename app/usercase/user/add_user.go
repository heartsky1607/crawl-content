package userusecase

import "socket_app/model"

type AddUserStorage interface {
	AddUser(*model.InstitutionCreate) error
}

type addUserUc struct {
	repo AddUserStorage
}

func NewAddUserUc(repo AddUserStorage) *addUserUc {
	return &addUserUc{
		repo: repo,
	}
}

func (biz *addUserUc) AddUser(institution *model.InstitutionCreate) error {
	return biz.repo.AddUser(institution)
}
