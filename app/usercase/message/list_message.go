package messageusecace

import "socket_app/model"

type ListMesasgeStorage interface {
	ListMessage(int, int) ([]model.Message, error)
}

type listMessageUc struct {
	repo ListMesasgeStorage
}

func NewListMessageUc(repo ListMesasgeStorage) *listMessageUc {
	return &listMessageUc{
		repo: repo,
	}
}

func (biz *listMessageUc) ListMessage(page int, limit int) ([]model.Message, error) {
	return biz.repo.ListMessage(page, limit)
}
