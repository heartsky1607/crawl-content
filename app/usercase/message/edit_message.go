package messageusecace

import "socket_app/model"

type EditMessageStorage interface {
	AddMesage(*model.MessageCreate) error
}

type editMessageUc struct {
	repo EditMessageStorage
}

func NewEditMesageUc(repo EditMessageStorage) *editMessageUc {
	return &editMessageUc{
		repo: repo,
	}
}

func (biz *editMessageUc) Store(msg *model.MessageCreate) error {
	return biz.repo.AddMesage(msg)
}
