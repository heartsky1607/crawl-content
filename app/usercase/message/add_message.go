package messageusecace

import "socket_app/model"

type AddMessageStorage interface {
	AddMessage(string, *model.MessageCreate) error
}

type addMessageUc struct {
	repo AddMessageStorage
}

func NewAddMessageUc(repo AddMessageStorage) *addMessageUc {
	return &addMessageUc{
		repo: repo,
	}
}

func (biz *addMessageUc) AddMessage(roomId string, msg *model.MessageCreate) error {
	return biz.repo.AddMessage(roomId, msg)
}
