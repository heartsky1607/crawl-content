package messageusecace

import "socket_app/model"

type DetailMessageStorage interface {
	DetailMessage(string) (model.Message, error)
}

type addMesageUc struct {
	repo DetailMessageStorage
}

func NewDetailMessageUc(repo DetailMessageStorage) *addMesageUc {
	return &addMesageUc{
		repo: repo,
	}
}

func (biz *addMesageUc) DetailMessage(id string) (model.Message, error) {
	return biz.repo.DetailMessage(id)
}
