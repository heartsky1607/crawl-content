package usercase

import (
	"socket_app/model"
)

type GetMessageStorage interface {
	GetMessagePaginator(string, int, int) ([]model.Message, error)
}

type getMessageConversationUc struct {
	repo GetMessageStorage
}

func NewGetMessageConversationUc(repo GetMessageStorage) *getMessageConversationUc {
	return &getMessageConversationUc{
		repo: repo,
	}
}

func (biz *getMessageConversationUc) GetPaginator(roomId string, page int, limit int) ([]model.Message, error) {
	return biz.repo.GetMessagePaginator(roomId, page, limit)
}
