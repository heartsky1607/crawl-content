package dispatcher

import (
	"socket_app/sky/provider"
)

type Worker struct {
	Id            int
	WorkerPool    chan chan provider.Payload
	WorkerChannel chan provider.Payload
	QuitChannel   chan bool
	Job           provider.SkyJob
}

func NewWorker(id int, pool chan chan provider.Payload, job provider.SkyJob) *Worker {
	return &Worker{
		Id:            id,
		WorkerPool:    pool,
		WorkerChannel: make(chan provider.Payload, 5),
		QuitChannel:   make(chan bool),
		Job:           job,
	}
}

func (worker *Worker) Initialize() {
	go func() {
		for {
			worker.WorkerPool <- worker.WorkerChannel
			select {
			case data := <-worker.WorkerChannel:
				func() {
					worker.Job.Execute(data)
				}()
			case <-worker.QuitChannel:
				return
			}
		}
	}()
}

func (worker *Worker) Stop() {
	go func() {
		worker.QuitChannel <- true
	}()
}
