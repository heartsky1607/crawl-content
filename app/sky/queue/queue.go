package queue

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Queue struct {
	queueName       string
	logger          *log.Logger
	connection      *amqp.Connection
	channel         *amqp.Channel
	done            chan bool
	notifyConnClose chan *amqp.Error
	notifyChanClose chan *amqp.Error
	notifyConfirm   chan amqp.Confirmation
	isReady         bool
	connectUrl      string
}

const (
	reconnectDelay = 50 * time.Second

	reInitDelay = 2 * time.Second

	resendDelay = 5 * time.Second
)

var (
	errNotConnected  = errors.New("not connected to a server")
	errAlreadyClosed = errors.New("already closed: not connected to the server")
	errShutdown      = errors.New("client is shutting down")
)

func NewQueue(queueName string) *Queue {
	addr := os.Getenv("AMQP_SERVER_URL")
	client := Queue{
		logger:     log.New(os.Stdout, "", log.LstdFlags),
		queueName:  queueName,
		done:       make(chan bool),
		connectUrl: addr,
	}

	go client.handleReconnect()
	return &client
}

func (client *Queue) handleReconnect() {
	for {
		client.isReady = false
		client.logger.Println("RAbbit MQ Attempting to connect")

		conn, err := client.Connect()

		if err != nil {

			client.logger.Println("RAbbit MQ Failed to connect. Retrying...")

			select {
			case <-client.done:
				return
			case <-time.After(reconnectDelay):
			}

			continue
		}

		if done := client.handleReInit(conn); done {
			break
		}
	}
}

func (q *Queue) Connect() (*amqp.Connection, error) {

	conn, err := amqp.Dial(q.connectUrl)

	if err != nil {
		return nil, err
	}

	q.changeConnection(conn)
	return conn, nil
}
func (client *Queue) handleReInit(conn *amqp.Connection) bool {
	for {
		client.isReady = false

		err := client.init(conn)
		fmt.Println(err)

		if err != nil {
			client.logger.Println("Rabbit MQ Failed to initialize channel. Retrying...")

			select {
			case <-client.done:
				return true
			case <-time.After(reInitDelay):

			}
			continue
		}

		select {
		case <-client.done:
			return true
		case <-client.notifyConnClose:
			client.logger.Println("Rabbit MQ Connection closed. Reconnecting...")
			return false
		case <-client.notifyChanClose:
			client.logger.Println("Rabbit MQ Channel closed. Re-running init...")
		}
	}
}

// init will initialize channel & declare queue
func (client *Queue) init(conn *amqp.Connection) error {
	ch, err := conn.Channel()

	if err != nil {
		return err
	}

	err = ch.Confirm(false)

	if err != nil {
		return err
	}
	_, err = ch.QueueDeclare(
		client.queueName,
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		return err
	}

	client.changeChannel(ch)
	client.isReady = true
	client.logger.Println("Rabbit MQ Setup done!")
	return nil
}

func (client *Queue) changeConnection(connection *amqp.Connection) {
	client.connection = connection
	client.notifyConnClose = make(chan *amqp.Error, 1)
	client.connection.NotifyClose(client.notifyConnClose)
}

func (client *Queue) changeChannel(channel *amqp.Channel) {
	client.channel = channel
	client.notifyChanClose = make(chan *amqp.Error, 1)
	client.notifyConfirm = make(chan amqp.Confirmation, 1)
	client.channel.NotifyClose(client.notifyChanClose)
	client.channel.NotifyPublish(client.notifyConfirm)
}

func (q *Queue) GetName() string {
	return q.queueName
}

func (q *Queue) GetStatus() bool {
	return q.isReady
}

// Close will cleanly shut down the channel and connection.
func (client *Queue) CloseConnection() error {
	if !client.isReady {
		return errAlreadyClosed
	}
	close(client.done)
	err := client.channel.Close()
	if err != nil {
		return err
	}
	err = client.connection.Close()
	if err != nil {
		return err
	}

	client.isReady = false
	return nil
}

func (q *Queue) GetChannel() *amqp.Channel {
	return q.channel
}

func (q *Queue) SendMessage(ctx context.Context, msg string) error {
	if !q.isReady {
		q.handleReconnect()
		time.Sleep(2 * time.Second)
		return q.SendMessage(ctx, msg)
	}
	for {
		err := q.UnsafePush([]byte(msg))
		if err != nil {
			q.logger.Println("Push failed. Retrying...")
			q.handleReconnect()
			select {
			case <-q.done:
				return errShutdown
			case <-time.After(resendDelay):

			}

			continue
		}
		confirm := <-q.notifyConfirm
		if confirm.Ack {
			// q.logger.Printf("Push confirmed [%d]!", confirm.DeliveryTag)
			return nil
		}
	}

}

func (client *Queue) UnsafePush(data []byte) error {
	if !client.isReady {
		return errNotConnected
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	return client.channel.PublishWithContext(
		ctx,
		"",
		client.queueName,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        data,
		},
	)
}

func (client *Queue) Consume() (<-chan amqp.Delivery, error) {
	if !client.isReady {
		return nil, errNotConnected
	}

	if err := client.channel.Qos(
		1,     // prefetchCount
		0,     // prefrechSize
		false, // global
	); err != nil {
		return nil, err
	}

	return client.channel.Consume(
		client.queueName,
		"",    // Consumer
		false, // Auto-Ack
		false, // Exclusive
		false, // No-local
		false, // No-Wait
		nil,   // Args
	)
}
