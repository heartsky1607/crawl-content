package transport

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"socket_app/jobs"
	"socket_app/model"
	"socket_app/sky/provider"

	"github.com/gin-gonic/gin"
)

func (handler *handler) SendBroadcastMessage(c *gin.Context) {
	roomId := c.Param("roomId")
	check := false
	if roomId == "" {
		check = true
	}
	var f map[string]interface{}
	err2 := c.ShouldBindJSON(&f)
	if err2 != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": http.ErrBodyNotAllowed,
			"data": err2.Error(),
		})
		fmt.Println("errror bin json", err2.Error())
		return
	}

	txt, err := json.Marshal(f)
	if err != nil {
		fmt.Println(string(txt))
		panic(err)
	}
	if check {
		fmt.Println(string(txt))
	}

	datacontent, ok := f["content"]
	if !ok {
		fmt.Println("error parse content")
		return
	}

	userInfo, ok := f["userinfo"]
	if !ok {
		fmt.Println("error parse info")
		return
	}

	//convert to string to add to redis stream
	txtdataContent, _ := json.Marshal(datacontent)
	f["content"] = string(txtdataContent)
	txtInfo, _ := json.Marshal(userInfo)
	f["userinfo"] = string(txtInfo)

	type Content struct {
		TokenChat      string                 `json:"tokenChat"`
		OpenTime       string                 `json:"openTime"`
		BetInfo        map[string]interface{} `json:"betInfo"`
		UserInfo       map[string]any         `json:"userInfo"`
		ChatBetContent []map[string]any       `json:"chatBetContent"`
	}

	var content Content
	err = json.Unmarshal(txtdataContent, &content)
	if err != nil {
		fmt.Println("content is wrong", string(txtdataContent), err.Error())
		return
	}
	msgCreate := model.MessageCreate{}
	err = json.Unmarshal(txt, &msgCreate)
	if err != nil {
		fmt.Println("message wrong", string(txt), err.Error())
		return
	}

	key := "room_message_" + roomId
	if msgCreate.Type == "2" && check {
		fmt.Println(f["content"])
	}
	err = handler.Redis().AddItemToStream(key, "", f)
	if err != nil {
		fmt.Println("error add to redis", err.Error())
		return
	}

	err = msgCreate.Validate()
	if err == nil {
		go func(roomId string, data model.MessageCreate) {
			defer func() {
				if r := recover(); r != nil {
					log.Println("recovery ", r)
				}
			}()
			ct := context.Background()
			var payload map[string]interface{}
			stringData, err := json.Marshal(data)
			if err != nil {
				fmt.Println(err)
				return
			}

			json.Unmarshal(stringData, &payload)
			payload["room"] = roomId
			item := provider.Payload{Data: payload, Delay: 0}
			err = jobs.MessageJob.Dispatch(ct, item)
			if err != nil {
				fmt.Println("error send to queue , ", err)
				panic(err)
			}
			//  else {
			// 	// fmt.Println("send data success to queue")
			// }
		}(roomId, msgCreate)
		handler.Socket().SendBroadcast(string(txt), roomId)
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusOK,
			"data": string(txt),
		})
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"erro": err.Error(),
		})
		fmt.Println("err data from json")
		panic(err)

	}

}
