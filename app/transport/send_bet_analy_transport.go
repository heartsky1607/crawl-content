package transport

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func (handler *handler) SendDataAnalyBet(c *gin.Context) {
	var f map[string]interface{}
	err2 := c.ShouldBindJSON(&f)
	if err2 != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": http.ErrBodyNotAllowed,
			"data": err2.Error(),
		})
		return
	}

	dateString := fmt.Sprint(f["openTime"])
	dateString = strings.Replace(dateString, ":", "", -1)
	dateString = strings.Replace(dateString, "-", "", -1)
	dateString = strings.Replace(dateString, " ", "", -1)

	gameInfo := f["betInfo"]
	var info map[string]interface{}
	infoByte, err := json.Marshal(gameInfo)
	if err != nil {
		panic(err)

	}
	json.Unmarshal(infoByte, &info)

	gameIdTx, ok := info["gameId"]
	var gameId string
	if ok {
		gameId = fmt.Sprint(gameIdTx)
	}

	data, _ := json.Marshal(f)
	exp, err := strconv.ParseFloat(dateString, 64)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": http.ErrBodyNotAllowed,
			"data": err2.Error(),
		})

		fmt.Println(dateString, f)
		panic(err)
	}

	err = handler.Redis().AddItemToSortedSet("bet_game_"+gameId, string(data), exp)
	if err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})

}
