package transport

import (
	"socket_app/sevice/redis"
	"socket_app/socket"

	"go.mongodb.org/mongo-driver/mongo"
)

type handler struct {
	redisClient *redis.RedisClient
	mongoClient *mongo.Client
	socket      *socket.HandleSocket
}

func NewHandler(redisClient *redis.RedisClient, mongoclient *mongo.Client) *handler {
	socketTransport := socket.Initialize(redisClient)
	return &handler{redisClient: redisClient, mongoClient: mongoclient, socket: socketTransport}
}

func (handler *handler) Redis() *redis.RedisClient {
	return handler.redisClient
}

func (handler *handler) Db() *mongo.Client {
	return handler.mongoClient
}

func (handler *handler) Socket() *socket.HandleSocket {
	return handler.socket
}
