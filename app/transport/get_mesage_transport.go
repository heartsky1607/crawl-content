package transport

import (
	"net/http"
	"socket_app/repository"
	"socket_app/usercase"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func (handler *handler) GetListMessage(c *gin.Context) {
	page, _ := strconv.Atoi(c.Param("page"))
	roomId := c.Param("roomId")
	connectionId := c.Param("connectionId")
	blockKey := roomId + "_" + connectionId

	blockCheck, _ := handler.Redis().Get(blockKey)

	if blockCheck == "test" {
		c.JSON(http.StatusBadGateway, gin.H{
			"code":    http.StatusBadGateway,
			"message": "authorization action",
		})
		return
	} else {
		handler.Redis().SetExpire(blockKey, "test", 2*time.Second)
	}

	limit, _ := strconv.Atoi(c.Param("limit"))
	repo := repository.NewConversationRepo(handler.Db())
	biz := usercase.NewGetMessageConversationUc(repo)
	data, err := biz.GetPaginator(roomId, page, limit)
	if err != nil {
		c.JSON(http.StatusBadGateway, gin.H{
			"code":    http.StatusBadGateway,
			"message": err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusOK,
			"data": data,
		})
	}

}
