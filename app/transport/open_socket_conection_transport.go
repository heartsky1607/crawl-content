package transport

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func (handler *handler) OpenSocketConnection(c *gin.Context) {
	roomId := c.Param("roomId")
	connectId := c.Param("connectionId")
	if len(roomId) == 0 || len(roomId) > 32 {
		c.JSON(http.StatusOK, gin.H{
			"code": http.ErrBodyNotAllowed,
			"data": "empty room",
		})
		return
	}
	if len(connectId) == 0 || len(connectId) > 32 {
		c.JSON(http.StatusOK, gin.H{
			"code": http.ErrBodyNotAllowed,
			"data": "incorrect connection",
		})
		return
	}
	if strings.HasPrefix(roomId, "skyx") {
		err := handler.Redis().AddItemToSet("list_room_available", roomId)
		if err != nil {
			panic(err)
		}
		err = handler.Redis().AddItemToSet("room_connection_list_"+roomId, connectId)
		if err != nil {
			panic(err)
		}
	}

	if handler.Socket().VerifyConnection(roomId, connectId) {
		if handler.redisClient.CheckItemInSet("list_room_available", roomId) && handler.Redis().CheckItemInSet("room_connection_list_"+roomId, connectId) {
			handler.Socket().StartConversation(c.Writer, c.Request, roomId, connectId)
		} else {
			if !handler.redisClient.CheckItemInSet("list_room_available", roomId) {
				// data, _ := handler.Redis().GetAllItemFromSet("list_room_available")
				fmt.Println("room is unvailable", roomId)
			}
			if !handler.Redis().CheckItemInSet("room_connection_list_"+roomId, connectId) {
				// dataconnection, _ := handler.Redis().GetAllItemFromSet("room_connection_list_" + roomId)
				// fmt.Println(dataconnection)
				fmt.Println("connection is unvailable", roomId)
			}

		}
	} else {
		fmt.Println("user already connect to room", roomId, connectId)
	}

}
