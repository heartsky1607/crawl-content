package transport

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func (handler *handler) GetLastMessage(c *gin.Context) {
	roomId := c.Param("roomId")
	connectionId := c.Param("connectionId")
	blockKey := roomId + "_" + connectionId

	blockCheck, _ := handler.Redis().Get(blockKey)
	if blockCheck != "" {
		c.JSON(http.StatusBadGateway, gin.H{
			"code":    http.StatusBadGateway,
			"message": "authorization action",
		})
		return
	} else {
		handler.Redis().SetExpire(blockKey, "test value", 500*time.Millisecond)
	}

	key := "room_message_" + roomId
	data, err := handler.Redis().GetItemFromStream(key)
	listData := make(map[string]map[string]interface{})

	for _, msg := range data {
		dataItem := msg.Values
		id, ok := dataItem["id"]
		if !ok {
			continue
		}
		msgId := fmt.Sprint(id)
		if len(msgId) > 0 {
			listData[msgId] = dataItem
		}
	}

	if err != nil {
		c.JSON(http.StatusBadGateway, gin.H{
			"code":    http.StatusBadGateway,
			"message": err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusOK,
			"data": listData,
		})
	}

}
