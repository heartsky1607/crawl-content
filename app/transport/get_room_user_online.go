package transport

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (handler *handler) GetCountRoomUserOnline(c *gin.Context) {
	roomId := c.Param("roomId")
	total := handler.Socket().CountOnline(roomId)
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": total,
	})

}
