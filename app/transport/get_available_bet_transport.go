package transport

import (
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func (handler *handler) GetBetAvailable(c *gin.Context) {
	gameId := c.Param("gameId")
	currentTime := time.Now()
	// lastWrite := currentTime.Format("2006-01-02 14:10:05")
	lastWrite := currentTime.String()
	lastWrite = lastWrite[:19]
	lastWrite = strings.Replace(lastWrite, ":", "", -1)
	lastWrite = strings.Replace(lastWrite, "-", "", -1)
	lastWrite = strings.Replace(lastWrite, " ", "", -1)

	endTime := time.Now().Add(36 * time.Hour)
	limit := endTime.String()
	limit = limit[:19]
	limit = strings.Replace(limit, ":", "", -1)
	limit = strings.Replace(limit, "-", "", -1)
	limit = strings.Replace(limit, " ", "", -1)

	handler.Redis().AddItemToSet("list_game_available", gameId)

	records, err := handler.Redis().GetItemFromSortedSetByPoint("bet_game_"+gameId, lastWrite, limit)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": http.ErrBodyNotAllowed,
			"data": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": records,
	})

}
