package main

import (
	"context"
	"fmt"
	"net/http"
	"net/rpc"
	"os"
	"socket_app/rpcserver"
	"socket_app/sevice/mongodb"
	"socket_app/sevice/redis"
	"time"

	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var queueMessage chan string

func main() {
	err := godotenv.Load("./.env")
	if err != nil {
		fmt.Println("env ", err.Error(), "error")
	}
	ctx := context.Context(context.TODO())
	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGO_URL")))
	if err != nil {
		panic(err)
	}
	if err := mongoClient.Ping(ctx, readpref.Primary()); err != nil {
		panic(err)
	}
	mongodb.MongoClient = mongoClient
	fmt.Println("connect mongo success")
	// queueMessage := make(chan provider.Payload, 100)

	queueMessage = make(chan string, 10000)
	for i := 0; i < 5; i++ {
		go handleData(queueMessage, fmt.Sprintf("%d", i))
		fmt.Println("init worker")
	}

	handle := rpcserver.NewHandleMessageRpc()
	handle.LoadConsumer(5)

	_, err = redis.Initialize()

	if err != nil {
		fmt.Println("error redis")
		panic(err)
	}

	msg := new(MessageSave)
	msg.Set(handle)
	rpc.Register(msg)
	rpc.HandleHTTP()

	err = http.ListenAndServe(":8099", nil)
	if err != nil {
		panic(err)
	}

}

func handleData(c chan string, name string) {
	for val := range c {
		fmt.Println(name, "handle data", val)
		time.Sleep(1 * time.Second)
	}

}

type MessageSave struct {
	handle *rpcserver.HandleMessageRpc
}

func (t *MessageSave) Set(handle *rpcserver.HandleMessageRpc) {
	t.handle = handle
	return
}

func (t *MessageSave) SaveMessage(payload string, reply *int) error {
	// var data provider.Payload
	// err := json.Unmarshal([]byte(payload), &data)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	fmt.Println(payload)
	// t.handle.Send(payload)
	*reply = 1
	return nil
}
