package middlewares

import (
	"fmt"
	"net/http"
	"socket_app/token"

	"github.com/gin-gonic/gin"
)

func JwtAuthMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, "Unauthorized")
			c.Abort()
			fmt.Println("error token", err.Error())
			return
		}
		c.Next()
	}
}
