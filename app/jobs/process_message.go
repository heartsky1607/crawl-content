package jobs

import (
	"encoding/json"
	"fmt"
	"socket_app/model"
	messageRepo "socket_app/repository/message"
	"socket_app/sevice/mongodb"
	"socket_app/sevice/redis"
	messageusecace "socket_app/usercase/message"

	"socket_app/sky/provider"
	"socket_app/sky/queue"
)

type processMessageJob struct {
	name string
	// queue *queue.Queue
	redis *redis.RedisClient
}

var MessageProcessJob *processMessageJob

func NewProcessMessageJob(queue *queue.Queue) *processMessageJob {

	MessageProcessJob = &processMessageJob{
		name: "ProcessMessage",
		// queue: queue,
		redis: redis.Redis,
	}
	return MessageProcessJob
}

func (job *processMessageJob) GetName() string {
	return job.name
}

func (job *processMessageJob) Execute(payload provider.Payload) error {
	if payload.Data["content"] == nil {
		fmt.Println("invalid message")
		return nil
	}
	repo := messageRepo.NewMessageRepo(mongodb.MongoClient)
	biz := messageusecace.NewAddMessageUc(repo)
	var message model.MessageCreate
	stringcontent, err := json.Marshal(payload.Data)
	if err != nil {
		fmt.Println(err)
		return err
	}
	json.Unmarshal(stringcontent, &message)
	if message.Validate() != nil {
		fmt.Println(err)
		return err
	}
	roomId := fmt.Sprint(payload.Data["room"])
	// fmt.Println("receive message", roomId, messages)
	biz.AddMessage(roomId, &message)
	return nil
}

func (job *processMessageJob) GetQueue() *queue.Queue {
	return nil

}

func (job *processMessageJob) FallBack(...interface{}) error {
	return nil
}
