package jobs

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"socket_app/model"
	messageRepo "socket_app/repository/message"
	"socket_app/sevice/mongodb"
	"socket_app/sevice/redis"
	messageusecace "socket_app/usercase/message"

	"socket_app/sky/provider"
	"socket_app/sky/queue"
)

type saveMessageJob struct {
	name  string
	queue *queue.Queue
	redis *redis.RedisClient
}

var MessageJob *saveMessageJob

func NewSaveMessageJob(queue *queue.Queue) *saveMessageJob {

	MessageJob = &saveMessageJob{
		name:  "SaveMessage",
		queue: queue,
		redis: redis.Redis,
	}
	return MessageJob
}

func (job *saveMessageJob) GetName() string {
	return job.name
}

func (job *saveMessageJob) Execute(payload provider.Payload) error {
	if payload.Data["content"] == nil {
		fmt.Println("invalid message")
		return nil
	}
	repo := messageRepo.NewMessageRepo(mongodb.MongoClient)
	biz := messageusecace.NewAddMessageUc(repo)
	var message model.MessageCreate
	stringcontent, err := json.Marshal(payload.Data)
	if err != nil {
		fmt.Println(err)
		return err
	}
	json.Unmarshal(stringcontent, &message)
	if message.Validate() != nil {
		fmt.Println(err)
		return err
	}
	roomId := fmt.Sprint(payload.Data["room"])
	// fmt.Println("receive message", roomId, message)
	biz.AddMessage(roomId, &message)
	return nil
}
func (job *saveMessageJob) Dispatch(ctx context.Context, payload provider.Payload) error {

	data, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	err = job.queue.SendMessage(ctx, string(data))
	if err != nil {
		return err
	}

	return nil
}

func (job *saveMessageJob) GetQueue() *queue.Queue {
	return job.queue
}

func (job *saveMessageJob) FallBack(...interface{}) error {
	return nil
}

func GetMD5Hash(text string) string {
	fmt.Println(text)
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}
