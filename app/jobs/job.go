package jobs

import (
	"context"
	"encoding/json"
	"fmt"
	"socket_app/sevice/redis"
	"socket_app/sky/provider"
	"socket_app/sky/queue"
)

type testJob struct {
	name  string
	queue *queue.Queue
	redis *redis.RedisClient
}

var TestJob *testJob

func NewTestJob(queue *queue.Queue) *testJob {

	TestJob = &testJob{
		name:  "Test",
		queue: queue,
		redis: redis.Redis,
	}
	return TestJob
}

func (job *testJob) GetName() string {
	return job.name
}

func (job *testJob) Execute(payload provider.Payload) error {
	fmt.Println(payload)

	return nil
}
func (job *testJob) Dispatch(ctx context.Context, payload provider.Payload) error {

	data, err := json.Marshal(payload)
	if err != nil {
		return err
	}
	q := job.queue

	err = q.SendMessage(ctx, string(data))
	if err != nil {
		return err
	}

	return nil
}

func (job *testJob) GetQueue() *queue.Queue {
	return job.queue
}

func (job *testJob) FallBack(...interface{}) error {
	return nil
}
