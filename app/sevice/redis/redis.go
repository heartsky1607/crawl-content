package redis

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

type RedisClient struct {
	client *redis.Client
}

var Redis *RedisClient
var RedisError error

func init() {
	Redis, RedisError = Initialize()

}

func Initialize() (*RedisClient, error) {
	redishost := os.Getenv("REDIS_HOST")
	redisport := os.Getenv("REDIS_PORT")
	redisDb, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	redispass := os.Getenv("REDIS_PASS")
	if len(redisport) == 0 {
		redisport = "6379"
	}
	if err != nil {
		redisDb = 0
	}
	client := redis.NewClient(&redis.Options{
		Addr:     redishost + ":" + redisport,
		Password: redispass,
		DB:       redisDb,
	})
	result, err := client.Ping().Result()

	if err != nil {
		fmt.Println(err)
		return nil, err

	}
	fmt.Println("redis ping", result)

	return &RedisClient{
		client: client,
	}, nil

}

type RedisInteface interface {
	Get()
	Set()
	Remove()
	AddItemToList()
	AddItemToSet()
	AddItemToHash()
	AddItemToSortedSet()
	GetItemFromSet()
	GetItemFromHash()
	CheckItemInSet()
	AddItemToStream()
	GetItemFromStream()
}

func (r *RedisClient) Get(key string) (string, error) {
	return r.client.Get(key).Result()

}

func (r *RedisClient) Set(key string, value string) error {
	return r.client.Set(key, value, 0).Err()
}

func (r *RedisClient) SetExpire(key string, value string, expiration time.Duration) error {
	return r.client.Set(key, value, expiration).Err()
}

func (r *RedisClient) Remove(key string) error {
	err := r.client.Del(key).Err()
	return err
}

func (r *RedisClient) AddItemToSet(key string, val string) error {

	return r.client.SAdd(key, val).Err()
}

func (r *RedisClient) CheckItemInSet(key string, item string) bool {
	okay, err := r.client.SIsMember(key, item).Result()
	if err != nil {
		fmt.Println(err)
		return false
	}
	return okay
}

func (r *RedisClient) PopItemFromSet(key string) (string, error) {
	return r.client.SPop(key).Result()
}

func (r *RedisClient) GetAllItemFromSet(key string) ([]string, error) {
	return r.client.SMembers(key).Result()
}

func (r *RedisClient) AddItemToHash(key string, data map[string]string) error {
	for prop, val := range data {
		err := r.client.HSet(key, prop, val).Err()
		if err != nil {
			return err
		}
	}
	return nil
}

func (r *RedisClient) GetItemFromHash(key string, prop string) (string, error) {
	return r.client.HGet(key, prop).Result()
}

func (r *RedisClient) GetAllItemFromHash(key string) (map[string]string, error) {
	return r.client.HGetAll(key).Result()
}

func (r *RedisClient) AddItemToStream(key string, prop string, data map[string]interface{}) error {

	param := redis.XAddArgs{Stream: key, MaxLen: 100, Values: data}
	if len(prop) > 0 {
		param.ID = prop
	}
	err := r.client.XAdd(&param).Err()
	return err
}

func (r *RedisClient) GetItemFromStream(key string) ([]redis.XMessage, error) {
	vals, err := r.client.XRange(key, "-", "+").Result()
	return vals, err
}

func (r *RedisClient) AddItemToSortedSet(key string, data string, point float64) error {
	param := redis.Z{Score: point, Member: data}
	return r.client.ZAdd(key, param).Err()

}

func (r *RedisClient) GetItemFromSortedSetByPoint(key string, start string, end string) ([]string, error) {
	parram := redis.ZRangeBy{Min: start, Max: end}
	data, rr := r.client.ZRangeByScore(key, parram).Result()
	return data, rr

}

func (r *RedisClient) RemoveItemFromSortedSetByPoint(key string, start string, end string) error {
	rr := r.client.ZRemRangeByScore(key, start, end).Err()
	return rr

}
