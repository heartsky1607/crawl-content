package mongodb

import (
	"context"
	"fmt"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var MongoClient *mongo.Client

func ConnectDb() error {
	ctx := context.TODO()
	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGO_URL")))
	if err != nil {
		return (err)
	}
	if err := mongoClient.Ping(ctx, readpref.Primary()); err != nil {
		return (err)
	}
	MongoClient = mongoClient
	return nil
}

func Verify() bool {
	ctx := context.TODO()
	if err := MongoClient.Ping(ctx, readpref.Primary()); err != nil {
		fmt.Println(err)
		return false
	}
	return true
}
