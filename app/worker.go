package main

import (
	"context"
	"fmt"
	"os"
	"socket_app/jobs"
	"socket_app/sevice/mongodb"
	"socket_app/sevice/redis"
	"socket_app/sky/dispatcher"
	"socket_app/sky/queue"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func main() {
	router := gin.New()

	err := godotenv.Load("./.env")
	if err != nil {
		panic(err)
	}
	redisCli, err := redis.Initialize()

	ctx := context.Context(context.TODO())

	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGO_URL")))
	if err != nil {
		panic(err)
	}
	if err := mongoClient.Ping(ctx, readpref.Primary()); err != nil {
		panic(err)
	}
	mongodb.MongoClient = mongoClient
	queueMessage := queue.NewQueue("save_message")
	<-time.After(2 * time.Second)

	// var forever chan struct{}
	job := jobs.NewSaveMessageJob(queueMessage)
	dispath := dispatcher.NewDispatcher(job)
	dispath.Initialize(10)
	defer queueMessage.CloseConnection()

	ticker := time.NewTicker(5 * time.Hour)
	quit := make(chan bool)
	go func() {
		for {
			select {
			case <-ticker.C:
				currentTime := time.Now()
				// lastWrite := currentTime.Format("2006-01-02 14:10:05")
				lastWrite := currentTime.String()
				lastWrite = lastWrite[:19]
				lastWrite = strings.Replace(lastWrite, ":", "", -1)
				lastWrite = strings.Replace(lastWrite, "-", "", -1)
				lastWrite = strings.Replace(lastWrite, " ", "", -1)

				listKey, err := redisCli.GetAllItemFromSet("list_game_available")
				if err == nil {
					for _, key := range listKey {
						redisCli.RemoveItemFromSortedSetByPoint("bet_game_"+key, "0", lastWrite)
						fmt.Println("remove last", lastWrite)
					}

				} else {
					fmt.Println(err)
				}
				// do stuff
				//

				fmt.Println("run schedule", lastWrite)
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
	router.Run(":8066")

	// <-forever
	// Serve on port :8080, fudge yeah hardcoded port
}
