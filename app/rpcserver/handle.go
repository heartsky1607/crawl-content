package rpcserver

import (
	"fmt"
	"socket_app/jobs"
	"socket_app/sky/provider"
)

type HandleMessageRpc struct {
	dataChannel chan provider.Payload
}

func NewHandleMessageRpc() *HandleMessageRpc {
	dataChannel := make(chan provider.Payload, 1000)
	return &HandleMessageRpc{
		dataChannel: dataChannel,
	}

}

func (handler *HandleMessageRpc) LoadConsumer(count int) {
	for i := 0; i < count; i++ {
		go func() {
			fmt.Println("start worker")
			for val := range handler.dataChannel {
				fmt.Println("handle data", val)
				jobs.MessageProcessJob.Execute(val)
			}
		}()
	}

}
func (handler *HandleMessageRpc) Send(msg provider.Payload) {
	handler.dataChannel <- msg
	fmt.Println("sends to channel", msg)
}
