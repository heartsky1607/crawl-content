package model

import "time"

type User struct {
	Id              string    `json:"id" bson:"id"`
	Amount          int64     `json:"amount" bson:"amount"`
	AvailableAmount int64     `json:"available_amount" bson:"available_amount"`
	PendingAmount   int64     `json:"pending_amount" bson:"pending_amount"`
	Name            string    `json:"name" bson:"name"`
	CreatedAt       time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time `json:"updated_at" bson:"updated_at"`
}

type UserUpdate struct {
	Id              string    `json:"id" bson:"id"`
	Amount          int64     `json:"amount" bson:"amount"`
	AvailableAmount int64     `json:"available_amount" bson:"available_amount"`
	PendingAmount   int64     `json:"pending_amount" bson:"pending_amount"`
	Name            string    `json:"name" bson:"name"`
	CreatedAt       time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time `json:"updated_at" bson:"updated_at"`
}
