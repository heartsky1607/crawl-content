package model

import (
	"errors"
	"strings"
	"time"
)

type Conversation struct {
	Id             string    `json:"id" bson:"id"`
	InstitutionId  string    `json:"institution_id" bson:"institution_id"`
	Messages       []Message `json:"messages"  bson:"messages"`
	OpenTime       time.Time `json:"open_time" bson:"open_time" `
	CloseTime      time.Time `json:"close_time" bson:"close_time"`
	Code           string    `json:"code" bson:"code"`
	Result         string    `json:"result" bson:"result"`
	Member         string    `json:"member" bson:"member"`
	LivestreamUrl  string    `json:"live_stream_url"  bson:"live_stream_url" `
	LivestreamTime time.Time `json:"live_stream_time" bson:"live_stream_time"`
	Winner         string    `json:"winner" bson:"winner"`
	AvailableMem   string    `json:"available_mem" bson:"available_mem"`
	PendingMem     string    `json:"pending_mem" bson:"pending_mem"`
	CreatedAt      time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt      time.Time `json:"updated_at" bson:"updated_at"`
}

type ConversationCreate struct {
	Id           string `json:"id" bson:"id"`
	UserId       string `json:"user_id"  bson:"user_id"`
	MerchantId   string `json:"merchant_id" bson:"merchant_id"`
	LastActive   string `json:"last_active" bson:"last_active"`
	ActiveStatus bool   `json:"active_status" bson:"active_status"`
}

type ConversationUpdate struct {
	LastActive   string `json:"last_active" bson:"last_active"`
	ActiveStatus bool   `json:"active_status" bson:"active_status"`
}

func (conv ConversationCreate) Validate() error {
	conv.Id = strings.TrimSpace(conv.Id)
	conv.MerchantId = strings.TrimSpace(conv.MerchantId)
	conv.UserId = strings.TrimSpace(conv.UserId)
	if len(conv.Id) == 0 || len(conv.UserId) == 0 || len(conv.MerchantId) == 0 {
		return errors.New("data invalid")
	}
	return nil
}
