package model

import (
	"errors"
	"strings"
)

type Message struct {
	Content   map[string]any `json:"content" bson:"content"`
	Id        string         `json:"id" bson:"id"`
	Success   bool           `json:"success" bson:"success"`
	Type      string         `json:"type" bson:"type"`
	Userinfo  map[string]any `json:"userinfo" bson:"userinfo"`
	CreatedAt string         `json:"created_at" bson:"created_at"`
}

type MessageCreate struct {
	Id        string         `json:"id" bson:"id" binding:"required"`
	Content   map[string]any `json:"content" bson:"content" binding:"required"`
	Type      string         `json:"type" bson:"type" binding:"required"`
	Userinfo  map[string]any `json:"userinfo" bson:"userinfo" binding:"required"`
	CreatedAt string         `json:"created_at" bson:"created_at" binding:"required"`
}

type MessageUpdate struct {
	Id      string `json:"id" bson:"id"`
	Content string `json:"content"`
	Success bool   `json:"success"`
	Type    int    `json:"type" bson:"type"`
}

type Messages []Message

type MessageCreates []MessageCreate

type MessageUpdates []MessageUpdate

func (conv MessageCreate) Validate() error {

	if len(conv.Id) == 0 || conv.Content == nil || len(conv.Type) == 0 {
		return errors.New("data invalid message create")
	}
	return nil
}

func (conv Message) Validate() error {

	conv.CreatedAt = strings.TrimSpace(conv.CreatedAt)
	if conv.Content != nil || len(conv.CreatedAt) == 0 {
		return errors.New("data invalid")
	}
	return nil
}

func (conv Message) getType() string {
	switch typeMesage := conv.Type; typeMesage {
	case "1":
		return "message"
	case "2":
		return "notify"
	case "3":
		return "bet"
	}
	return "message"
}
