package socket

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"socket_app/common"
	"socket_app/jobs"
	"socket_app/model"
	"socket_app/sky/provider"
	"time"

	"github.com/gorilla/websocket"
)

type Subscription struct {
	conn *connection
	room string
}

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

// readPump pumps messages from the websocket connection to the hub.
func (s Subscription) readPump(h Hub) {

	c := s.conn
	defer func() {
		h.unregister <- s
		c.ws.Close()
	}()
	c.ws.SetReadLimit(maxMessageSize)

	fmt.Println("pong wait", pongWait)

	c.ws.SetReadDeadline(time.Now().Add(pongWait))
	c.ws.SetPongHandler(func(string) error { c.ws.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, msg, err := c.ws.ReadMessage()

		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				var data map[string]interface{}
				json.Unmarshal(msg, &data)
				log.Printf("error: %v", err)
				fmt.Println(data)
			}
			break
		} else {
			var data model.MessageCreate
			err := json.Unmarshal(msg, &data)
			if err != nil {
				fmt.Println(err.Error(), "validate messsage create", string(msg))
				continue
			}

			err = ValidateBetMes(data)
			if err != nil {
				fmt.Println(err.Error())
				continue
			}
			stringData, err := json.Marshal(data)
			if err != nil {
				fmt.Println(err.Error())
				continue
			}

			var payload map[string]interface{}
			err = json.Unmarshal(stringData, &payload)
			if err != nil {
				fmt.Println(err.Error(), string(stringData))
				continue
			}

			if err == nil {
				go func(roomId string, data model.MessageCreate) {
					defer common.Recovery()

					payload["room"] = s.room
					item := provider.Payload{Data: payload, Delay: 0}

					ct := context.Background()
					err = jobs.MessageJob.Dispatch(ct, item)
					if err != nil {
						fmt.Println("error send to queue , ", err)
					}

				}(s.room, data)
				msg = bytes.TrimSpace(bytes.Replace(msg, newline, space, -1))
				// fmt.Println("sent to socket data", string(msg))
				m := Message{msg, s.room}
				currentTime := time.Now()

				if c.LastWrite != currentTime.Format("20060102141005") {
					c.LastWrite = currentTime.Format("20060102141005")
					key := "room_message_" + s.room
					datacontent, ok := payload["content"]
					if !ok {
						panic(err)
					}
					userInfo, ok := payload["userinfo"]
					if !ok {
						panic(err)
					}
					txtdataContent, _ := json.Marshal(datacontent)
					payload["content"] = string(txtdataContent)
					txtInfo, _ := json.Marshal(userInfo)
					payload["userinfo"] = string(txtInfo)

					err := h.redis.AddItemToStream(key, "", payload)
					if err != nil {
						panic(err)
					}
					h.broadcast <- m
				}

			} else {
				fmt.Println(err.Error())
			}

		}

	}
}

// writePump pumps messages from the hub to the websocket connection.
func (s *Subscription) writePump(h Hub) {
	c := s.conn
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.ws.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.write(websocket.CloseMessage, []byte{})
				return
			}
			if err := c.write(websocket.TextMessage, message); err != nil {
				return
			}
		case <-ticker.C:
			if err := c.write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

func ValidateBetMes(mes model.MessageCreate) error {
	err := (mes.Validate())
	if err != nil {
		fmt.Println("error normal")
		return err
	}

	return nil
}
