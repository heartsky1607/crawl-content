package socket

import (
	"log"
	"net/http"
	"socket_app/common"
	"socket_app/sevice/redis"
)

type HandleSocket struct {
	hub Hub
}

func Initialize(redis *redis.RedisClient) *HandleSocket {
	var h = Hub{
		broadcast:    make(chan Message),
		register:     make(chan Subscription),
		unregister:   make(chan Subscription),
		rooms:        make(map[string]map[*connection]bool),
		conectionIds: make(map[string]map[string]string),
		redis:        redis,
	}
	go h.run()

	return &HandleSocket{
		hub: h,
	}
}

func (handle *HandleSocket) StartConversation(w http.ResponseWriter, r *http.Request, roomId string, connectionId string) bool {
	defer common.Recovery()
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("upgrade error")
		panic(err)
	}
	c := &connection{send: make(chan []byte, 256), ws: ws, id: connectionId}
	s := Subscription{c, roomId}
	handle.hub.register <- s
	go s.writePump(handle.hub)
	go s.readPump(handle.hub)
	return true

}

func (handle *HandleSocket) SendBroadcast(data string, room string) {
	// fmt.Println("send brocast", data, room)
	handle.hub.SendToBoadcast(data, room)
}

func (handle *HandleSocket) CountOnline(room string) int {
	return handle.hub.CountOnline(room)
}

func (handle *HandleSocket) VerifyConnection(room string, connectionId string) bool {
	return handle.hub.VerifyConnection(room, connectionId)
}
