package socket

import (
	"socket_app/sevice/redis"
)

type Message struct {
	data []byte
	room string
}

// hub maintains the set of active connections and broadcasts messages to the
// connections.
type Hub struct {
	// Registered connections.
	rooms map[string]map[*connection]bool

	// Inbound messages from the connections.
	broadcast chan Message

	// Register requests from the connections.
	register chan Subscription

	// Unregister requests from connections.
	unregister chan Subscription

	//conection Id

	conectionIds map[string]map[string]string

	redis *redis.RedisClient
}

func (h *Hub) SendToBoadcast(data string, room string) {
	m := Message{[]byte(data), room}
	// fmt.Println("send from hub", data)

	h.broadcast <- m
}

func (h *Hub) CountOnline(room string) int {
	total := 0
	if connects, ok := h.conectionIds[room]; ok {
		total = len(connects)
	}
	return total
}

func (h *Hub) run() {
	for {
		select {
		case s := <-h.register:

			connections := h.rooms[s.room]
			if connections == nil {
				connections = make(map[*connection]bool)
				h.rooms[s.room] = connections
			}

			if len(h.rooms[s.room]) < 10000 {
				h.rooms[s.room][s.conn] = true
				connectionsIds := h.conectionIds[s.room]
				if connectionsIds == nil {
					connectionsIds = make(map[string]string)
					h.conectionIds[s.room] = connectionsIds
				}

				h.conectionIds[s.room][s.conn.id] = s.conn.id
				// fmt.Println("insert connection ", s.conn.id, h.conectionIds[s.room])
			}

		case s := <-h.unregister:
			connections := h.rooms[s.room]
			if connections != nil {
				if _, ok := connections[s.conn]; ok {
					delete(connections, s.conn)
					close(s.conn.send)
					if len(connections) == 0 {
						delete(h.rooms, s.room)
					}
					connectionsIds := h.conectionIds[s.room]
					if connectionsIds != nil {
						if _, ok := connectionsIds[s.conn.id]; ok {
							delete(connectionsIds, s.conn.id)
							if len(connectionsIds) == 0 {
								delete(h.conectionIds, s.room)
							}
							// fmt.Println("remove connection ", s.conn.id, h.conectionIds[s.room])

						}
					}
				}
			}

		case m := <-h.broadcast:
			connections := h.rooms[m.room]
			for c := range connections {
				// fmt.Println("send message to connection", m.data)
				select {
				case c.send <- m.data:
				default:
					close(c.send)
					// fmt.Println("close connection")
					delete(connections, c)
					if len(connections) == 0 {
						delete(h.rooms, m.room)
					}
				}
			}
		}
	}
}

func (h *Hub) VerifyConnection(room string, connId string) bool {
	connectionIds := h.conectionIds[room]
	if connectionIds != nil {
		if _, ok := connectionIds[connId]; ok {
			return false
		}
	}
	return true
}
