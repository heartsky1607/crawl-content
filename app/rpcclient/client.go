package rpcclient

import (
	"fmt"
	"net/rpc"
	"sync"
)

var RPCClient *rPCClient

type rPCClient struct {
	mutex sync.Mutex
	rpc   *rpc.Client
	addr  string
}

func InitializeClient(address string) *rPCClient {
	RPCClient = &rPCClient{
		addr: address,
	}
	return RPCClient
}

func (client *rPCClient) Close() error {
	client.mutex.Lock()
	defer client.mutex.Unlock()
	if client.rpc == nil {
		return nil
	}
	clConn := client.rpc
	client.rpc = nil
	return clConn.Close()
}

func (client *rPCClient) Call(method string, args interface{}, reply interface{}) error {
	fmt.Println("start send")
	client.mutex.Lock()
	defer client.mutex.Unlock()
	sendRequest := func() error {
		fmt.Println(client.rpc == nil)
		if client.rpc == nil {
			clientRpc, err := rpc.DialHTTP("tcp", client.addr)
			if err != nil {
				fmt.Println(err)
				return nil
			}
			client.rpc = clientRpc

		}
		fmt.Println("connect success")
		return client.rpc.Call(method, args, reply)
	}
	if err := sendRequest(); err == rpc.ErrShutdown {
		fmt.Println("eror send", err)
		client.rpc.Close()
		client.rpc = nil
		return sendRequest()
	}
	return nil
}
