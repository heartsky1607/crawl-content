package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"socket_app/jobs"
	"socket_app/middlewares"
	"socket_app/sevice/mongodb"
	"socket_app/sevice/redis"
	"socket_app/sky/provider"
	"socket_app/sky/queue"
	"socket_app/transport"
	"sync"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func main() {

	router := gin.New()

	err := godotenv.Load("./.env")
	if err != nil {
		panic(err)
	}

	ctx := context.Context(context.TODO())

	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGO_URL")))
	if err != nil {
		panic(err)
	}
	if err := mongoClient.Ping(ctx, readpref.Primary()); err != nil {
		panic(err)
	}
	mongodb.MongoClient = mongoClient
	fmt.Println("connect mongo success")

	redisCli, err := redis.Initialize()

	if err != nil {
		panic(err)
	}
	// socketTransport := socket.Initialize(redisCli)
	queueMessage := queue.NewQueue("save_message")
	// queueConversation := queue.NewQueue("store_conversation")

	<-time.After(2 * time.Second)
	jobs.NewSaveMessageJob(queueMessage)

	// job := jobs.NewSaveMessageJob(queueMessage)
	// dispath := dispatcher.NewDispatcher(job)
	// dispath.Initialize(5)
	defer queueMessage.CloseConnection()

	//
	//save conversation
	// jobs.NewStoreConversationJob(queueConversation)
	// jobStoreConversation := jobs.NewStoreConversationJob(queueConversation)
	// dispathStoreConversation := dispatcher.NewDispatcher(jobStoreConversation)
	// dispathStoreConversation.Initialize(10)
	// defer queueConversation.CloseConnection()

	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	handler := transport.NewHandler(redisCli, mongoClient)
	// router.Use(middlewares.JwtAuthMiddleware())
	router.Use(middlewares.Recovery())
	// Register the middleware
	router.GET("/test-room", func(c *gin.Context) {
		testQueue(queueMessage)
		// testQueue(queueConversation)
	})

	router.GET("/message-room/:roomId/:connectionId/:page/:limit", middlewares.JwtAuthMiddleware(), handler.GetListMessage)
	router.GET("/last-message-room/:roomId/:connectionId", middlewares.JwtAuthMiddleware(), handler.GetLastMessage)
	router.GET("/room/:roomId/active", handler.GetCountRoomUserOnline)
	router.POST("/bet-analyer", middlewares.JwtAuthMiddleware(), handler.SendDataAnalyBet)
	router.GET("/bet-available/:gameId", handler.GetBetAvailable)
	router.POST("/send-room/:roomId", middlewares.JwtAuthMiddleware(), handler.SendBroadcastMessage)
	router.GET("/ws/:roomId/:connectionId", middlewares.JwtAuthMiddleware(), handler.OpenSocketConnection)

	router.Run(":8088")
}

func testQueue(queue *queue.Queue) error {
	var wg sync.WaitGroup
	for i := 0; i < 2; i++ {
		wg.Add(1)
		go func(data map[string]interface{}, wg *sync.WaitGroup) {

			defer func() {
				wg.Done()
				if err := recover(); err != nil {
					log.Println("panic occurred: send domain to job ", err)
				}
			}()
			ct := context.Background()
			item := provider.Payload{Data: data, Delay: 0}
			conten, _ := json.Marshal(item)
			fmt.Println(string(conten))
			queue.SendMessage(ct, string(conten))

		}(map[string]any{"name": "seaca", "number": i}, &wg)
	}
	wg.Wait()
	return nil
}
