package main

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()
	app.Get("/", func(ctx *fiber.Ctx) error {
		ctx.Status(fiber.StatusOK).JSON(fiber.Map{
			"status": true,
		})
		return nil
	})
	fmt.Println("done")
	app.Listen("8008")
}
