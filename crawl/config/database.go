package config

import (
	"github.com/joho/godotenv"
	"os"
	"strconv"
)

type DatabaseConfig struct {
	Hot      string `json:"hot"`
	Port     int    `json:"port"`
	Database string `json:"database"`
	User string `json:"user"`
	Password string `json:"password"`
}

var DbConfig *DatabaseConfig

func init()  {
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
		return
	}
	dbport ,err := strconv.Atoi(os.Getenv("PORT"))
	if err != nil {
		dbport = 5432
	}


	DbConfig = &DatabaseConfig {
		os.Getenv("HOST"),
		dbport ,
		os.Getenv("DATABASE"),
		os.Getenv("USERNAME"),
		os.Getenv("PASSWORD"),
	}
}
