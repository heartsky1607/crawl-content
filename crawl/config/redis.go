package config

import (
	"github.com/joho/godotenv"
	"os"
	"strconv"
)

type RedisConfig struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Database int    `json:"database"`
}

var RedisCfg *RedisConfig

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
		return
	}
	dbport, err := strconv.Atoi(os.Getenv("REDIS_PORT"))
	if err != nil {
		dbport = 6379
	}
	db, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	if err != nil {
		dbport = 1
	}

	RedisCfg = &RedisConfig{
		os.Getenv("REDIS_HOST"),
		dbport,
		db,
	}
}
