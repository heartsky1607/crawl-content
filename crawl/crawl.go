package main

import (
	"fmt"
	"github.com/gocolly/colly"
	"github.com/gofiber/fiber/v2"
	"regexp"
	"sea/crawl/app/repositories/site"
	"sea/crawl/app/services/database"
	redis "sea/crawl/app/services/redis"
	site2 "sea/crawl/app/usecases/site"
	"strings"
	"sync"
)

const (
	numberProcess = 3
)

var listWeb = []string{
	"https://www.football.london",
	"https://www.marca.com",
	"https://www.acmilaninfo.com",
	"https://www.givemesport.com",
	"https://en.psg.fr",
	"https://www.skysports.com/football/news",
	"https://www.realmadrid.com",
	////"https://www.mancity.com",
	////"https://www.uefa.com",
	////"https://www.fifa.com",
	////"https://www.goal.com",

}

func main() {
	Load()

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	ScanUrl("https://www.givemesport.com")
	//fmt.Println("crawl done")
	//CrawlUrlDetail()
	//sv := crawl2.LoadInstance()
	//sv.CrawlContentUefa("https://www.uefa.com/futsalunder19/news/027b-16ac901961e9-99fb847e9147-1000--england-kosovo-montenegro-through/")
	//repo := site.LoadInstance()
	//uc := site2.LoadInstance(repo)
	//uc.CrawlNewDetail("https://www.givemesport.com", "https://www.givemesport.com/southampton-saints-ruben-selles-premier-league-manager-news-st-marys/")

	//uc.CrawlNewDetail("https://www.givemesport.com", "https://www.givemesport.com/88112045-mykhailo-mudryks-stats-from-chelsea-0-0-fulham-are-shocking")
	app.Listen(":8000")

	//for key, val := range listWeb {
	//	fmt.Println(key)
	//	fmt.Println(val)
	//	crawl(val)
	//	break
	//}
	//sv := ddos.LoadInstance()
	//sv.Send("")
	//reqObj := req.LoadInstance()
	//params := make(map[string]string)
	//params["length"] = "10"
	//params["orderBy"] = "id"
	//
	//reqObj.PostJson("https://httpbin.org/post", params)

	//sv.LoadSite("https://www.marca.com")
	/*

	 */

}
func ScanUrl(url string) {
	redis := redis.Redis
	c := colly.NewCollector(
		// MaxDepth is 2, so only the links on the scraped page
		// and links on those pages are visited
		colly.MaxDepth(3),
		colly.Async(true),
	)
	domain := strings.Replace(url, ".", "", -1)
	re := regexp.MustCompile(`//|:|https|www`)
	domain = re.ReplaceAllString(domain, "")

	// Limit the maximum parallelism to 2
	// This is necessary if the goroutines are dynamically
	// created to control the limit of simultaneous requests.
	//
	// Parallelism can be controlled also by spawning fixed
	// number of go routines.
	c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: 2})
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		if !(strings.HasPrefix(link, "/tag") ||
			strings.HasPrefix(link, "#") ||
			strings.HasPrefix(link, "http") ||
			strings.HasPrefix(link, "mailto") ||
			strings.HasPrefix(link, "javascript") ||
			strings.HasPrefix(link, "/football/fixtures/") ||
			strings.HasPrefix(link, "/contact/") ||
			strings.HasPrefix(link, "/contributor") ||
			strings.HasPrefix(link, "/page/")) {
			link = url + link

			if !(redis.CheckItemInSet("check_"+domain, link)) {
				redis.AddItemToSet("check_"+domain, link)
				fmt.Println(link)
				e.Request.Visit(link)
			}

		}

	})

	fmt.Println(url)

	// Start scraping on https://en.wikipedia.org
	c.Visit(url)
	// Wait until threads are finished
	c.Wait()
}

func CollectUrl() {
	repo := site.LoadInstance()
	uc := site2.LoadInstance(repo)
	queue := StartQueue()
	var wg sync.WaitGroup
	wg.Add(1)
	go CrawlListUrl(queue, uc, &wg)

	wg.Wait()
}

func CrawlUrlDetail() {
	repo := site.LoadInstance()
	uc := site2.LoadInstance(repo)
	queue := StartQueue()

	var wg sync.WaitGroup
	for i := 1; i <= numberProcess; i++ {
		wg.Add(1)
		go CrawlSingleUrlDetail(queue, uc, &wg)
	}
	wg.Wait()
}
func CrawlSingleUrlDetail(queue <-chan string, uc *site2.SiteUc, wg *sync.WaitGroup) {
	defer wg.Done()
	for url := range queue {
		crawl(url, uc)

	}
}

func CrawlListUrl(queue <-chan string, uc *site2.SiteUc, wg *sync.WaitGroup) {
	defer wg.Done()
	for url := range queue {
		uc.CrawlListNews(url)
	}
}

func StartQueue() <-chan string {
	queue := make(chan string, 100)
	go func() {
		for _, url := range listWeb {
			queue <- url
		}
		close(queue)
	}()

	return queue
}

func Load() {
	err := database.LoadInstance()
	if err != nil {
		panic(err)
	}

	_, err = redis.Initialize()
	if err != nil {
		panic(err)
	}
}

func crawl(siteUrl string, uc *site2.SiteUc) {

	domain := strings.Replace(siteUrl, ".", "", -1)
	re := regexp.MustCompile(`//|:|https|www`)
	domain = re.ReplaceAllString(domain, "")

	redisClient := redis.Redis
	for {
		val, _ := redisClient.PopItemFromSet("new_list" + domain)
		fmt.Println(val)
		if val == "" {
			break
		}
		uc.CrawlNewDetail(siteUrl, val)

	}

}
