package redis

import (
	"fmt"
	"sea/crawl/config"
	"strconv"

	"github.com/go-redis/redis"
)

type RedisClient struct {
	client *redis.Client
}

var Redis *RedisClient
var RedisError error

func init() {
	Redis, RedisError = Initialize()

}

func Initialize() (*RedisClient, error) {
	RedisConfig := config.RedisCfg
	client := redis.NewClient(&redis.Options{
		Addr:     RedisConfig.Host + ":" + strconv.Itoa(RedisConfig.Port),
		Password: "secret_redis",
		DB:       RedisConfig.Database,
	})
	result, err := client.Ping().Result()

	if err != nil {
		fmt.Println(err)
		return nil, err

	}
	fmt.Println(result)

	return &RedisClient{
		client: client,
	}, nil

}

type RedisInteface interface {
	Get()
	Set()
	Remove()
	AddItemToList()
	AddItemToSet()
	AddItemToHash()
	AddItemToSortedSet()
	GetItemFromSet()
	GetItemFromHash()
	CheckItemInSet()
}

func (r *RedisClient) Get(key string) (string, error) {
	return r.client.Get(key).Result()

}

func (r *RedisClient) Set(key string, value string) error {
	return r.client.Set(key, value, 0).Err()
}

func (r *RedisClient) Remove(key string) error {
	err := r.client.Del(key).Err()
	return err
}

func (r *RedisClient) AddItemToSet(key string, val string) error {

	return r.client.SAdd(key, val).Err()
}

func (r *RedisClient) CheckItemInSet(key string, item string) bool {
	okay, err := r.client.SIsMember(key, item).Result()
	if err != nil {
		return false
	}
	return okay
}

func (r *RedisClient) PopItemFromSet(key string) (string, error) {
	return r.client.SPop(key).Result()
}

func (r *RedisClient) GetAllItemFromSet(key string) ([]string, error) {
	return r.client.SMembers(key).Result()
}

func (r *RedisClient) AddItemToHash(key string, data map[string]string) error {
	for prop, val := range data {
		err := r.client.HSet(key, prop, val).Err()
		if err != nil {
			return err
		}
	}
	return nil
}

func (r *RedisClient) GetItemFromHash(key string, prop string) (string, error) {
	return r.client.HGet(key, prop).Result()
}

func (r *RedisClient) GetAllItemFromHash(key string) (map[string]string, error) {
	return r.client.HGetAll(key).Result()
}
