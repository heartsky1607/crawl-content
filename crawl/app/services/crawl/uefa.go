package crawl

import (
	"bytes"
	"fmt"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"log"
	"net/url"
	"sea/crawl/app/entities"
	"strings"
)

func (cr *Crawl) CollectUrlUefa(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)

	c.OnHTML("div.content > div > div.col-contents > div.col-c > div.featured-slot> div.row >div >div.card", func(e *colly.HTMLElement) {
		profileUrl := e.ChildAttr("a", "href")
		if !strings.HasPrefix(profileUrl, "#") {
			profileUrl = domain + profileUrl
			cr.saveDomainPostUrl(domain, profileUrl)
		}
	})

	c.OnHTML("div.content > div > div.col-contents > div.col-c > div> div.hp-new >div.row >div.mosaic-card >div.card", func(e *colly.HTMLElement) {
		profileUrl := e.ChildAttr("a", "href")
		if !strings.HasPrefix(profileUrl, "#") && strings.Contains(profileUrl, "/news/") {
			profileUrl = domain + profileUrl
			cr.saveDomainPostUrl(domain, profileUrl)
		}
	})

	c.Visit(domain)
	return
}

func (cr *Crawl) CrawlContentUefa(urlPath string) entities.Post {
	//fmt.Println(urlPath)
	var post entities.Post
	c := colly.NewCollector(
		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)

	//infoCollector := c.Clone()
	fmt.Println("start collect")
	c.OnHTML("article > header > h1", func(e *colly.HTMLElement) {
		//fmt.Println(e.Text)
		post.Title = e.Text
	})
	c.OnHTML("article > div.article_top-content >div > figure > div > picture", func(e *colly.HTMLElement) {
		imageUrl := e.ChildAttr("img", "src")
		if imageUrl != "" {
			u, _ := url.Parse(imageUrl)
			u.RawQuery = ""
			u.Fragment = ""
			post.FeatureImage = u.String()
			//fmt.Println(post.FeatureImage)
		}

	})
	c.OnHTML("article > div.article_content > div.article_body", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContentUefa(post.Content)
		content = RemoveTags(content)
		//tempContent := strings.Split(content, " ")
		post.Content = content
		//fmt.Println(content)
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})
	c.Visit(urlPath)
	post.OriginUrl = urlPath
	//cr.db.Create(&post)
	return post
}

func filterContentUefa(s string) string {

	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScriptUefa(doc)
	//removeDivUefa(doc)
	removeDiv(doc)
	html.Render(&content, doc)
	body, err := getBody(doc)

	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)
	body = removeSPaceAndBreakline(body)
	return body
}

func removeScriptUefa(n *html.Node) {

	// if note is script tag
	if n.Type == html.ElementNode && n.Data == "script" {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		collectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}

	}
	if n.Type == html.ElementNode && n.Data == "div" {
		text := &bytes.Buffer{}
		collectText(n, text)
		for _, attr := range n.Attr {
			if attr.Key == "class" {
				if strings.Contains(attr.Val, "article-embedded_video") {
					n.Parent.RemoveChild(n)
					return
				}
			}
			if attr.Key == "style" && strings.Contains(attr.Val, "display: none") {
				n.Parent.RemoveChild(n)
				return
			}

		}

	}
	if n.Type == html.ElementNode && n.Data == "img" {
		imageUrl := ""
		alt := ""
		for _, attr := range n.Attr {
			if attr.Key == "data-src" {
				imageUrl = ClearImageUrl(attr.Val)
			}
			if attr.Key == "alt" {
				alt = attr.Val

			}

		}

		attr := []html.Attribute{}
		attr = append(attr, html.Attribute{"", "src", imageUrl})
		attr = append(attr, html.Attribute{"", "alt", alt})
		imageNode := &html.Node{
			Type: html.ElementNode,
			Data: "img",
			Attr: attr,
		}
		newNode := n.Parent.Parent.Parent
		fmt.Println("node image ", newNode.Data)
		newNode.Parent.InsertBefore(imageNode, newNode)
		newNode.Parent.RemoveChild(newNode)
		return
	}

	if n.Type == html.ElementNode && n.Data == "source" {
		n.Parent.RemoveChild(n)
		return
	}

	if n.Type == html.ElementNode && n.Data == "pk-button" {
		n.Parent.RemoveChild(n)
		return
	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScriptUefa(c)
	}
}

//func removeDivUefa(n *html.Node) {
//
//	for c := n.FirstChild; c != nil; c = c.NextSibling {
//		defer removeDivUefa(c)
//	}
//}

func handleImageElement(n *html.Node, imageNode *html.Node) {

	if n.Type == html.ElementNode && n.Data == "img" {
		imageUrl := ""
		alt := ""
		for _, attr := range n.Attr {
			if attr.Key == "src" {
				imageUrl = ClearImageUrl(attr.Val)
			}
			if attr.Key == "alt" {
				alt = attr.Val

			}

		}

		attr := []html.Attribute{}
		attr = append(attr, html.Attribute{"", "src", imageUrl})
		attr = append(attr, html.Attribute{"", "alt", alt})
		imageNode = &html.Node{
			Type: html.ElementNode,
			Data: "img",
			Attr: attr,
		}
		return
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeDivGiveMeSport(c)
	}
}
