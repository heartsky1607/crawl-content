package crawl

import (
	"bytes"
	"fmt"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"log"
	"sea/crawl/app/entities"
	"strings"
)

func (cr *Crawl) CollectUrlAcmilaninfo(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)

	c.OnHTML("div.besides-block > div.beside-post", func(e *colly.HTMLElement) {
		profileUrl := e.ChildAttr("a", "href")
		cr.saveDomainPostUrl(domain, profileUrl)
	})
	c.OnHTML("div#home-content > div#primary > main#main > div.home-featured-block ", func(e *colly.HTMLElement) {
		e.ForEach("div.featured-post > figure.post-thumb", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			cr.saveDomainPostUrl(domain, profileUrl)
		})

	})

	c.OnHTML("div#home-content > div#primary > main#main > div.home-featured-block ", func(e *colly.HTMLElement) {
		e.ForEach("div.featured-post-three > figure.post-thumb-mini", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			cr.saveDomainPostUrl(domain, profileUrl)
		})

	})

	c.OnHTML("div#home-content > div#primary > main#main > div.single-col ", func(e *colly.HTMLElement) {
		e.ForEach("div.featured-post-block-coltype > figure.post-thumb-mini", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			cr.saveDomainPostUrl(domain, profileUrl)
		})
	})
	c.Visit(domain)
	return
}

func (cr *Crawl) CrawlContentAcmilaninfo(urlPath string) entities.Post {
	fmt.Println(urlPath)
	var post entities.Post
	c := colly.NewCollector(
		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)
	//infoCollector := c.Clone()
	//fmt.Println("start collect")
	c.OnHTML("article > header > h1", func(e *colly.HTMLElement) {
		fmt.Println(e.Text)
		post.Title = e.Text
	})
	c.OnHTML("article > div > figure.single-thumb", func(e *colly.HTMLElement) {
		post.FeatureImage = e.ChildAttr("img", "src")
	})

	c.OnHTML("article > div.entry-content", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContentAcmilanInfo(post.Content)
		content = RemoveTags(content)
		fmt.Println(content)
		post.Content = content
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})

	c.Visit(urlPath)
	post.OriginUrl = urlPath
	//cr.db.Create(&post)
	return post
}

func filterContentAcmilanInfo(s string) string {
	index := strings.Index(s, "READ MORE")
	if index > 0 {
		s = s[0:index]
	}

	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScriptAcmilanInfo(doc)
	removeDiv(doc)
	html.Render(&content, doc)
	body, err := getBody(doc)
	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)
	body = removeSPaceAndBreakline(body)
	return body
}

func removeScriptAcmilanInfo(n *html.Node) {
	// if note is script tag
	if n.Type == html.ElementNode && n.Data == "script" {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		collectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}
	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScript(c)
	}
}
