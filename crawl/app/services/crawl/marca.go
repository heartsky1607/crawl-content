package crawl

import (
	"bytes"
	"fmt"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"log"
	"net/url"
	"sea/crawl/app/entities"
	"strings"
)

func (cr *Crawl) CollectUrlMarca(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)

	c.OnHTML("article > div > div.ue-c-cover-content__main > header", func(e *colly.HTMLElement) {
		profileUrl := e.ChildAttr("a", "href")
		cr.saveDomainPostUrl(domain, profileUrl)
	})
	c.Visit(domain)
	return
}

func (cr *Crawl) CrawlContentMarca(urlPath string) entities.Post {
	//fmt.Println(urlPath)
	var post entities.Post
	c := colly.NewCollector(
		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)

	//infoCollector := c.Clone()
	//fmt.Println("start collect")
	c.OnHTML("main > div.ue-l-article > div.ue-l-article__inner > article > div.ue-l-article__header > div > div > div > h1", func(e *colly.HTMLElement) {
		fmt.Println(strings.Trim(e.Text, " "))
		post.Title = e.Text
	})
	c.OnHTML("main > div.ue-l-article > div.ue-l-article__inner > article > div.ue-l-article__body > div > figure > div > picture ", func(e *colly.HTMLElement) {
		imageUrl := e.ChildAttr("img", "src")
		if imageUrl != "" {
			u, _ := url.Parse(imageUrl)
			u.RawQuery = ""
			u.Fragment = ""
			post.FeatureImage = u.String()
			fmt.Println(post.FeatureImage)
		}
	})
	c.OnHTML("main > div.ue-l-article > div.ue-l-article__inner > article > div.ue-l-article__body > div > div.ue-c-article__body", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContentMarca(post.Content)
		content = RemoveTags(content)
		post.Content = content
		fmt.Println(content)
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})
	c.Visit(urlPath)
	post.OriginUrl = urlPath
	//cr.db.Create(&post)
	return post
}

func filterContentMarca(s string) string {

	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScriptMarca(doc)
	removeDivMarca(doc)
	html.Render(&content, doc)
	body, err := getBody(doc)

	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)
	body = removeSPaceAndBreakline(body)
	return body
}

func removeScriptMarca(n *html.Node) {

	// if note is script tag
	if n.Type == html.ElementNode && (n.Data == "script" || n.Data == "blockquote") {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		collectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}
	}

	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScriptMarca(c)
	}
}

func removeDivMarca(n *html.Node) {

	if n.Type == html.ElementNode && n.Data == "div" {
		for _, attr := range n.Attr {
			if attr.Key == "class" && (strings.Contains(attr.Val, "midwidget-taboola") || strings.Contains(attr.Val, "teads-inread")) {
				n.Parent.RemoveChild(n)
				return
			}
		}
	}
	if n.Type == html.ElementNode && n.Data == "figure" {
		for _, attr := range n.Attr {
			if attr.Key == "class" && strings.Contains(attr.Val, "ue-c-article__media--video") {
				n.Parent.RemoveChild(n)
				return
			}
		}
	}

	if n.Type == html.ElementNode && n.Data == "source" {
		parentNode := n.Parent
		if parentNode.Data == "picture" {
			parentNode.RemoveChild(n)
			return
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeDivMarca(c)
	}
}
