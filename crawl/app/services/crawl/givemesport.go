package crawl

import (
	"bytes"
	"fmt"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"log"
	"sea/crawl/app/entities"
	"strings"
)

func (cr *Crawl) CollectUrlGiveMeSport(domain string) {

	c := colly.NewCollector(
		colly.MaxDepth(3),
	)
	c.OnHTML("div.gms-body > main.gms-main > div.gms-hero", func(e *colly.HTMLElement) {
		profileUrl := e.ChildAttr("a", "href")
		fmt.Println(profileUrl)
		cr.saveDomainPostUrl(domain, profileUrl)

	})
	c.OnHTML("div.gms-body > main.gms-main > div.gms-cols > div.gms-group", func(e *colly.HTMLElement) {
		e.ForEach("div.gms-teaser", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			cr.saveDomainPostUrl(domain, profileUrl)
		})
	})
	c.OnHTML("div.gms-body > main.gms-main > section.gms-cols", func(e *colly.HTMLElement) {
		e.ForEach("div.gms-teaser", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			cr.saveDomainPostUrl(domain, profileUrl)
		})
	})
	c.Visit(domain)
	return
}

func (cr *Crawl) CrawlContentGiveMeSport(urlPath string) entities.Post {
	var post entities.Post
	c := colly.NewCollector(
		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)

	//infoCollector := c.Clone()
	//fmt.Println("start collect")
	c.OnHTML("main > div >article > header > h1", func(e *colly.HTMLElement) {
		//fmt.Println(e.Text)
		post.Title = e.Text
	})
	c.OnHTML("main > div >article  > div.gms-content-body", func(e *colly.HTMLElement) {
		post.FeatureImage = e.ChildAttr("img", "src")
		//fmt.Println(post.FeatureImage)
	})

	c.OnHTML("main > div >article > div.gms-content-body", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContentGiveMeSport(post.Content)
		content = RemoveTags(content)
		post.Content = content
		fmt.Println(content)
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})
	c.Visit(urlPath)
	post.OriginUrl = urlPath
	//cr.db.Create(&post)
	return post
}

func filterContentGiveMeSport(s string) string {
	index := strings.Index(s, "<nav class=\"gms-circulation\">")
	if index > 0 {
		s = s[0:index]
	}

	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScriptGiveMeSport(doc)
	removeDivGiveMeSport(doc)
	removeDiv(doc)
	html.Render(&content, doc)
	body, err := getBody(doc)
	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)
	body = removeSPaceAndBreakline(body)
	return body
}

func removeScriptGiveMeSport(n *html.Node) {

	// if note is script tag
	if n.Type == html.ElementNode && n.Data == "script" {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		collectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}

	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScriptSkySports(c)
	}
}

func removeDivGiveMeSport(n *html.Node) {
	if n.Type == html.ElementNode && n.Data == "div" {
		for _, attr := range n.Attr {
			if attr.Key == "class" && attr.Val == "c-inblog_ad" {
				n.Parent.RemoveChild(n)
				return
			}
		}

	}
	if n.Type == html.ElementNode && n.Data == "figure" {
		for _, attr := range n.Attr {
			if attr.Key == "class" && strings.Contains(attr.Val, "wp-block-embed is-type-video") {
				n.Parent.RemoveChild(n)
				return
			}
		}

	}

	if n.Type == html.ElementNode && n.Data == "img" {
		imageUrl := ""
		imageSrc := ""
		alt := ""
		for _, attr := range n.Attr {
			if attr.Key == "srcset" {
				imgs := strings.Split(attr.Val, ",")
				for _, v := range imgs {
					if strings.Contains(v, "960w") {
						imageSrc = strings.Replace(v, "960w", "", 1)
					}
				}

			}
			if attr.Key == "src" {
				imageUrl = attr.Val

			}
			if attr.Key == "alt" {
				alt = attr.Val

			}

		}
		if imageSrc != "" {
			imageUrl = imageSrc
		}
		attr := []html.Attribute{}
		attr = append(attr, html.Attribute{"", "src", imageUrl})
		attr = append(attr, html.Attribute{"", "alt", alt})
		nodeLink := html.Node{
			Type: html.ElementNode,
			Data: "img",
			Attr: attr,
		}

		n.Parent.InsertBefore(&nodeLink, n.NextSibling)

		n.Parent.RemoveChild(n)

	}

	if n.Type == html.ElementNode && n.Data == "p" {
		text := &bytes.Buffer{}
		collectText(n, text)
		if strings.Contains(text.String(), "You can find all of the latest") || strings.Contains(text.String(), "Read more") {
			n.Parent.RemoveChild(n)
			return
		}
	}
	if n.Type == html.ElementNode && n.Data == "h2" {
		text := &bytes.Buffer{}
		collectText(n, text)
		if strings.Contains(text.String(), "Football Terrace") || strings.Contains(text.String(), "QUIZ") {
			n.Parent.RemoveChild(n)
			return
		}

	}

	if removeTag(n, "link") {
		return
	}

	if removeTag(n, "iframe") {
		return
	}
	if removeTag(n, "blockquote") {
		return
	}

	if removeTag(n, "form") {
		return
	}
	if removeTag(n, "ul") {
		return
	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeDivGiveMeSport(c)
	}
}
