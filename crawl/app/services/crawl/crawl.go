package crawl

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/gocolly/colly"
	"golang.org/x/net/html"
	"gorm.io/gorm"
	"io"
	"log"
	"net/url"
	"regexp"
	"sea/crawl/app/entities"
	"sea/crawl/app/services/database"
	"sea/crawl/app/services/redis"
	"strings"
	"unicode"
)

type Crawl struct {
	db    *gorm.DB
	redis *redis.RedisClient
}

func LoadInstance() *Crawl {

	return &Crawl{
		db:    database.DB,
		redis: redis.Redis,
	}
}

func (cr *Crawl) LoadSite(domain string) {
	fmt.Println("handle " + domain)
	switch domain {
	case "https://www.acmilaninfo.com":
		cr.CollectUrlAcmilaninfo(domain)
	case "https://www.football.london":
		cr.CollectUrlFootballLondon(domain)
	case "https://www.skysports.com/football/news":
		cr.CollectUrlSkySports(domain)
	case "https://www.givemesport.com":
		cr.CollectUrlGiveMeSport(domain)
	case "https://www.realmadrid.com":
		cr.CollectUrlRealMadrid(domain)
	case "https://www.uefa.com":
		cr.CollectUrlUefa(domain)
	case "https://en.psg.fr":
		cr.CollectUrlPsg(domain)
	case "https://www.marca.com":
		cr.CollectUrlMarca(domain)

	}
}

func (cr *Crawl) CrawlPost(domain string, postUrl string) entities.Post {

	var newPost entities.Post
	switch domain {
	case "https://www.acmilaninfo.com":
		newPost = cr.CrawlContentAcmilaninfo(postUrl)
	case "https://www.football.london":
		newPost = cr.CrawlContentFootballLondon(postUrl)
	case "https://www.skysports.com/football/news":
		newPost = cr.CrawlContentSkySports(postUrl)
	case "https://www.givemesport.com":
		newPost = cr.CrawlContentGiveMeSport(postUrl)
	case "https://www.realmadrid.com":
		newPost = cr.CrawlContentRealMadrid(postUrl)
	case "https://www.uefa.com":
		newPost = cr.CrawlContentUefa(postUrl)
	case "https://en.psg.fr":
		newPost = cr.CrawlContentPsg(postUrl)
	case "https://www.marca.com":
		newPost = cr.CrawlContentMarca(postUrl)
	}

	return newPost
}

func (cr *Crawl) saveDomainPostUrl(site string, postUrl string) {
	domain := strings.Replace(site, ".", "", -1)
	re := regexp.MustCompile(`//|:|https|www`)
	domain = re.ReplaceAllString(domain, "")
	check := cr.redis.CheckItemInSet("crawl_list"+domain, postUrl)
	fmt.Println(postUrl)
	if check == false {
		cr.redis.AddItemToSet("crawl_list"+domain, postUrl)
		cr.redis.AddItemToSet("new_list"+domain, postUrl)
	}
}

func (cr *Crawl) CollectUrl(siteId int) {
	var site entities.Site
	cr.db.First(&site, "id=?", siteId)

	if len(site.Settings) == 0 {
		var settings entities.SettingSites
		setting1 := entities.SettingSite{
			MainKey: "div.besides-block > div.beside-post",
		}
		settings = append(settings, setting1)

		setting2 := entities.SettingSite{
			MainKey: "div#home-content > div#primary > main#main > div.home-featured-block",
			SubKey:  "div.featured-post > figure.post-thumb",
		}
		settings = append(settings, setting2)
		setting3 := entities.SettingSite{
			MainKey: "div#home-content > div#primary > main#main > div.home-featured-block",
			SubKey:  "div.featured-post-three > figure.post-thumb-mini",
		}
		settings = append(settings, setting3)

		setting4 := entities.SettingSite{
			MainKey: "div#home-content > div#primary > main#main > div.single-col",
			SubKey:  "div.featured-post-block-coltype > figure.post-thumb-mini",
		}
		settings = append(settings, setting4)
		site.Settings = settings
		cr.db.Updates(&site)

	}
	c := colly.NewCollector(

		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)

	fmt.Println(site.Url)
	//infoCollector := c.Clone()

	settings := site.Settings

	for _, val := range settings {
		c.OnHTML(val.MainKey, func(e *colly.HTMLElement) {
			if (val.SubKey) != "" {
				e.ForEach(val.SubKey, func(_ int, kf *colly.HTMLElement) {
					profileUrl := kf.ChildAttr("a", "href")
					fmt.Println(profileUrl)

					check := cr.redis.CheckItemInSet("crawl_list", profileUrl)
					if check == false {
						cr.redis.AddItemToSet("crawl_list", profileUrl)
						cr.redis.AddItemToSet("new_list", profileUrl)
					}
				})
			} else {
				profileUrl := e.ChildAttr("a", "href")
				fmt.Println("no sub" + profileUrl)
				fmt.Println(val.MainKey)
				check := cr.redis.CheckItemInSet("crawl_list", profileUrl)
				if check == false {
					cr.redis.AddItemToSet("crawl_list", profileUrl)
					cr.redis.AddItemToSet("new_list", profileUrl)
				}
			}

		})
	}

	c.OnHTML("div.besides-block > div.beside-post", func(e *colly.HTMLElement) {
		profileUrl := e.ChildAttr("a", "href")
		fmt.Println(profileUrl)
		check := cr.redis.CheckItemInSet("crawl_list", profileUrl)
		if check == false {
			cr.redis.AddItemToSet("crawl_list", profileUrl)
			cr.redis.AddItemToSet("new_list", profileUrl)
		}

	})
	c.OnHTML("div#home-content > div#primary > main#main > div.home-featured-block ", func(e *colly.HTMLElement) {
		e.ForEach("div.featured-post > figure.post-thumb", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			check := cr.redis.CheckItemInSet("crawl_list", profileUrl)
			if check == false {
				cr.redis.AddItemToSet("crawl_list", profileUrl)
				cr.redis.AddItemToSet("new_list", profileUrl)
			}
		})

	})

	c.OnHTML("div#home-content > div#primary > main#main > div.home-featured-block ", func(e *colly.HTMLElement) {
		e.ForEach("div.featured-post-three > figure.post-thumb-mini", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			check := cr.redis.CheckItemInSet("crawl_list", profileUrl)
			fmt.Println(profileUrl)
			if check == false {
				cr.redis.AddItemToSet("crawl_list", profileUrl)
				cr.redis.AddItemToSet("new_list", profileUrl)
			}
		})

	})

	c.OnHTML("div#home-content > div#primary > main#main > div.single-col ", func(e *colly.HTMLElement) {
		e.ForEach("div.featured-post-block-coltype > figure.post-thumb-mini", func(_ int, kf *colly.HTMLElement) {
			profileUrl := kf.ChildAttr("a", "href")
			check := cr.redis.CheckItemInSet("crawl_list", profileUrl)
			fmt.Println(profileUrl)
			if check == false {
				cr.redis.AddItemToSet("crawl_list", profileUrl)
				cr.redis.AddItemToSet("new_list", profileUrl)
			}
		})

	})

	c.Visit(site.Url)
	return
	/*
		c.OnHTML("a.lister-page-next", func(e *colly.HTMLElement) {
			nextPage := e.Request.AbsoluteURL(e.Attr("href"))
			c.Visit(nextPage)
		})

		infoCollector.OnHTML("#content-2-wide", func(e *colly.HTMLElement) {
			tmpProfile := star{}
			tmpProfile.Name = e.ChildText("h1.header > span.itemprop")
			tmpProfile.Photo = e.ChildAttr("#name-poster", "src")
			tmpProfile.JobTitle = e.ChildText("#name-job-categories > a > span.itemprop")
			tmpProfile.BirthDate = e.ChildAttr("#name-born-info time", "datetime")

			tmpProfile.Bio = strings.TrimSpace(e.ChildText("#name-bio-text > div.name-trivia-bio-text > div.inline"))


			e.ForEach("div.knownfor-title", func(_ int, kf *colly.HTMLElement) {
				tmpMovie := movie{}
				tmpMovie.Title = kf.ChildText("div.knownfor-title-role > a.knownfor-ellipsis")
				tmpMovie.Year = kf.ChildText("div.knownfor-year > span.knownfor-ellipsis")
				tmpProfile.TopMovies = append(tmpProfile.TopMovies, tmpMovie)
			})
			js, err := json.MarshalIndent(tmpProfile, "", "    ")
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(string(js))
		})

		c.OnRequest(func(r *colly.Request) {
			fmt.Println("Visiting: ", r.URL.String())
		})

		infoCollector.OnRequest(func(r *colly.Request) {
			fmt.Println("Visiting Profile URL: ", r.URL.String())
		})*/

}

func (cr *Crawl) CrawlContent(urlPath string) {
	fmt.Println(urlPath)
	var post entities.Post
	c := colly.NewCollector(
		// using async makes you lose the sort order
		//colly.Async(true),
		colly.MaxDepth(3),
	)

	//infoCollector := c.Clone()
	fmt.Println("start collect")
	c.OnHTML("article > header > h1", func(e *colly.HTMLElement) {
		post.Title = e.Text
	})
	c.OnHTML("article > div > figure.single-thumb", func(e *colly.HTMLElement) {
		post.FeatureImage = e.ChildAttr("img", "src")
	})

	c.OnHTML("article > div.entry-content", func(e *colly.HTMLElement) {
		post.Content, _ = e.DOM.Html()
		content := filterContent(post.Content)
		content = RemoveTags(content)

		fmt.Println(content)
	})
	c.OnHTML("head > title", func(e *colly.HTMLElement) {
		post.SeoTitle = e.Text
	})

	c.OnHTML("head>meta[name=\"description\"]", func(e *colly.HTMLElement) {
		post.SeoDescription = e.Attr("content")
	})

	c.Visit(urlPath)
	post.OriginUrl = urlPath
	cr.db.Create(&post)
	return
}

func filterContent(s string) string {
	doc, err := html.Parse(strings.NewReader(s))
	if err != nil {
		log.Fatal(err)
	}
	var content bytes.Buffer
	removeScript(doc)
	removeDiv(doc)
	html.Render(&content, doc)
	body, err := getBody(doc)
	body = strings.Replace(body, "<body>", "", 1)
	body = strings.Replace(body, "</body>", "", 1)
	return body
}
func RemoveTags(html string) string {
	re, _ := regexp.Compile("<[^>/]+></[^>]+>")
	rep := re.ReplaceAllString(html, "")
	if rep != html {
		return RemoveTags(rep)
	}
	return rep
}

func getBody(n *html.Node) (string, error) {
	var body *html.Node
	var crawler func(*html.Node)
	crawler = func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == "body" {
			body = node
			return
		}
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			crawler(child)
		}
	}
	crawler(n)
	if body != nil {
		return renderNode(body), nil
	}
	return "", errors.New("Missing <body> in the node tree")
}

func removeScript(n *html.Node) {

	// if note is script tag
	if n.Type == html.ElementNode && n.Data == "script" {
		n.Parent.RemoveChild(n)
		return
	}
	if n.Type == html.ElementNode && n.Data == "a" {
		text := &bytes.Buffer{}
		collectText(n, text)
		if text.String() == "" {
			n.Parent.RemoveChild(n)
			return
		} else {
			nodeLink := html.Node{
				Type: html.TextNode,
				Data: text.String(),
			}
			n.Parent.AppendChild(&nodeLink)
			n.Parent.RemoveChild(n)
			return
		}

	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeScript(c)
	}
}

func renderNode(n *html.Node) string {
	var buf bytes.Buffer
	w := io.Writer(&buf)
	html.Render(w, n)
	return buf.String()
}

func removeTag(n *html.Node, tag string) bool {
	if n.Type == html.ElementNode && n.Data == tag {
		n.Parent.RemoveChild(n)
		return true
	}
	return false
}

func removeDiv(n *html.Node) {

	if n.Type == html.ElementNode && n.Data == "div" {
		text := &bytes.Buffer{}

		collectText(n, text)
		if strings.TrimSpace(text.String()) == "" {
			n.Parent.RemoveChild(n)
		}
		return
	}
	// traverse DOM
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		defer removeDiv(c)
	}
}
func collectText(n *html.Node, buf *bytes.Buffer) {
	if n.Type == html.TextNode {
		buf.WriteString(n.Data)
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		collectText(c, buf)
	}
}
func ClearImageUrl(imageUrl string) string {
	u, _ := url.Parse(imageUrl)
	u.RawQuery = ""
	u.Fragment = ""
	return u.String()
}

func removeSPaceAndBreakline(s string) string {
	isSpace := true
	rr := make([]rune, 0, len(s))
	for _, r := range s {
		if !unicode.IsSpace(r) {
			rr = append(rr, r)
			isSpace = false
		} else {
			if isSpace == false {
				rr = append(rr, r)
				isSpace = true
			}
		}
	}
	result := string(rr)
	re, _ := regexp.Compile("\n")
	result = re.ReplaceAllString(result, "")
	re, _ = regexp.Compile("<!--.*?-->")
	result = re.ReplaceAllString(result, "")
	result = RemoveTags(result)
	re, _ = regexp.Compile("<div (.*?)>")
	result = re.ReplaceAllString(result, "<div>")
	re, _ = regexp.Compile("<p (.*?)>")
	result = re.ReplaceAllString(result, "<p>")
	re, _ = regexp.Compile("<span (.*?)>")
	result = re.ReplaceAllString(result, "<span>")
	re, _ = regexp.Compile("<figcaption (.*?)>")
	result = re.ReplaceAllString(result, "<figcaption>")
	re, _ = regexp.Compile("<figure (.*?)>")
	result = re.ReplaceAllString(result, "<figure>")
	re, _ = regexp.Compile("\"\"")
	result = re.ReplaceAllString(result, "\"")

	return result
}
