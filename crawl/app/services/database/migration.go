package database

import (
	"fmt"
	"sea/crawl/app/entities"
)

func Migration() {
	err := DB.AutoMigrate(entities.Site{}, entities.SettingSite{})
	if err != nil {
		fmt.Println(err)
	}
	err = DB.AutoMigrate(entities.Post{})
	if err != nil {
		fmt.Println(err)
	}
}
