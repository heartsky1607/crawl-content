package database

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"sea/crawl/config"
	"time"
)

var DB *gorm.DB

func LoadInstance() error {
	var err error
	config := config.DbConfig
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable TimeZone=Asia/Shanghai",
		config.Hot,
		config.User,
		config.Password,
		config.Database,
		config.Port,
	)
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
		return err
	}
	sqlDb, err := DB.DB()

	if err != nil {
		fmt.Println(err)
		return err
	}
	sqlDb.SetConnMaxLifetime(time.Hour)
	sqlDb.SetMaxIdleConns(10)
	sqlDb.SetMaxOpenConns(50)
	Migration()
	return nil

}
