package site

import (
	"sea/crawl/app/interfaces"
)

type SiteUc struct {
	repo interfaces.SiteInterface
}

func LoadInstance(repo interfaces.SiteInterface) *SiteUc {
	return &SiteUc{repo: repo}
}

func (uc *SiteUc) CrawlListNews(site string) error {
	return uc.repo.CrawlListNews(site)
}

func (uc *SiteUc) CrawlNewDetail(siteUrl string, postUrl string) error {
	return uc.repo.CrawlNewDetail(siteUrl, postUrl)
}
