package interfaces

type SiteInterface interface {
	CrawlListNews(string) error
	CrawlNewDetail(string, string) error
}
