package entities

import "time"

type Site struct {
	ID             uint         `gorm:"primary_key, AUTO_INCREMENT" json:"id"`
	Url            string       `json:"url"`
	Settings       SettingSites `json:"settings"`
	FunctionColect string       `json:"functionColect"`
	CreatedAt      time.Time    `json:"created_at"`
	UpdatedAt      time.Time    `json:"updated_at"`
}

type Sites []Site
