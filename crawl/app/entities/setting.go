package entities

import "time"

type SettingSite struct {
	ID        uint      `gorm:"primary_key, AUTO_INCREMENT" json:"id"`
	MainKey   string    `json:"mainKey"`
	SubKey    string    `json:"subKey"`
	SiteId    int       `json:"siteId"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type SettingSites []SettingSite
