package entities

import (
	"time"
)

type Post struct {
	ID               uint      `gorm:"primary_key, AUTO_INCREMENT" json:"id"`
	Title            string    `json:"title"`
	Content          string    `json:"content"`
	SeoTitle         string    `json:"seoTitle"`
	SeoDescription   string    `json:"seoDescription"`
	FeatureImage     string    `json:"featureImage"`
	SiteUrl          string    `json:"siteUrl"`
	OriginUrl        string    `json:"originUrl"`
	ProcessStatus    int       `json:"processStatus"`
	CreatedAt        time.Time `json:"created_at"`
	UpdatedAt        time.Time `json:"updated_at"`
	FeatureId        int       `json:"featureId"`
	UpdateFeatureUrl string    `json:"updateFeatureUrl"`
	WpTitle          string    `json:"wpTitle"`
	WpContent        string    `json:"wpContent"`
	WpSeoTitle       string    `json:"wpSeoTitle"`
	WpSeoDescription string    `json:"wpSeoDescription"`
}
type Posts []Post
