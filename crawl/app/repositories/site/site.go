package site

import (
	"fmt"
	"gorm.io/gorm"
	"sea/crawl/app/entities"
	crawl2 "sea/crawl/app/services/crawl"
	"sea/crawl/app/services/database"
	"sea/crawl/app/services/redis"
	"strings"
)

type SiteRepository struct {
	db    *gorm.DB
	redis *redis.RedisClient
}

func LoadInstance() *SiteRepository {
	return &SiteRepository{
		db:    database.DB,
		redis: redis.Redis,
	}
}

func (repo *SiteRepository) CrawlListNews(site string) error {
	sv := crawl2.LoadInstance()
	fmt.Println("site " + site)
	sv.LoadSite(site)
	return nil
}

func (repo *SiteRepository) CrawlNewDetail(siteUrl string, postUrl string) error {
	sv := crawl2.LoadInstance()
	post := sv.CrawlPost(siteUrl, postUrl)
	if post.Title == "" || post.Content == "" {
		post.ProcessStatus = 5
	}
	post.SiteUrl = siteUrl

	var originPost entities.Post
	err := repo.db.First(&originPost, "origin_url=?", postUrl)

	if err.Error == nil {
		originPost.Content = post.Content
		originPost.Title = post.Title
		originPost.SeoTitle = post.SeoTitle
		originPost.SeoDescription = post.SeoDescription
		if originPost.Title == "" && originPost.SeoTitle != "" {
			originPost.Title = originPost.SeoTitle
		}
		if originPost.Title != "" && originPost.SeoTitle == "" {
			originPost.SeoTitle = originPost.Title
		}

		if originPost.FeatureImage == "" {
			originPost.ProcessStatus = 5
		}

		if originPost.Title == "" || originPost.Content == "" {
			originPost.ProcessStatus = 5
		} else {
			splitContent := strings.Split(originPost.Content, " ")
			fmt.Println("content length", len(splitContent))
			if len(splitContent) < 300 {
				originPost.ProcessStatus = 5
			}
		}
		repo.db.Save(&originPost)
		fmt.Println(originPost.ID)
	} else {
		if post.Title == "" && post.SeoTitle != "" {
			post.Title = post.SeoTitle
		}
		if post.Title != "" && post.SeoTitle == "" {
			post.SeoTitle = post.Title
		}
		if post.Title == "" || post.Content == "" {
			post.ProcessStatus = 5
		} else {
			splitContent := strings.Split(post.Content, " ")
			fmt.Println("content length", len(splitContent))
			if len(splitContent) < 300 {
				post.ProcessStatus = 5
			}
		}
		repo.db.Save(&post)
		fmt.Println(post.ID)
	}
	return nil
}
