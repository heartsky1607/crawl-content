package main

import (
	"github.com/gofiber/fiber/v2"
	"sea/backup/app/controllers"
)

func main() {
	app := fiber.New()
	controller := controllers.LoadHomeInstance()
	app.Get("/", func(ctx *fiber.Ctx) error {
		ctx.Status(fiber.StatusOK).JSON(fiber.Map{
			"status": true,
		})
		return nil
	})
	app.Get("/single", controller.LoadSingleDomain)

	app.Listen(":8000")
}
