package controllers

import (
	"github.com/gofiber/fiber/v2"
	"sea/backup/app/repositories"
	"sea/backup/app/usecase"
)

type HomeController struct {
	repo *usecase.HomeUc
}

func LoadHomeInstance() *HomeController {
	repo := repositories.NewHomeRepository()
	uc := usecase.NewHomeUc(repo)
	return &HomeController{
		repo: uc,
	}
}

func (controller *HomeController) LoadSingleDomain(ctx *fiber.Ctx) error {
	domain := []string{
		"bayerleverkusen.net",
		"buitiendung.org",
		"mancity2022.com",
		"westham.cc",
	}
	controller.repo.HandleAllDomain(domain)
	ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"success": true,
	})
	return nil
}
