package usecase

type HomeInterface interface {
	HandleDomain(string) error
	HandleListDomain([]string) error
}

type HomeUc struct {
	repo HomeInterface
}

func NewHomeUc(repo HomeInterface) *HomeUc {
	return &HomeUc{
		repo: repo,
	}
}

func (uc *HomeUc) HandleSingleDomain(domain string) error {
	return uc.repo.HandleDomain(domain)
}

func (uc *HomeUc) HandleAllDomain(domains []string) error {
	return uc.repo.HandleListDomain(domains)
}
