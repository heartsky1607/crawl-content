package repositories

import (
	"encoding/json"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"sea/crawl/app/services/database"
	"sea/crawl/app/services/redis"
	"sea/crawl/app/services/req"
	"strings"
	"sync"
)

type HomeRepository struct {
	db      *gorm.DB
	redis   *redis.RedisClient
	request *req.Req
}

func NewHomeRepository() *HomeRepository {
	return &HomeRepository{
		db:      database.DB,
		redis:   redis.Redis,
		request: req.LoadInstance(),
	}
}

type DataChan struct {
	fileUrl  string
	filePath string
}

func (repo *HomeRepository) HandleDomain(s string) error {
	queue := repo.LoadUrl(s)
	var wg sync.WaitGroup
	for i := 1; i < 10; i++ {
		wg.Add(1)
		go repo.DownloadImage(queue, &wg)
	}
	wg.Wait()
	fmt.Println("done")
	return nil

}

func (repo *HomeRepository) DownloadImage(queue <-chan DataChan, wg *sync.WaitGroup) {
	defer wg.Done()
	for data := range queue {
		fmt.Println(DownloadFile(data.filePath, data.fileUrl))
	}
}

func (repo *HomeRepository) LoadUrl(domain string) <-chan DataChan {
	folder := strings.Replace(domain, ".", "", -1)
	var links map[string]string
	var chanLink = make(chan DataChan, 100)
	result, code := repo.request.Get("https://"+domain+"/wp-json/sea/all-media-post", map[string]string{})
	if code != 200 {
		fmt.Println("error ", code)
	}
	err := json.Unmarshal([]byte(result), &links)
	if err != nil {
		fmt.Println(err)
	}

	go func() {

		for _, link := range links {
			u, _ := url.Parse(link)
			path := u.Path
			chanLink <- DataChan{
				link,
				folder + path,
			}

		}
		close(chanLink)
	}()
	return chanLink

}

func (repo *HomeRepository) HandleListDomain(domains []string) error {

	for _, domain := range domains {
		fmt.Println(domain)
		repo.HandleDomain(domain)
	}
	return nil
}

func DownloadFile(imagePath string, url string) (string, error) {

	path := filepath.Dir(imagePath)
	pathDir := "./storage/" + path
	if _, err := os.Stat(pathDir); errors.Is(err, os.ErrNotExist) {
		err := os.MkdirAll(pathDir, os.ModePerm)
		if err != nil {
			return "", err
		}
	}

	// Get the data

	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if err != nil {
		return "", err
	}
	// Create the file
	storagePath := "./storage/" + imagePath
	out, err := os.Create(storagePath)
	if err != nil {
		return "", err
	}
	// fmt.Println(out)
	defer out.Close()
	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return storagePath, err
}
