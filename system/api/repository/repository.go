package repository

import (
	"encoding/json"
	"fmt"
	"system/core/model"
	"system/core/service/systemredis"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo"
)

type apiRepository struct {
	redisClient *systemredis.RedisClient
	mongoClient *mongo.Client
}

func Initialization(redisClient *systemredis.RedisClient, mongoClient *mongo.Client) *apiRepository {
	return &apiRepository{
		redisClient: redisClient,
		mongoClient: mongoClient,
	}

}

func (sk *apiRepository) GetLastMessage(c *fiber.Ctx, user string, channel string) ([]model.Message, error) {

	var mesgs []model.Message
	key := "last_message_channel_" + channel
	data, err := sk.redisClient.GetItemFromStream(key)
	if err != nil {
		return nil, err
	}

	for _, msg := range data {
		dataItem := msg.Values
		var mesg model.Message
		msgData, err := json.Marshal(dataItem)
		if err != nil {
			fmt.Println(err)
		}
		err = json.Unmarshal(msgData, &mesg)
		if err != nil {
			fmt.Println(err)
		}
		mesgs = append(mesgs, mesg)

	}

	return mesgs, nil

}
func (sk *apiRepository) SendBroadCastChannel(c *fiber.Ctx, channel string, msg string) error {
	return sk.redisClient.PublishMessage(channel, msg)
}
