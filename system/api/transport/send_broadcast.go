package transport

import (
	"net/http"
	"system/api/biz"

	"github.com/gofiber/fiber/v2"
)

type broadcast struct {
	Channel string `json:"channel"`
	Content string `json:"content"`
}

func (hdl *apiHandler) SendBroadCastChannel(c *fiber.Ctx) error {
	biz := biz.ApiBiz(hdl.repo)
	channel := c.Params("channel")
	var msg broadcast
	if err := c.BodyParser(&msg); err != nil {
		c.Status(http.StatusOK).JSON(&fiber.Map{
			"success": true,
			"error":   err.Error(),
		})
		return nil
	}

	err := biz.SendBroadCastChannel(c, channel, msg.Content)
	if err != nil {
		c.Status(http.StatusOK).JSON(&fiber.Map{
			"success": true,
			"error":   err.Error(),
		})
		return nil
	}
	c.Status(http.StatusOK).JSON(&fiber.Map{
		"success": true,
		"data":    true,
	})
	return nil
}
