package transport

import "system/api/biz"

type apiHandler struct {
	repo biz.ApiBiz
}

func NewApiHandler(repo biz.ApiBiz) *apiHandler {
	return &apiHandler{
		repo: repo,
	}

}
