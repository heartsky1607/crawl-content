package transport

import (
	"fmt"
	"net/http"
	"system/api/biz"

	"github.com/gofiber/fiber/v2"
)

func (hdl *apiHandler) GetLastMessage(c *fiber.Ctx) error {
	biz := biz.ApiBiz(hdl.repo)
	channel := c.Params("channel")
	userId := c.Params("userId")
	data, err := biz.GetLastMessage(c, userId, channel)
	if err != nil {
		fmt.Println(err)
	}
	c.Status(http.StatusOK).JSON(&fiber.Map{
		"success": true,
		"data":    data,
	})
	return nil
}
