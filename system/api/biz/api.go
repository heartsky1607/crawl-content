package biz

import (
	"system/core/model"

	"github.com/gofiber/fiber/v2"
)

type ApiBiz interface {
	GetLastMessage(*fiber.Ctx, string, string) ([]model.Message, error)
	SendBroadCastChannel(*fiber.Ctx, string, string) error
}

type apiBiz struct {
	repo ApiBiz
}

func NewApiBiz(repo ApiBiz) *apiBiz {
	return &apiBiz{
		repo: repo,
	}
}

func (biz *apiBiz) GetLastMessage(ctx *fiber.Ctx, username string, channel string) ([]model.Message, error) {

	return biz.repo.GetLastMessage(ctx, username, channel)
}

func (biz *apiBiz) SendBroadCastChannel(ctx *fiber.Ctx, channel string, msg string) error {

	return biz.repo.SendBroadCastChannel(ctx, channel, msg)
}
