package main

import (
	"net/http"
	"os"
	"system/api/repository"
	"system/api/transport"
	"system/core/service/mongodb"
	"system/core/service/systemredis"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/joho/godotenv"
)

func init() {
	err := godotenv.Load("./../.env")
	if err != nil {
		panic(err)
	}

}

func main() {
	SystemRedisClient, err := systemredis.Initialize()
	if err != nil {
		panic(err)
	}

	MongoConnectClient, err := mongodb.ConnectDb()
	if err != nil {
		panic(err)
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowMethods:     "*",
		AllowHeaders:     "*",
		ExposeHeaders:    "*",
		AllowCredentials: true,
		MaxAge:           1200,
	}))
	app.Use(recover.New())

	repo := repository.Initialization(SystemRedisClient, MongoConnectClient)
	skyHandler := transport.NewApiHandler(repo)
	app.Get("/", func(c *fiber.Ctx) error {
		c.Status(http.StatusOK).JSON(&fiber.Map{
			"status": true,
		})
		return nil
	})
	app.Get("/last-message-room/:channel", skyHandler.GetLastMessage)
	app.Post("/broadcast/:channel", skyHandler.SendBroadCastChannel)
	app.Listen(":" + os.Getenv("APP_PORT_API"))

}
