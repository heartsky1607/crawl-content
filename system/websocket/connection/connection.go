package connection

import (
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 30 * time.Second
)

type connection struct {
	ws        *websocket.Conn
	send      chan []byte
	LastWrite string
	id        string
}

func NewConnection(ws *websocket.Conn, connectionId string) *connection {
	return &connection{send: make(chan []byte, 256), ws: ws, id: connectionId}
}

func (c *connection) Write(mt int, payload []byte) error {
	c.ws.SetWriteDeadline(time.Now().Add(writeWait))
	return c.ws.WriteMessage(mt, payload)
}
