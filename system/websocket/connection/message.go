package connection

type Message struct {
	data   []byte
	roomId string
}

type BroadCastMessage struct {
	data   []byte
	roomId string
}
