package connection

import (
	"fmt"
	"log"
	"net/http"
	"system/core/common"
	"system/core/service/systemredis"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

type SocketConnection interface {
	VerifyConnection(string, string) bool
	StartConversation(gin.ResponseWriter, *http.Request, string, string)
	GetUpgrader() *websocket.Upgrader
}

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

const (
	// Time allowed to write a message to the peer.
	// writeWait = 30 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 20 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 51200000
)

type socket struct {
	upgrader  *websocket.Upgrader
	roomMap   map[string]map[string]*connection
	broadcast chan BroadCastMessage
	// Register requests from the connections.
	register chan Subscription

	// Unregister requests from connections.
	unregister chan Subscription

	//conection Id

	conectionIds map[string]map[string]string

	redis *systemredis.RedisClient
}

func NewSocketInstance(redis *systemredis.RedisClient) *socket {
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	sk := socket{
		upgrader:     &upgrader,
		roomMap:      make(map[string]map[string]*connection),
		conectionIds: make(map[string]map[string]string),
		broadcast:    make(chan BroadCastMessage),
		register:     make(chan Subscription),
		unregister:   make(chan Subscription),
		redis:        redis,
	}

	return &sk
}

func (sk *socket) Upgrader() *websocket.Upgrader {
	return sk.upgrader
}

func (sk *socket) VerifyConnection(roomId string, connectId string) bool {
	connectionIds := sk.roomMap[roomId]
	if connectionIds != nil {
		if _, ok := connectionIds[connectId]; ok {
			return false
		}
	}
	return true
}

func (sk *socket) StartConversation(writer gin.ResponseWriter, request *http.Request, roomId string, connectonId string) {
	defer common.Recovery()
	fmt.Println("start handle reques ws")
	sk.upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	ws, err := sk.upgrader.Upgrade(writer, request, nil)
	if err != nil {
		log.Println("upgrade error")
		panic(err)
	}
	c := NewConnection(ws, connectonId)
	s := NewSubscription(c, roomId)

	sk.register <- s
	go s.writePump(sk)
	go s.readPump(sk)
}

func (sk *socket) SendToBoadcast(data string, room string) {
	m := BroadCastMessage{[]byte(data), room}
	// fmt.Println("send from hub", data)

	sk.broadcast <- m
}

func (sk *socket) CountOnline(room string) int {
	total := 0
	if connects, ok := sk.roomMap[room]; ok {
		total = len(connects)
	}
	return total
}

func (sk *socket) Initialize() {
	for {
		select {
		case s := <-sk.register:
			fmt.Println("register room ", s.roomId)
			room := sk.roomMap[s.roomId]
			if room == nil {
				roomMap := make(map[string]*connection)
				sk.roomMap[s.roomId] = roomMap
			}

			if len(sk.roomMap[s.roomId]) < 10000 {
				sk.roomMap[s.roomId][s.conn.id] = s.conn

			}

		case s := <-sk.unregister:
			fmt.Println("register room ", s.roomId)
			connectionsIds := sk.roomMap[s.roomId]
			if connectionsIds != nil {
				if _, ok := connectionsIds[s.conn.id]; ok {
					delete(connectionsIds, s.conn.id)
					close(s.conn.send)
					if len(connectionsIds) == 0 {
						delete(sk.roomMap, s.roomId)
					}
				}
			}

		case m := <-sk.broadcast:
			connections := sk.roomMap[m.roomId]
			for cid, c := range connections {
				select {
				case c.send <- m.data:
				default:
					close(c.send)
					delete(connections, cid)
					if len(connections) == 0 {
						delete(sk.roomMap, m.roomId)
					}
				}
			}

		}
	}
}

func (sk *socket) GetUpgrader() *websocket.Upgrader {
	return sk.upgrader
}
