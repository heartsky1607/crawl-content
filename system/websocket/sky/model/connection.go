package model

import (
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 30 * time.Second
)

type connection struct {
	Id   string
	ws   *websocket.Conn
	send chan redis.Message
}

func NewConnection(ws *websocket.Conn) *connection {
	connect := connection{
		Id:   uuid.NewString(),
		send: make(chan redis.Message),
		ws:   ws,
	}

	return &connect
}

func (c *connection) Write(mt int, payload []byte) error {
	// c.mu.Lock()
	// defer c.mu.Unlock()
	c.ws.SetWriteDeadline(time.Now().Add(writeWait))
	return c.ws.WriteMessage(mt, payload)
}
func (c *connection) GetWs() *websocket.Conn {
	return c.ws
}
