package model

import (
	"context"
	"encoding/json"
	"fmt"
	"system/core/common"
	"system/core/model"
	"system/core/service/systemredis"
	"system/core/sky/provider"
	"system/worker/jobs"

	"time"

	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

const (
	commandSubscribe   = "0"
	commandUnsubscribe = "1"
	commandChat        = "2"
	DebugMod           = true
	pongWait           = 200 * time.Second
	maxMessageSize     = 51200000
	pingPeriod         = (pongWait * 9) / 10
)
const (
	publish_channel             = "list_room_available"                // available channel
	user_channel                = "user_channel_%s"                    //user subscribe channel list
	list_user_available         = "list_system_user_available"         // list available user
	list_conversation_available = "list_system_conversation_available" // list available conversation
	conversation_mesage         = "conversation_message_%s"
)

type Subscription struct {
	Id                 string
	name               string
	StopListenerChan   chan bool
	StopReadingChan    chan bool
	MessageReadChannel chan model.Message
	conn               *connection
	PubSubChannel      *redis.PubSub
	redisClient        *systemredis.RedisClient
	DisconnectChan     chan bool
}

func NewSubscription(name string, ws *websocket.Conn, redisClient *systemredis.RedisClient) *Subscription {
	conn := NewConnection(ws)

	sub := Subscription{
		Id:                 uuid.NewString(),
		name:               name,
		StopListenerChan:   make(chan bool),
		StopReadingChan:    make(chan bool),
		DisconnectChan:     make(chan bool),
		MessageReadChannel: make(chan model.Message),
		conn:               conn,
		redisClient:        redisClient,
	}
	conn.ws.SetCloseHandler(func(code int, text string) error {
		if DebugMod {
			fmt.Println("makeconnect", "connection closed id", sub.conn.Id)
		}
		sub.DisconnectChan <- true
		return nil
	})
	c, err := sub.getchannelList()
	if err != nil {
		panic(err)
	}
	if DebugMod {
		fmt.Println("list channel", c)
	}
	if len(c) > 0 {
		pubsub := sub.redisClient.SubscribeChannel(c)
		sub.PubSubChannel = pubsub
		go sub.HandleWriteData()
	}

	return &sub
}

func (sub *Subscription) Initialization() {
	go sub.readData()
	defer func() {
		sub.disconnect()
	}()
	for {
		select {
		case <-sub.DisconnectChan:
			return
		}
	}
}

func (sub *Subscription) ReLoadConnection() {
	defer common.Recovery()
	c, err := sub.getchannelList()
	if err != nil {
		panic(err)
	}
	if len(c) > 0 {
		pubsub := sub.redisClient.SubscribeChannel(c)
		sub.PubSubChannel = pubsub
		sub.StopListenerChan <- true
		go sub.HandleWriteData()
	}

}

func (sub *Subscription) SubscribeUser(channel string) (bool, error) {
	userChannelsKey := fmt.Sprintf(user_channel, sub.name)
	if sub.redisClient.CheckItemInSet(userChannelsKey, channel) {
		return true, nil
	}
	if err := sub.redisClient.AddItemToSet(userChannelsKey, channel); err != nil {
		return false, err
	}
	sub.ReLoadConnection()
	return true, nil
}

func (sub *Subscription) UnsubscribeUser(channel string) (bool, error) {
	userChannelsKey := fmt.Sprintf(user_channel, sub.name)
	if !sub.redisClient.CheckItemInSet(userChannelsKey, channel) {
		return true, nil
	}
	if err := sub.redisClient.RemoveItemToSet(userChannelsKey, channel); err != nil {
		return false, err
	}
	sub.ReLoadConnection()
	return true, nil
}

func (sub *Subscription) disconnect() (bool, error) {
	defer common.Recovery()
	if DebugMod {
		fmt.Println("======StartConnect", "disconnect to ", sub.name, sub.conn.Id)
	}
	go func() {
		defer common.Test("StopListenerChan")
		close(sub.StopListenerChan)

	}()

	go func() {
		defer common.Test("StopReadingChan")
		close(sub.StopReadingChan)

	}()

	if sub.PubSubChannel != nil {
		err := sub.PubSubChannel.Unsubscribe(context.Context(context.TODO()))
		if err != nil {
			panic(err)
		}
		err = sub.PubSubChannel.Close()
		if err != nil {
			panic(err)
		}
	}
	go func() {
		defer common.Recovery()
		close(sub.MessageReadChannel)
		close(sub.DisconnectChan)
	}()

	fmt.Println("==========ok")
	return true, nil
}

func (sub *Subscription) HandleWriteData() {
	conn := sub.conn
	if DebugMod {
		fmt.Println("writePubsub", "starting the listener for user:", sub.name)
	}
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		defer common.Test("HandleWriteData")
		sub.PubSubChannel.Close()
		ticker.Stop()

	}()
	for {
		select {
		case msg, ok := <-sub.PubSubChannel.Channel():
			if !ok {
				return
			}
			if err := conn.Write(websocket.TextMessage, []byte(msg.Payload)); err != nil {
				if DebugMod {
					fmt.Println("HandleWriteData", err, conn.Id)
				}
				return
			}

		case <-sub.StopListenerChan:
			if DebugMod {
				fmt.Println("writePubsub", "stopping the listener for user:", sub.name, sub.conn.Id)
			}
			return
		case <-ticker.C:
			fmt.Println("send ping message")
			if err := conn.Write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}

}

func (sub *Subscription) HandleReadData() {
	fmt.Println("HandleReadData", "listent from connection", sub.conn.Id)
	defer func() {

		sub.GetConnection().ws.Close()
		sub.DisconnectChan <- true
	}()
	for {
		select {
		case <-sub.StopReadingChan:
			if DebugMod {
				fmt.Println("HandleReadData", "stopping the listener for connect:", sub.conn.Id)
			}
			return
		default:
			sub.readData()
		}
	}

}

func (sub *Subscription) readData() {
	var msg model.Message
	conn := sub.conn.GetWs()
	defer func() {
		if DebugMod {
			fmt.Println("readData", "send signal stop reading ", sub.Id)
		}
		sub.StopReadingChan <- true
	}()

	conn.SetReadLimit(maxMessageSize)

	fmt.Println("pong wait", pongWait)

	conn.SetReadDeadline(time.Now().Add(pongWait))
	conn.SetPongHandler(func(string) error { conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, item, err := conn.ReadMessage()
		if err != nil {
			// fmt.Println(websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway))
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				var data map[string]interface{}
				json.Unmarshal(item, &data)
				fmt.Println("readData", "error: ", err)
				fmt.Println(data)
			}
			if DebugMod {
				fmt.Println("readData", "error read socket", err.Error())
			}
			break
		} else {
			err = json.Unmarshal(item, &msg)
			if err != nil {
				fmt.Println("readData", "decode messsage error", err)
				continue
			}
			// sk.SendMessage("test2", string(msg))
			if DebugMod {
				fmt.Println("readData", "read from connection", sub.conn.Id, msg.Command)
				// fmt.Println("readPubsub", "read from sub", sub.Id)
				// fmt.Println("readPubsub", "command", msg.Command)
				fmt.Println("readPubsub", "content", msg.Content)
			}

			switch msg.Command {
			case commandSubscribe:
				if _, err := sub.SubscribeUser(msg.Channel); err != nil {
					fmt.Println(err)
				}
			case commandUnsubscribe:
				if _, err := sub.UnsubscribeUser(msg.Channel); err != nil {
					fmt.Println(err)
				}
			case commandChat:
				if DebugMod {
					fmt.Println("send text")
				}
				if err := sub.sendMessage(&msg); err != nil {
					fmt.Println(err)
				}
			}
			// limit speed process 10 message/s
			// time.Sleep(100 * time.Millisecond)
		}

	}

}

func (sub *Subscription) getchannelList() ([]string, error) {
	var c []string

	c1, err := sub.redisClient.GetAllItemFromSet(publish_channel)
	if err != nil {
		return nil, err
	}
	c = append(c, c1...)
	c2, err := sub.redisClient.GetAllItemFromSet(fmt.Sprintf(user_channel, sub.name))
	if err != nil {
		return nil, err
	}
	c = append(c, c2...)

	return c, nil
}

func (sub *Subscription) sendMessage(msg *model.Message) error {

	loc, _ := time.LoadLocation("UTC")
	now := time.Now().In(loc)
	t := fmt.Sprintf("%d", now.UnixMilli())
	msg.CreatedAt = t
	var data map[string]interface{}
	dataStrim, err := json.Marshal(msg)
	if err != nil {
		fmt.Println(err)
	}
	if DebugMod {
		fmt.Println("SendMessage", "publish message ", msg.Channel, string(dataStrim))
	}
	json.Unmarshal(dataStrim, &data)
	var payload map[string]interface{}
	json.Unmarshal(dataStrim, &payload)
	sub.redisClient.AddItemToStream("last_message_channel_"+msg.Channel, "", data)
	go func(data map[string]interface{}) {
		defer common.Recovery()
		item := provider.Payload{Data: data, Delay: 0}
		ct := context.Background()
		fmt.Println("dispatch", item)
		err = jobs.MessageJob.Dispatch(ct, item)
		if err != nil {
			fmt.Println("error send to queue , ", err)
		}
	}(payload)

	return sub.redisClient.PublishMessage(msg.Channel, string(dataStrim))
}

func (sub *Subscription) GetName() string {
	return sub.name
}

func (sub *Subscription) GetConnection() *connection {
	return sub.conn
}
