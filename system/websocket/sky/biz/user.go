package biz

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type user struct {
	repo SubscriptionStorage
}

func NewUserConnection(repo SubscriptionStorage) *user {
	return &user{repo: repo}
}

func (u *user) Connect(w http.ResponseWriter, r *http.Request, username string) (bool, error) {

	fmt.Println("start handle reques ws")
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return false, nil
	}
	return u.repo.CreateConnection(username, ws)
}
