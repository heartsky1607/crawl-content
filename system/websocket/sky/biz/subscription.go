package biz

import (
	"github.com/gorilla/websocket"
)

type SubscriptionStorage interface {
	CreateConnection(username string, ws *websocket.Conn) (bool, error)
	Subscribe(username string, channel string) (bool, error)
	Unsubscribe(username string, channel string) (bool, error)
	Debug(string, string)
}

type subscription struct {
	repo SubscriptionStorage
}

func NewSubscription(repo SubscriptionStorage) *subscription {
	return &subscription{
		repo: repo,
	}
}

// user subscribe a room
func (sub *subscription) Subscribe(username string, channel string) (bool, error) {
	return sub.repo.Subscribe(username, channel)
}

func (sub *subscription) Unsubscribe(username string, channel string) (bool, error) {
	return sub.repo.Unsubscribe(username, channel)
}
