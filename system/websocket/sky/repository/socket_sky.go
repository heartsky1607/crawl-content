package repository

import (
	"fmt"
	"system/core/common"
	"system/core/service/systemredis"
	"system/websocket/sky/model"
	"time"

	"github.com/gorilla/websocket"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	commandSubscribe   = "0"
	commandUnsubscribe = "1"
	commandChat        = "2"
	DebugMod           = true
	pongWait           = 20 * time.Second
	maxMessageSize     = 51200000
	pingPeriod         = (pongWait * 9) / 10
)

type socketSky struct {
	redis          *systemredis.RedisClient
	db             *mongo.Client
	Subscription   map[string]*model.Subscription
	connectchan    chan *model.Subscription
	disconnectchan chan *model.Subscription
	resconnectchan chan *model.Subscription
	closeChn       chan bool
}

func NewSocketSky(client *systemredis.RedisClient, db *mongo.Client) *socketSky {
	var socket = socketSky{
		redis:          client,
		db:             db,
		Subscription:   make(map[string]*model.Subscription),
		connectchan:    make(chan *model.Subscription),
		disconnectchan: make(chan *model.Subscription),
		resconnectchan: make(chan *model.Subscription),
		closeChn:       make(chan bool),
	}
	return &socket
}
func (sk *socketSky) CreateConnection(username string, conn *websocket.Conn) (bool, error) {
	defer common.Recovery()
	originSub, ok := sk.Subscription[username]
	if ok {
		originSub.StopListenerChan <- true
		originSub.StopReadingChan <- true
		delete(sk.Subscription, username)
	}
	go sk.SetUpConnection(username, conn)
	return true, nil
}
func (sk *socketSky) SetUpConnection(username string, conn *websocket.Conn) {
	defer common.Recovery()
	sub := model.NewSubscription(username, conn, sk.redis)
	sub.HandleReadData()
	sk.Subscription[username] = sub
}
func (sk *socketSky) CloseConnection(sub *model.Subscription) {
	defer common.Recovery()

	err := sub.GetConnection().GetWs().Close()
	if err != nil {
		panic(err)
	}
	fmt.Println("==========")
}
func (sk *socketSky) Subscribe(username string, channel string) (bool, error) {
	sub, ok := sk.Subscription[username]
	if !ok {
		if DebugMod {
			fmt.Println("Subscribe", "username in valid", username)
		}
		return false, nil
	}

	return sub.SubscribeUser(channel)
}

func (sk *socketSky) Unsubscribe(username string, channel string) (bool, error) {

	sub, ok := sk.Subscription[username]
	if !ok {
		return false, nil
	}
	return sub.UnsubscribeUser(channel)
}

func (sk *socketSky) Debug(username string, channel string) {
	userChannelsKey := fmt.Sprintf("user_channel_%s", channel)
	sk.redis.AddItemToSet(userChannelsKey, channel)

}
