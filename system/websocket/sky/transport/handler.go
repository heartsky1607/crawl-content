package transport

import (
	"system/websocket/sky/biz"
)

type skyHandler struct {
	repo biz.SubscriptionStorage
}

func NewSkyHandler(repo biz.SubscriptionStorage) *skyHandler {
	return &skyHandler{
		repo: repo,
	}
}
