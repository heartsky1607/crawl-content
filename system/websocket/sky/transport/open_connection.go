package transport

import (
	"fmt"
	"strings"
	"system/websocket/sky/biz"

	"github.com/gin-gonic/gin"
)

func (hdl *skyHandler) OpenConnection(c *gin.Context) {

	biz := biz.NewUserConnection(hdl.repo)
	username := c.Param("userId")
	if strings.HasPrefix(username, "skyx") {
		hdl.repo.Debug(username, username)
	}
	_, err := biz.Connect(c.Writer, c.Request, username)
	if err != nil {
		fmt.Println(err)
	}
}
