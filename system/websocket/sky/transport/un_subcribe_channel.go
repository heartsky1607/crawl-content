package transport

import (
	"net/http"
	"system/websocket/sky/biz"

	"github.com/gin-gonic/gin"
)

func (hdl *skyHandler) UnSubscribeChannel(c *gin.Context) {
	biz := biz.NewSubscription(hdl.repo)
	username := c.Param("userId")
	channel := c.Param("channel")
	rs, err := biz.Unsubscribe(username, channel)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"message": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": rs,
	})

}
