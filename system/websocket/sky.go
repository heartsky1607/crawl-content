package main

import (
	"os"
	"system/core/service/mongodb"
	"system/core/sky/queue"
	"system/worker/jobs"

	// "system/websocket/sevice/systemredis"
	"system/core/service/systemredis"
	"system/websocket/sky/repository"

	skytransport "system/websocket/sky/transport"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func init() {
	err := godotenv.Load("./../.env")
	if err != nil {
		panic(err)
	}

}

func main() {
	SystemRedisClient, err := systemredis.Initialize()
	if err != nil {
		panic(err)
	}

	MongoConnectClient, err := mongodb.ConnectDb()
	if err != nil {
		panic(err)
	}
	router := gin.New()
	// router.Use(middlewares.Recovery())
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	// skConnect := connection.NewSocketInstance()
	// go skConnect.Initialize()
	// handler := transport.NewHandler(systemredis.Redis, mongodb.MongoClient, skConnect)
	queueMessage := queue.NewQueue("save_message")
	// queueConversation := queue.NewQueue("store_conversation")

	<-time.After(2 * time.Second)
	jobs.NewSaveMessageJob(queueMessage, SystemRedisClient, MongoConnectClient)
	repo := repository.NewSocketSky(SystemRedisClient, MongoConnectClient)
	skyHandler := skytransport.NewSkyHandler(repo)

	router.GET("/ws/:userId", skyHandler.OpenConnection)
	router.GET("/subscribe/:userId/:channel", skyHandler.SubscribeChannel)
	router.GET("/unsubscribe/:userId/:channel", skyHandler.UnSubscribeChannel)

	router.Run(":" + os.Getenv("APP_PORT_SOCKET"))
}
