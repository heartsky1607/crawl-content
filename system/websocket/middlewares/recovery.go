package middlewares

import (
	"log"

	"github.com/gin-gonic/gin"
)

func Recovery() func(*gin.Context) {
	return func(c *gin.Context) {
		defer func() {
			if r := recover(); r != nil {
				log.Println("recovery ", r)
			}
		}()

		c.Next()
	}
}
