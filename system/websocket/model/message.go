package model

type Message struct {
	Content string `json:"content,omitempty"`
	Channel string `json:"channel,omitempty"`
	Command string `json:"command,omitempty"`
	Err     string `json:"err,omitempty"`
}
