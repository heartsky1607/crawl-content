package main

import (
	"os"
	"system/core/service/mongodb"
	"system/core/service/systemredis"
	"system/core/sky/dispatcher"
	"system/core/sky/queue"
	"system/worker/jobs"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
)

func init() {
	err := godotenv.Load("./../.env")
	if err != nil {
		panic(err)
	}

}
func main() {
	app := fiber.New()
	SystemRedisClient, err := systemredis.Initialize()
	if err != nil {
		panic(err)
	}

	MongoConnectClient, err := mongodb.ConnectDb()
	if err != nil {
		panic(err)
	}

	// socketTransport := socket.Initialize(redisCli)
	queueMessage := queue.NewQueue("save_message")
	// queueConversation := queue.NewQueue("store_conversation")

	<-time.After(2 * time.Second)
	jobs.NewSaveMessageJob(queueMessage, SystemRedisClient, MongoConnectClient)

	job := jobs.NewSaveMessageJob(queueMessage, SystemRedisClient, MongoConnectClient)
	dispath := dispatcher.NewDispatcher(job)
	dispath.Initialize(5)
	defer queueMessage.CloseConnection()
	app.Listen(":" + os.Getenv("APP_PORT_WORKER"))
}
