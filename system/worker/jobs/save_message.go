package jobs

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"system/core/model"
	"system/core/service/systemredis"
	messageRepo "system/worker/message"

	"system/core/sky/provider"
	"system/core/sky/queue"

	"go.mongodb.org/mongo-driver/mongo"
)

type saveMessageJob struct {
	name   string
	queue  *queue.Queue
	redis  *systemredis.RedisClient
	client *mongo.Client
}

var MessageJob *saveMessageJob

func NewSaveMessageJob(queue *queue.Queue, redis *systemredis.RedisClient, mongoClient *mongo.Client) *saveMessageJob {

	MessageJob = &saveMessageJob{
		name:   "SaveMessage",
		queue:  queue,
		redis:  redis,
		client: mongoClient,
	}
	return MessageJob
}

func (job *saveMessageJob) GetName() string {
	return job.name
}

func (job *saveMessageJob) Execute(payload provider.Payload) error {
	if payload.Data["content"] == nil {
		fmt.Println("invalid message")
		return nil
	}
	repo := messageRepo.NewMessageRepo(job.redis, job.client)
	var message model.Message
	stringcontent, err := json.Marshal(payload.Data)
	if err != nil {
		fmt.Println(err)
		return err
	}
	json.Unmarshal(stringcontent, &message)
	if !message.Validate() {

		return nil
	}
	repo.AddMessage(message.Channel, &message)
	// fmt.Println("receive message", roomId, message)
	return nil
}
func (job *saveMessageJob) Dispatch(ctx context.Context, payload provider.Payload) error {

	data, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	err = job.queue.SendMessage(ctx, string(data))
	if err != nil {
		return err
	}

	return nil
}

func (job *saveMessageJob) GetQueue() *queue.Queue {
	return job.queue
}

func (job *saveMessageJob) FallBack(...interface{}) error {
	return nil
}

func GetMD5Hash(text string) string {
	fmt.Println(text)
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}
