package message

import (
	"fmt"
	"system/core/service/mongodb"
	"system/core/service/systemredis"

	"go.mongodb.org/mongo-driver/mongo"
)

type messageRepository struct {
	redis  *systemredis.RedisClient
	client *mongo.Client
}

func NewMessageRepo(redis *systemredis.RedisClient, client *mongo.Client) *messageRepository {
	var repo messageRepository
	if !mongodb.Verify() {
		fmt.Println("reconnect Db")
		client, err := mongodb.ConnectDb()
		if err != nil {
			panic(err)

		}
		repo = messageRepository{
			redis:  redis,
			client: client,
		}

	} else {
		repo = messageRepository{
			redis:  redis,
			client: client,
		}
	}
	return &repo
}
