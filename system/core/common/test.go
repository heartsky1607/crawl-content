package common

import "log"

func Test(name string) {
	if err := recover(); err != nil {
		log.Println(name, err)
	}
}
