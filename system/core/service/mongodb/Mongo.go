package mongodb

import (
	"context"
	"fmt"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var MongoClient *mongo.Client

func ConnectDb() (*mongo.Client, error) {
	ctx := context.TODO()
	fmt.Println(os.Getenv("MONGO_URL"))
	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGO_URL")))
	if err != nil {
		return nil, err
	}
	if err := mongoClient.Ping(ctx, readpref.Primary()); err != nil {
		return nil, err
	}
	MongoClient = mongoClient
	return mongoClient, nil
}

func Verify() bool {
	ctx := context.TODO()
	if err := MongoClient.Ping(ctx, readpref.Primary()); err != nil {
		fmt.Println(err)
		return false
	}
	return true
}
