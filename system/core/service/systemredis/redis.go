package systemredis

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
)

type RedisClient struct {
	client *redis.Client
}

var Redis *RedisClient
var RedisError error
var Msg redis.Message

func init() {
	Redis, RedisError = Initialize()

}

func Initialize() (*RedisClient, error) {
	redishost := os.Getenv("REDIS_HOST")
	redisport := os.Getenv("REDIS_PORT")
	redisDb, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	redispass := os.Getenv("REDIS_PASS")
	if len(redisport) == 0 {
		redisport = "6379"
	}
	if err != nil {
		redisDb = 0
	}
	fmt.Println(redishost + ":" + redisport)
	client := redis.NewClient(&redis.Options{
		Addr:     redishost + ":" + redisport,
		Password: redispass,
		DB:       redisDb,
	})
	ctx := context.Context(context.TODO())
	result, err := client.Ping(ctx).Result()

	if err != nil {
		fmt.Println(err)
		return nil, err

	}
	fmt.Println("redis ping", result)

	return &RedisClient{
		client: client,
	}, nil

}

type RedisInteface interface {
	Get()
	Set()
	Remove()
	AddItemToList()
	AddItemToSet()
	AddItemToHash()
	AddItemToSortedSet()
	GetItemFromSet()
	GetItemFromHash()
	CheckItemInSet()
	AddItemToStream()
	GetItemFromStream()
}

func (r *RedisClient) Client() *redis.Client {
	return r.client
}
func (r *RedisClient) Get(key string) (string, error) {
	ctx := context.Context(context.TODO())
	return r.client.Get(ctx, key).Result()

}

func (r *RedisClient) Set(key string, value string) error {
	ctx := context.Context(context.TODO())
	return r.client.Set(ctx, key, value, 0).Err()
}

func (r *RedisClient) SetExpire(key string, value string, expiration time.Duration) error {
	ctx := context.Context(context.TODO())
	return r.client.Set(ctx, key, value, expiration).Err()
}

func (r *RedisClient) Remove(key string) error {
	ctx := context.Context(context.TODO())
	err := r.client.Del(ctx, key).Err()
	return err
}

func (r *RedisClient) AddItemToSet(key string, val string) error {
	ctx := context.Context(context.TODO())
	return r.client.SAdd(ctx, key, val).Err()
}

func (r *RedisClient) RemoveItemToSet(key string, val string) error {
	ctx := context.Context(context.TODO())
	return r.client.SRem(ctx, key, val).Err()
}

func (r *RedisClient) CheckItemInSet(key string, item string) bool {
	ctx := context.Context(context.TODO())
	okay, err := r.client.SIsMember(ctx, key, item).Result()
	if err != nil {
		fmt.Println(err)
		return false
	}
	return okay
}

func (r *RedisClient) PopItemFromSet(key string) (string, error) {
	ctx := context.Context(context.TODO())
	return r.client.SPop(ctx, key).Result()
}

func (r *RedisClient) GetAllItemFromSet(key string) ([]string, error) {
	ctx := context.Context(context.TODO())
	return r.client.SMembers(ctx, key).Result()
}

func (r *RedisClient) AddItemToHash(key string, data map[string]string) error {
	ctx := context.Context(context.TODO())
	for prop, val := range data {
		err := r.client.HSet(ctx, key, prop, val).Err()
		if err != nil {
			return err
		}
	}
	return nil
}

func (r *RedisClient) GetItemFromHash(key string, prop string) (string, error) {
	ctx := context.Context(context.TODO())
	return r.client.HGet(ctx, key, prop).Result()
}

func (r *RedisClient) GetAllItemFromHash(key string) (map[string]string, error) {
	ctx := context.Context(context.TODO())
	return r.client.HGetAll(ctx, key).Result()
}

func (r *RedisClient) AddItemToStream(key string, prop string, data map[string]interface{}) error {
	ctx := context.Context(context.TODO())
	param := redis.XAddArgs{Stream: key, MaxLen: 100, Values: data}
	if len(prop) > 0 {
		param.ID = prop
	}
	err := r.client.XAdd(ctx, &param).Err()
	return err
}

func (r *RedisClient) GetItemFromStream(key string) ([]redis.XMessage, error) {
	ctx := context.Context(context.TODO())
	vals, err := r.client.XRange(ctx, key, "-", "+").Result()
	return vals, err
}

func (r *RedisClient) AddItemToSortedSet(key string, data string, point float64) error {
	ctx := context.Context(context.TODO())
	param := redis.Z{Score: point, Member: data}
	return r.client.ZAdd(ctx, key, &param).Err()

}

func (r *RedisClient) SubscribeChannel(channels []string) *redis.PubSub {
	ctx := context.Context(context.TODO())
	return r.client.Subscribe(ctx, channels...)

}
func (r *RedisClient) PublishMessage(channel string, content string) error {
	ctx := context.Context(context.TODO())
	return r.client.Publish(ctx, channel, content).Err()

}

func (r *RedisClient) GetItemFromSortedSetByPoint(key string, start string, end string) ([]string, error) {
	parram := redis.ZRangeBy{Min: start, Max: end}
	ctx := context.Context(context.TODO())
	data, rr := r.client.ZRangeByScore(ctx, key, &parram).Result()
	return data, rr

}

func (r *RedisClient) RemoveItemFromSortedSetByPoint(key string, start string, end string) error {
	ctx := context.Context(context.TODO())
	rr := r.client.ZRemRangeByScore(ctx, key, start, end).Err()
	return rr

}
