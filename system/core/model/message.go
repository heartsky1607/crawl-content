package model

type Message struct {
	Id        string `json:"id,omitempty"`
	Content   string `json:"content,omitempty"`
	Channel   string `json:"channel,omitempty"`
	Command   string `json:"command,omitempty"`
	CreatedAt string `json:"created_at,omitempty"`
	Err       string `json:"err,omitempty"`
}

type MessageCreate struct {
	Id        string `json:"id" bson:"id" binding:"required"`
	Content   string `json:"content,omitempty" bson:"content" binding:"required"`
	Type      string `json:"type,omitempty" bson:"type" binding:"required"`
	Userinfo  string `json:"userinfo,omitempty" bson:"userinfo" binding:"required"`
	CreatedAt string `json:"created_at,omitempty" bson:"created_at" binding:"required"`
}

func (mes Message) Validate() bool {
	return true
}
