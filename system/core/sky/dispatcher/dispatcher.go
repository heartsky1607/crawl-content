package dispatcher

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"system/core/sky/provider"
	"system/core/sky/queue"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Dispatcher struct {
	Workers    []*Worker
	WorkerPool chan chan provider.Payload
	Queue      *queue.Queue
	Job        provider.SkyJob
}

func NewDispatcher(job provider.SkyJob) *Dispatcher {
	pool := make(chan chan provider.Payload)
	workQueue := job.GetQueue()

	return &Dispatcher{
		WorkerPool: pool,
		Queue:      workQueue,
		Job:        job,
	}
}

func (disp *Dispatcher) Initialize(maxWorker int) {
	// Now, create all of our workers.
	for i := 0; i < maxWorker; i++ {

		func(job provider.SkyJob) {
			worker := NewWorker(i+1, disp.WorkerPool, job)
			fmt.Println(i, disp.Queue.GetName())
			disp.Workers = append(disp.Workers, worker)
			worker.Initialize()
		}(disp.Job)

	}
	go func() {

		messages, err := disp.Queue.Consume()
		chClosedCh := make(chan *amqp.Error, 1)
		disp.Queue.GetChannel().NotifyClose(chClosedCh)

		if err != nil {
			log.Println("close channne", err)
		}
		ctx := context.Context(context.TODO())

		for {
			select {
			case <-ctx.Done():
				disp.Queue.CloseConnection()
				return

			case amqErr := <-chClosedCh:
				// This case handles the event of closed channel e.g. abnormal shutdown
				fmt.Printf("AMQP Channel closed due to: %s\n", amqErr)

				messages, err = disp.Queue.Consume()
				if err != nil {
					// If the AMQP channel is not ready, it will continue the loop. Next
					// iteration will enter this case because chClosedCh is closed by the
					// library
					fmt.Println("Error trying to consume, will try again")
					time.Sleep(2 * time.Second)
					continue
				}

				// Re-set channel to receive notifications
				// The library closes this channel after abnormal shutdown
				chClosedCh = make(chan *amqp.Error, 1)
				disp.Queue.GetChannel().NotifyClose(chClosedCh)

			case message := <-messages:
				// Ack a message every 2 seconds

				if err := message.Ack(false); err != nil {
					fmt.Printf("Error acknowledging message: %s\n", err)
				}
				var data provider.Payload
				err = json.Unmarshal([]byte(message.Body), &data)
				fmt.Println(data)
				if err != nil {
					fmt.Println(err, data)
					continue
				}

				go func() {
					worker := <-disp.WorkerPool
					worker <- data
				}()
			}
		}

	}()
}
