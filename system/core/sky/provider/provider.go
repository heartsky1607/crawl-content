package provider

import (
	"context"
	"system/core/sky/queue"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Queue interface {
	GetName() string
	CloseConnection() error
	GetChannel() *amqp.Channel
	SendMessage(context.Context, string) error
}

type Payload struct {
	Data  map[string]interface{}
	Delay time.Duration
}

type SkyJob interface {
	GetName() string
	Execute(Payload) error
	GetQueue() *queue.Queue
	Dispatch(context.Context, Payload) error
	FallBack(...interface{}) error
}
