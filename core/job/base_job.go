package corejob

import (
	"context"
)

type Backend interface {
	Execute(ctx context.Context) error
	Fallback(ctx context.Context) error
	Defer(ctx context.Context) error
	State() JobState
	Retry(ctx context.Context) error
}

type BaseJob struct {
}

func (b BaseJob) Execute(ctx context.Context) error {
	return nil
}
func (b BaseJob) Fallback(ctx context.Context) error {
	return nil
}
func (b BaseJob) Defer(ctx context.Context) error {
	return nil
}
