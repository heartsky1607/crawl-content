package corejob

import (
	"context"
	"log"
	"sea/admin/commons"
	"sync"
)

type JobGroup interface {
}

type jobGroup struct {
	jobs       []Backend
	concurrent bool
	wg         *sync.WaitGroup
}

func NewJobGroup(isConcurrent bool, jobs ...Backend) *jobGroup {
	return &jobGroup{
		jobs:       jobs,
		concurrent: isConcurrent,
		wg:         new(sync.WaitGroup),
	}
}

func (jg *jobGroup) Handle(ctx context.Context) error {
	maxWorker := 10
	if len(jg.jobs) < 10 {
		maxWorker = len(jg.jobs)
	}
	jg.wg.Add(maxWorker)
	errChan := make(chan error, len(jg.jobs))
	for _, j := range jg.jobs {
		if jg.concurrent {
			go func(i Backend) {
				defer commons.Recover()
				errChan <- jg.ExecuteJob(ctx, i)
				jg.wg.Done()
			}(j)
			continue
		}
		err := jg.ExecuteJob(ctx, j)
		if err != nil {
			return err
		}
		errChan <- err
		jg.wg.Done()

	}
	for err := range errChan {
		if err != nil {
			return err
		}
	}
	return nil
}

func (jg *jobGroup) ExecuteJob(ctx context.Context, j Backend) error {
	if err := j.Execute(ctx); err != nil {
		for {
			log.Println(err)
			if j.State() == StateRetryFailed {
				return err
			}

			if j.Retry(ctx) == nil {
				return nil
			}
		}
	}

	return nil
}
