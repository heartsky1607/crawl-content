package entities

import (
	"errors"
	"gorm.io/gorm"
	"strings"
)

type User struct {
	gorm.Model
	UserName string `json:"username" gorm:"column:username"`
	Password string `json:"password" gorm:"column:password"`
	Roles    *Roles `gorm:"many2many:user_roles;"`
}
type Users []User

type UserUpdate struct {
	UserName *string `json:"url" gorm:"column:username"`
	Password *string `json:"keywords" gorm:"column:password"`
}

type UserCreate struct {
	UserName string `json:"url" gorm:"column:username"`
	Password string `json:"keywords" gorm:"column:password"`
}

func (user UserCreate) Validate() error {
	user.UserName = strings.TrimSpace(user.UserName)
	if len(user.UserName) == 0 {
		return errors.New("username's empty")
	}

	user.Password = strings.TrimSpace(user.Password)
	if len(user.Password) == 0 {
		return errors.New("password's empty")
	}
	return nil
}
