package entities

import (
	"errors"
	"gorm.io/gorm"
	"strings"
)

type Role struct {
	gorm.Model
	Name string `json:"name" gorm:"column:name"`
}

type Roles []Role

func (Role) TableName() string {
	return "roles"
}

type RoleUpdate struct {
	gorm.Model
	Name string `json:"name" gorm:"column:name"`
}

func (RoleUpdate) TableName() string {
	return Role{}.TableName()
}

type RoleCreate struct {
	gorm.Model
	Name string `json:"name"`
}

func (role RoleCreate) Validate() error {
	role.Name = strings.TrimSpace(role.Name)
	if len(role.Name) == 0 {
		return errors.New("name's empty")
	}
	return nil
}
func (role RoleUpdate) Validate() error {
	role.Name = strings.TrimSpace(role.Name)
	if len(role.Name) == 0 {
		return errors.New("name's empty")
	}
	return nil
}

func (RoleCreate) TableName() string {
	return Role{}.TableName()
}
