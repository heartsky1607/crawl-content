package entities

import (
	"gorm.io/gorm"
)

type Site struct {
	gorm.Model
	Url      string   `json:"url"`
	Keywords []string `json:"keywords"`
}

type Sites []Site

func (Site) TableName() string {
	return "sites"
}

type SiteUpdate struct {
	Url      *string   `json:"url" gorm:"column:url"`
	Keywords *[]string `json:"keywords" gorm:"column:keywords"`
}

func (SiteUpdate) TableName() string {
	return Site{}.TableName()
}

type SiteCreate struct {
	Url      string   `json:"url" gorm:"column:url"`
	Keywords []string `json:"keywords" gorm:"column:keywords"`
}
