package entities

import "time"

type UserRole struct {
	Role      *Role
	User      *User
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
