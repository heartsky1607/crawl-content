package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/urfave/cli/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"math"
	"os"
	"sea/admin/commons"
	corejob "sea/core/job"
	skyDispatcher "sea/core/module/sky/dispatcher"
	queue2 "sea/core/module/sky/queue"
	"sea/core/services/database"
	"strconv"
	"time"
)

func main() {
	Load()
	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	SetupRoute(app)
	ctx := context.Background()
	queue, err := queue2.NewQueue("test")
	fmt.Println(err)

	app.Get("/send", func(ctx *fiber.Ctx) error {
		for i := 0; i < 1000; i++ {
			payload := skyDispatcher.Payload{Name: "test" + strconv.Itoa(i), Data: map[string]interface{}{"content": "demo", "check": true}, Delay: time.Duration(5 * time.Second)}
			data, err := json.Marshal(payload)
			err = queue.SendMessage(ctx.UserContext(), string(data))
			fmt.Println(err)
		}

		return err
	})

	//queue.CloseConnection()

	ctx = context.WithValue(ctx, "data", "hello")
	go func(ctx context.Context) {
		defer commons.Recover()
		job := corejob.NewJob(func(ctx context.Context) error {
			time.Sleep(5 * time.Second)
			fmt.Println("run job")
			fmt.Println(ctx.Value("data"))
			return nil
		}, corejob.WithName("IncreaseLikeCount"))

		if err := corejob.NewJobGroup(false, job).Handle(ctx); err != nil {
			fmt.Println(err)
		}
	}(ctx)

	app.Listen(":8000")

}

var collection *mongo.Collection

func Load() {
	database.LoadDbInstance()
	var ctx = context.TODO()

	clientOptions := options.Client().ApplyURI("mongodb://root:example@localhost:27017")
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)
	fmt.Println("check status")
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	collection = client.Database("demo").Collection("tasks")
	app := &cli.App{
		Name:  "tasker",
		Usage: "A simple CLI program to manage your tasks",
		Commands: []*cli.Command{
			{
				Name:    "add",
				Aliases: []string{"a"},
				Usage:   "add a task to the list",
				Action: func(c *cli.Context) error {
					str := c.Args().First()
					if str == "" {
						return errors.New("Cannot add an empty task")
					}

					task := &Task{
						ID:        primitive.NewObjectID(),
						CreatedAt: time.Now(),
						UpdatedAt: time.Now(),
						Text:      str,
						Completed: false,
					}

					return createTask(task)
				},
			},
		},
	}

	err = app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

type Task struct {
	ID        primitive.ObjectID `bson:"_id"`
	CreatedAt time.Time          `bson:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at"`
	Text      string             `bson:"text"`
	Completed bool               `bson:"completed"`
}

func createTask(task *Task) error {
	var ctx = context.TODO()
	_, err := collection.InsertOne(ctx, task)
	return err
}

func timeSince(start time.Time) time.Duration {
	secs := float64(time.Since(start)) / float64(time.Second)
	return time.Duration(math.Floor(secs)) * time.Second
}

func SetupRoute(app *fiber.App) {
	adminGroup := app.Group("/v1/administration")
	{
		roleGroup := adminGroup.Group("/roles")
		{
			roleGroup.Post("/", func(ctx *fiber.Ctx) error {
				ctx.Status(fiber.StatusOK).JSON(&fiber.Map{
					"success": true,
				})
				return nil
			})
		}

	}
}
