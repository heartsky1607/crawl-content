package skydispatcher

import "time"

type Payload struct {
	Name  string
	Data  map[string]interface{}
	Delay time.Duration
}
