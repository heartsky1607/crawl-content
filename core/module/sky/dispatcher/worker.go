package skydispatcher

import (
	"fmt"
	"time"
)

type worker struct {
	Id            int
	WorkerPool    chan chan Payload
	WorkerChannel chan Payload
	QuitChannel   chan bool
}

func NewWorker(id int, workerPool chan chan Payload) *worker {
	return &worker{
		Id:            id,
		WorkerPool:    workerPool,
		WorkerChannel: make(chan Payload, 3),
		QuitChannel:   make(chan bool),
	}
}

func (w *worker) Start() {
	go func() {
		for {
			fmt.Println("first", cap(w.WorkerPool))
			w.WorkerPool <- w.WorkerChannel
			fmt.Println(cap(w.WorkerPool))
			select {
			case payload := <-w.WorkerChannel:
				fmt.Printf("worker%d: Received work request, delaying for %f seconds\n", w.Id, payload.Delay.Seconds())
				time.Sleep(payload.Delay)
				fmt.Printf("worker%d: Hello, %s!\n", w.Id, payload.Name)
			case <-w.QuitChannel:
				fmt.Printf("worker%d stopping\n", w.Id)
				return
			}
		}

	}()

}

func (w *worker) Stop() {
	go func() {
		w.QuitChannel <- true
	}()
}
