package skydispatcher

import (
	"fmt"
)

var WorkerQueue chan chan Payload

func StartDispatcher(nworkers int, WorkQueue chan Payload) {
	// First, initialize the channel we are going to but the workers' work channels into.
	WorkerQueue = make(chan chan Payload, nworkers)

	// Now, create all of our workers.
	for i := 0; i < nworkers; i++ {
		fmt.Println("Starting worker", i+1)
		worker := NewWorker(i+1, WorkerQueue)
		worker.Start()
	}

	go func() {
		for {
			select {
			case work := <-WorkQueue:
				fmt.Println("Received work requeust")
				go func() {
					worker := <-WorkerQueue
					worker <- work
				}()
			}
		}
	}()
}
