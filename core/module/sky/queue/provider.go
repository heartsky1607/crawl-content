package queue

import (
	"context"
	amqp "github.com/rabbitmq/amqp091-go"
)

type Queue interface {
	GetName() string
	CloseConnection() error
	GetChannel() *amqp.Channel
	SendMessage(context.Context, string) error
}
