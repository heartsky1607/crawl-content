package queue

import (
	"context"
	"github.com/joho/godotenv"
	amqp "github.com/rabbitmq/amqp091-go"
	"os"
)

type queue struct {
	name       string
	chanMq     *amqp.Channel
	connection *amqp.Connection
}

func NewQueue(queueName string) (*queue, error) {
	err := godotenv.Load(".env")
	if err != nil {
		return nil, err
	}
	amqpServerURL := os.Getenv("AMQP_SERVER_URL")

	connectRabbitMQ, err := amqp.Dial(amqpServerURL)
	if err != nil {
		return nil, err
	}

	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		return nil, err
	}

	_, err = channelRabbitMQ.QueueDeclare(
		queueName, // queue name
		true,      // durable
		false,     // auto delete
		false,     // exclusive
		false,     // no wait
		nil,       // arguments
	)
	if err != nil {
		return nil, err
	}
	return &queue{queueName, channelRabbitMQ, connectRabbitMQ}, nil
}

func (q *queue) GetName() string {
	return q.name
}

func (q *queue) CloseConnection() error {
	q.chanMq.Close()
	q.connection.Close()
	return nil
}

func (q *queue) GetChannel() *amqp.Channel {
	return q.chanMq
}
func (q *queue) SendMessage(ctx context.Context, msg string) error {

	message := amqp.Publishing{
		ContentType: "text/plain",
		Body:        []byte(msg),
	}
	// Attempt to publish a message to the queue.
	if err := q.chanMq.PublishWithContext(
		ctx,
		"",          // exchange
		q.GetName(), // queue name
		false,       // mandatory
		false,       // immediate
		message,     // message to publish
	); err != nil {
		return err
	}
	return nil
}
