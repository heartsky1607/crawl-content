package req

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type Req struct {
}

func LoadInstance() *Req {
	return &Req{}
}

func (req *Req) Get(urlPath string, param map[string]string) (string, int) {
	fmt.Println(urlPath, param)
	var query []string
	for k, val := range param {
		item := k + "=" + url.QueryEscape(val)
		query = append(query, item)
	}
	queryString := strings.Join(query, "&")
	path := fmt.Sprintf("%s?%s", urlPath, queryString)
	resp, err := http.Get(path)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)

	}
	return string(body), resp.StatusCode

}

func (req *Req) PostForm(urlPath string, param map[string]string) map[string]interface{} {
	data := url.Values{}
	for k, val := range param {
		data.Set(k, val)
	}
	t := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   60 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		TLSHandshakeTimeout: 60 * time.Second,
	}
	client := &http.Client{
		Transport: t,
	}
	resp, err := client.PostForm(urlPath, data)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	var res map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&res)
	return res
}

func (req *Req) PostJson(UrlPath string, param map[string]string) (map[string]interface{}, int) {
	jsonData, err := json.Marshal(param)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := http.Post(UrlPath, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		log.Fatal(err)
	}
	var res map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		log.Fatal(err)
	}
	return res, resp.StatusCode

}

func (req *Req) Post(urlPath string, param map[string]string) map[string]interface{} {
	var pr []string
	for k, val := range param {
		pr = append(pr, k+"="+val)
	}
	query := strings.Join(pr, "&")
	payload := strings.NewReader(query)

	request, _ := http.NewRequest("POST", urlPath, payload)
	request.Header.Add("cache-control", "no-cache")

	result, err := http.DefaultClient.Do(request)
	defer result.Body.Close()
	var res map[string]interface{}
	json.NewDecoder(result.Body).Decode(&res)
	if err != nil {
		log.Fatal(err)
	}

	return res
}
