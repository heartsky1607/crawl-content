package database

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"sea/core/config"
)

var DB *gorm.DB
var err error

func LoadDbInstance() (*gorm.DB, error) {

	config := config.DbConfig
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable TimeZone=Asia/Shanghai",
		config.Hot,
		config.User,
		config.Password,
		config.Database,
		config.Port,
	)

	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return DB, nil

}
