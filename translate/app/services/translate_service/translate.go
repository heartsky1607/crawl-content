package translate_service

import (
	translate "cloud.google.com/go/translate/apiv3"
	"cloud.google.com/go/translate/apiv3/translatepb"
	"context"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type TranslateService struct {
}

func LoadInstance() *TranslateService {
	return &TranslateService{}
}

func (sv *TranslateService) TranslateText(text string) (string, error) {
	//argReplace, strReplace := sv.ReplaceHtmlTagV2(text)
	result, err := sv.translateText(text)
	if err != nil {
		return "", err
	}

	//result := sec["text"]

	//for key, val := range argReplace {
	//
	//	result = strings.Replace(result, key, val, 1)
	//}
	return result, nil

}

func (sv *TranslateService) translateText(text string) (string, error) {
	projectID := "prefab-mountain-376903"
	// Instantiates a client
	ctx := context.Background()
	client, err := translate.NewTranslationClient(ctx)
	if err != nil {
		return "", err
	}
	defer client.Close()

	// Construct request
	req := &translatepb.TranslateTextRequest{
		Parent:             fmt.Sprintf("projects/%s/locations/global", projectID),
		TargetLanguageCode: "vi",
		MimeType:           "text/html", // Mime types: "text/plain", "text/html"
		Contents:           []string{text},
	}
	resp, err := client.TranslateText(ctx, req)
	if err != nil {
		return "", err
	}

	// Display the translation for each input text provided
	var result string
	for _, translation := range resp.GetTranslations() {
		result = translation.GetTranslatedText()
	}

	return result, nil
}

func (sv *TranslateService) HandleTranslate(language string, text string) (string, error) {
	projectID := "prefab-mountain-376903"
	// Instantiates a client
	ctx := context.Background()
	client, err := translate.NewTranslationClient(ctx)
	if err != nil {
		return "", err
	}
	defer client.Close()

	// Construct request
	req := &translatepb.TranslateTextRequest{
		Parent:             fmt.Sprintf("projects/%s/locations/global", projectID),
		TargetLanguageCode: language,
		MimeType:           "text/html", // Mime types: "text/plain", "text/html"
		Contents:           []string{text},
	}
	resp, err := client.TranslateText(ctx, req)
	if err != nil {
		return "", err
	}

	// Display the translation for each input text provided
	var result string
	for _, translation := range resp.GetTranslations() {
		result = translation.GetTranslatedText()
	}

	return result, nil
}

func (sv *TranslateService) ReplaceHtmlTagV2(content string) (map[string]string, string) {
	var result = make(map[string]string)
	re := regexp.MustCompile(`<.*?>|<\/.*?>`)
	matches := re.FindAllString(content, -1)
	contentNew := content
	if len(matches) > 0 {
		for key, val := range matches {
			index := "<T" + strconv.Itoa(key) + ">"
			contentNew = strings.Replace(contentNew, val, index, 1)
			result[index] = val
		}

	}

	return result, contentNew
}

func (sv *TranslateService) Chunks(s string, chunkSize int) []string {
	if len(s) == 0 {
		return nil
	}
	if chunkSize >= len(s) {
		return []string{s}
	}
	var chunks []string = make([]string, 0, (len(s)-1)/chunkSize+1)
	currentLen := 0
	currentStart := 0
	for i := range s {
		if currentLen == chunkSize {
			chunks = append(chunks, s[currentStart:i])
			currentLen = 0
			currentStart = i
		}
		currentLen++
	}
	chunks = append(chunks, s[currentStart:])
	return chunks
}
