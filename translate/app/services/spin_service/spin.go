package spin_service

import (
	"fmt"
	"regexp"
	"sea/crawl/app/services/req"
	"strconv"
	"strings"
)

const (
	actQuota           = "api_quota"
	actSpin            = "text_with_spintax"
	actVariation       = "unique_variation"
	actVariationFrSpin = "unique_variation_from_spintax"
)

var defaultParam = map[string]string{
	"email_address": "daijames122@gmail.com",
	"api_key":       "807e3a8#ca5b7a5_dffb385?d7f3959",
	"action":        actVariation,
	"text":          "",
}

type SpinService struct {
	request *req.Req
}

func NewSpinService() *SpinService {
	request := req.LoadInstance()
	return &SpinService{
		request: request,
	}
}

func (sp *SpinService) SpinContent(content string) (string, error) {
	var regMatch = make(map[string]string)
	re := regexp.MustCompile(`<.*?>|<\/.*?>`)
	matches := re.FindAllString(content, -1)
	contentNew := content
	if len(matches) > 0 {
		for key, val := range matches {
			index := "[T" + strconv.Itoa(key) + "T]"
			contentNew = strings.Replace(contentNew, val, index, 1)
			regMatch[index] = val
		}
	}
	defaultParam["text"] = contentNew
	response := sp.request.PostForm("https://www.spinrewriter.com/action/api", defaultParam)
	resultText := fmt.Sprintf("%s", response["response"])

	for key, val := range regMatch {
		resultText = strings.Replace(resultText, key, val, 1)
		checkIndex := strings.Replace(strings.Replace(key, "[", "", 1), "]", "", 1)
		resultText = strings.Replace(resultText, checkIndex, val, 1)
	}

	return resultText, nil
}
