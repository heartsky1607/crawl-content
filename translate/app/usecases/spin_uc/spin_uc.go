package spin_uc

type SpinInterface interface {
	SpinContent(string) (string, error)
}

type SpinUc struct {
	repo SpinInterface
}

func NewSpinUc(repo SpinInterface) *SpinUc {
	return &SpinUc{
		repo: repo,
	}
}

func (uc *SpinUc) SpinContent(text string) (string, error) {
	return uc.repo.SpinContent(text)
}
