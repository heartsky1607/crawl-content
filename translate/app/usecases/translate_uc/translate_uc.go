package translate_uc

type TranslateInterface interface {
	HandlePost(int)
	TranslateTexts(int, map[string]string) bool
}

type TranslateUc struct {
	repo TranslateInterface
}

func LoadInstance(repo TranslateInterface) *TranslateUc {
	return &TranslateUc{
		repo: repo,
	}
}

func (uc TranslateUc) ProcessPost(postId int) {
	uc.repo.HandlePost(postId)
}

func (uc TranslateUc) TranslateTexts(postId int, texts map[string]string) {
	uc.repo.TranslateTexts(postId, texts)
}
