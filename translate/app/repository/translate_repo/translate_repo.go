package translate_repo

import (
	"fmt"
	"gorm.io/gorm"
	"sea/crawl/app/entities"
	"sea/crawl/app/services/database"
	"sea/crawl/app/services/redis"
	"sea/translate/app/services/spin_service"
	"sea/translate/app/services/translate_service"
	"strconv"
	"sync"
)

type TranslateRepository struct {
	db          *gorm.DB
	redis       *redis.RedisClient
	service     *translate_service.TranslateService
	spinService *spin_service.SpinService
}

func LoadInstance() *TranslateRepository {
	sv := translate_service.LoadInstance()
	spinSv := spin_service.NewSpinService()
	return &TranslateRepository{
		db:          database.DB,
		redis:       redis.Redis,
		service:     sv,
		spinService: spinSv,
	}
}

func (repo *TranslateRepository) HandlePost(postId int) {
	var post entities.Post
	repo.db.First(&post, "id=?", postId)
	fmt.Println("start process ", postId)
	var data = make(map[string]string)
	var err error
	status := 1

	if len(post.Title) > 10 {
		fmt.Println(post.Title)
		data["title"], err = repo.reFactorData(post.Title)
		if err != nil {
			status = 5
			fmt.Println(err)
		}
	}
	fmt.Println("handle title ")
	if len(post.Content) > 500 {
		data["content"], err = repo.reFactorData(post.Content)
		if err != nil {
			status = 5
			fmt.Println(err)
		}
	}
	fmt.Println("handle content ")
	if len(post.SeoTitle) > 10 {
		data["seo_title"], err = repo.reFactorData(post.SeoTitle)
		if err != nil {
			status = 5
			fmt.Println(err)
		}
	}

	fmt.Println("handle seo title ")
	if len(post.SeoDescription) > 10 {
		data["seo_description"], err = repo.reFactorData(post.SeoDescription)
		if err != nil {
			status = 5
			fmt.Println(err)
		}
	}

	fmt.Println("handle seo description ")

	if status == 1 {
		result := repo.TranslateTexts(int(post.ID), data)
		if result != true {
			status = 5
		}
	}
	post.ProcessStatus = status
	repo.db.Save(&post)
	fmt.Println("process done", postId, status)

}

func (repo *TranslateRepository) TranslateTexts(postId int, m map[string]string) bool {

	var wg sync.WaitGroup

	chanRequest := make(chan map[string]string)
	wg.Add(1)
	go repo.TranslateText(postId, chanRequest, &wg)
	for k, v := range m {
		chanRequest <- map[string]string{k: v}
	}

	close(chanRequest)

	wg.Wait()
	return true
}

func (repo *TranslateRepository) TranslateText(postId int, c chan map[string]string, wg *sync.WaitGroup) {
	defer wg.Done()
	for val := range c {
		for k, v := range val {
			key := k
			resultTranslate, err := repo.service.TranslateText(v)
			fmt.Println(resultTranslate, err)
			if err == nil {
				repo.redis.AddItemToHash("translation_post_"+strconv.Itoa(postId), map[string]string{key: resultTranslate})
			} else {
				fmt.Println(err)
			}
		}
	}
}

func (repo *TranslateRepository) reFactorData(text string) (string, error) {
	var result string
	textEnglish, err := repo.service.HandleTranslate("en", text)

	if err == nil {
		result, err = repo.spinService.SpinContent(textEnglish)
	} else {
		//result, err = repo.spinService.SpinContent(text)
	}
	//time.Sleep(time.Second * 7)

	return result, err
}
