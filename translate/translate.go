package main

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"sea/crawl/app/entities"
	"sea/crawl/app/services/database"
	"sea/translate/app/repository/translate_repo"
	"sea/translate/app/usecases/translate_uc"
)

func main() {
	Load()
	app := fiber.New()
	//
	repo := translate_repo.LoadInstance()
	uc := translate_uc.LoadInstance(repo)
	//uc.ProcessPost(2678)

	//text := "T0 Finals attract as well as arrange T2 T3 T4 T5 Semi-finals T6 Wednesday 14 June: (Rotterdam, 20:45 CET) T8 Thursday 15 June: (Enschede, 20:45 CET)Spain vs ItalyNetherlands vs Croatia T9 Match for 3rd area T12 Sunday 18 June: Netherlands/Croatia vs Spain/Italy (Enschede, 15:00 CET) T14 T15 Final T18 Sunday 18 June: Netherlands/Croatia vs Spain/Italy (Rotterdam, 20:45 CET) T20 T21 Neck and neck documents T22 T24 T25 Netherlands vs Croatia T29 Total record: Netherlands wins: 1, Croatia wins: 1, Draws: 0 T30 T32 T33 Spain vs Italy T37 Overall record: Spain wins: 12, Italy wins: 11, Draws: 16 T38 T39 Nations Organization record: Spain wins: 1, Italy wins: 0, Attracts: 0 T40 T42 T43 Possible final/match for third-place ties T44 T45 T46 T47 Netherlands vs Spain T48 Total document: Netherlands wins: 6, Spain wins: 5, Attracts: 2 T52 T53 T54 T55 Netherlands vs Italy T59 Total record: Netherlands wins: 3, Italy wins: 10, Draws: 10 T60 T63 Croatia vs Spain T64 T65 T67 Overall document: Croatia wins: 3, Spain wins: 5, Draws: 1 T69 Croatia vs Italy T75 Total document: Croatia wins: 3, Italy wins: 1, Attracts: 5 T76 T77 T78 T79 What else do you need to know? T80 For the semi-finals and also final, if scores are degree at the end of normal time after that there is extra time. If ball games are still level after the added 30 minutes, it mosts likely to a fine combat. T83 For the match for 3rd place, there is no added time. If ball games are degree after completion of typical time, it mosts likely to a penalty shoot-out. T85 Previous Nations Organization finals T88 T89 2021 Hosts: Italy T92 Victors: France Alipay Top Marker Trophy: Kylian Mbappé, France (2) Gamer of the Tournament: Sergio Busquets, Spain T95 T96 T97 2019 Hosts: Portugal Winners: Portugal T102 Alipay Top Scorer Trophy: Cristiano Ronaldo, Portugal (3) T103 Player of the Tournament: Bernardo Silva, Portugal T104 T106 T107\nT0 Finals attract as well as arrange T2 T3 T4 T5 Semi-finals T6 Wednesday 14 June: (Rotterdam, 20:45 CET) T8 Thursday 15 June: (Enschede, 20:45 CET)Spain vs ItalyNetherlands vs Croatia T9 Match for 3rd area T12 Sunday 18 June: Netherlands/Croatia vs Spain/Italy (Enschede, 15:00 CET) T14 T15 Final T18 Sunday 18 June: Netherlands/Croatia vs Spain/Italy (Rotterdam, 20:45 CET) T20 T21 Neck and neck documents T22 T24 T25 Netherlands vs Croatia T29 Total record: Netherlands wins: 1, Croatia wins: 1, Draws: 0 T30 T32 T33 Spain vs Italy T37 Overall record: Spain wins: 12, Italy wins: 11, Draws: 16 T38 T39 Nations Organization record: Spain wins: 1, Italy wins: 0, Attracts: 0 T40 T42 T43 Possible final/match for third-place ties T44 T45 T46 T47 Netherlands vs Spain T48 Total document: Netherlands wins: 6, Spain wins: 5, Attracts: 2 T52 T53 T54 T55 Netherlands vs Italy T59 Total record: Netherlands wins: 3, Italy wins: 10, Draws: 10 T60 T63 Croatia vs Spain T64 T65 T67 Overall document: Croatia wins: 3, Spain wins: 5, Draws: 1 T69 Croatia vs Italy T75 Total document: Croatia wins: 3, Italy wins: 1, Attracts: 5 T76 T77 T78 T79 What else do you need to know? T80 For the semi-finals and also final, if scores are degree at the end of normal time after that there is extra time. If ball games are still level after the added 30 minutes, it mosts likely to a fine combat. T83 For the match for 3rd place, there is no added time. If ball games are degree after completion of typical time, it mosts likely to a penalty shoot-out. T85 Previous Nations Organization finals T88 T89 2021 Hosts: Italy T92 Victors: France Alipay Top Marker Trophy: Kylian Mbappé, France (2) Gamer of the Tournament: Sergio Busquets, Spain T95 T96 T97 2019 Hosts: Portugal Winners: Portugal T102 Alipay Top Scorer Trophy: Cristiano Ronaldo, Portugal (3) T103 Player of the Tournament: Bernardo Silva, Portugal T104 T106 T107"
	//re := regexp.MustCompile(`T[0-9]{3}|T[0-9]{1}|T[0-9]{2}`)

	//matches := re.FindAllString(text, -1)
	//fmt.Println(matches)
	var posts entities.Posts
	database.DB.Limit(80).Find(&posts, "process_status = 0")
	for _, v := range posts {
		uc.ProcessPost(int(v.ID))
	}
	fmt.Println(len(posts))

	//repo := spin_service.NewSpinService()
	//rs, _ := repo.SpinContent("<p>UEFA’s latest course for European assistant referees, held in Rome, provided the perfect opportunity to emphasise the value of the men and women with the flag as a source of vital support not only to referees, but also to the video assistant referees (VARs) introduced in recent years as an important enhancement of the on-field decision-making process.</p>")
	//fmt.Println(rs)
	app.Listen(":8000")
}
func Load() {
	err := database.LoadInstance()
	if err != nil {
		panic(err)
	}
}
